#include <stdio.h>
#include <iostream>
#include <fstream>
#include "plankal.h"

extern ofstream	outs;

extern int	structs;

static int	local;

void gen_comment(char *cp)
{
    outs << "// ";
    if (*cp == '\t')
	outs << ' ' << cp+1 << endl;
    else
	outs << cp << endl;
}

void Bitvar::set(unsigned char *b,long l)
{
    long	l1;
    char	i;

    l1 = (l+7)/8;	// byte count
    i = (l & 7) ? 8-(l & 7) : 0;
    len = l;
    bits = new unsigned char[l1];
    for (l = 0; l < l1; l++)
	bits[l] = b[l] >> i;
}

void Bitvar::set_re(unsigned char *b,long l)
{
    long	l1;

    l1 = (l+7)/8;	// byte count
    len = l;
    bits = new unsigned char[l1];
    for (l = 0; l < l1; l++)
	bits[l] = b[l1-l-1];
}

int Bitvar::operator== (const Bitvar &r) const
{
    return len == r.len && memcmp(bits,r.bits,len) == 0;
}

char *Bitvar::gen_name() const
{
    long	l1;
    char	*cp0;
    char	*cp;
    unsigned char *bp;

    l1 = (len+7)/8;	// byte count
    cp0 = cp = new char[10+l1];
    cp += sprintf(cp,"B%08lx",len);
    for (bp = bits; l1; l1--)
	cp += sprintf(cp,"%02x",*bp++);
    return cp0;
}

void Bitvar::gen() const
{
    long	l;
    long	l1;

    outs << "S1(" << len;
    l1 = (len+7)>>3;	// byte count
    for (l = 0; l < l1; l++)
	outs << ',' << (int)bits[l];
    outs << ')';
}

ostream &operator<< (ostream &s,const Bitvar &b)
{
    long	l;
    long	l1;

    s << '{' << b.len;
    l1 = (b.len+7)>>3;	// byte count
    for (l = 0; l < l1; l++)
	s << ',' << (int)b.bits[l];
    s << '}';
    return s;
}

static char *ktyp_c[] = {
    "NONE", "LONG", "DOUBLE", "BITS"
};

Konstante::Konstante(Ktyp t)
{
    typ = t;
}

Konstante::Konstante(long n)
{
    typ = LONG;
    lval = n;
}

Konstante::Konstante(Konstante *kp)
{
    typ = kp->typ;
    switch (typ) {
    case NONE:
	    break;
    case LONG:
	    lval = kp->lval;
	    break;
    case DOUBLE:
	    dval = kp->dval;
	    break;
    case BITS:
	    bval = kp->bval;
	    break;
    }
}

Konstante::~Konstante()
{
    if (typ == BITS)
	delete bval.bits;
}

void Konstante::print() const
{
    cerr << ktyp_c[typ];
    switch (typ) {
    case NONE:
	    break;
    case LONG:
	    cerr << ':' << lval;
	    break;
    case DOUBLE:
	    cerr << ':' << dval;
	    break;
    case BITS:
	    cerr << ':' << bval;
	    break;
    }
}

void Konstante::gen() const
{
    switch (typ) {
    case NONE:
	    break;
    case LONG:
	    outs << lval;
	    break;
    case DOUBLE:
	    outs << dval;
	    break;
    case BITS:
	    bval.gen();
	    break;
    }
}

Def *Konstante::genDef() const
{
    Def		*dp;
    int		i;
    long	tmp;

    switch (typ) {
    case LONG:
	if (lval == 0 || lval == 1)
	    dp = new Def(BIT);
	else {
	    tmp = lval;
	    for (i = 0; i < sizeof(lval)*8; i++) {
		tmp >>= 1;
		if (!tmp)
		    break;
	    }
	    dp = new Def(BITVEKTOR,i+1);
	}
	break;
    case DOUBLE:
	dp = NULL;
	break;
    case BITS:
	if (bval.len == 1)
	    dp = new Def(BIT);
	else
	    dp = new Def(BITVEKTOR,bval.len);
	break;
    default:
	dp = NULL;
	break;
    }
    return dp;
}

Def::Def(Typen t,long n)
{
    typ = t;
    anz = n;
    name = NULL;
    next = NULL;
    exp = NULL;
    down = NULL;
}

Def::Def(Typen t,Namen *np)
{
    typ = t;
    anz = 0;
    name = np;
    next = NULL;
    exp = NULL;
    down = NULL;
}

Def::Def(Def *ep,Def *ap)
{
    typ = PARAMETER;
    anz = 0;
    name = NULL;
    next = ap;
    exp = NULL;
    down = ep;
}

static char *typen_c[] = {
    "UNDEF", "long", "S0", "S1", "V", "S", "FUNCTION", "PAR", "N", "Y", "T"
};

// Debug Ausgabe einer Definition

void Def::print(bool s) const	// s = true => print local name list and expressions of a function
{
    for (const Def *dp = this; dp; dp = dp->next) {
#ifdef DEBUG_ADDRS
	cerr << dp << '-';
#endif
	cerr << typen_c[dp->typ];
	switch (dp->typ) {
	    case INT:
	    case BIT:
		break;
	    case BITVEKTOR:
		cerr << '.';
		if (dp->anz)
		    cerr << dp->anz;
		if (dp->name)
		    dp->name->print(true);
		break;
	    case VEKTOR:
		if (dp->anz)
		    cerr << '[' << dp->anz << ']';
		if (dp->name) {
		    cerr << '[';
		    dp->name->print(true);
		    cerr << ']';
		}
		if (dp->down) {
		    cerr << '(';
		    dp->down->print(s);
		    cerr << ')';
		}
		break;
	    case FUNCTION:
		cerr << '(';
		if (dp->down)
		    dp->down->print(s);
		cerr << ')' << endl;
		if (dp->name && s)
		    dp->name->druckeNamen(false);
		break;
	    case PARAMETER:
		cerr << '(';
		if (dp->down)
		    dp->down->print(s);
		cerr << ')';
		break;
	    case NAMEDEF:
		cerr << '<';
		if (dp->name)
		    dp->name->print(true);
		cerr << '>';
		break;
	    case TYPEDEF:
		cerr << '<';
		if (dp->name)
		    dp->name->print(false);
		cerr << '>';
		break;
	    case TEMPLATE:
		if (dp->down) {
		    cerr << '(';
		    dp->down->print(s);
		    cerr << ')';
		}
		break;
	    case STRUCT:
		if (dp->name) {
		    cerr << ' ';
		    cerr << dp->name->name;
		}
		cerr << '(';
		if (dp->down)
		    dp->down->print(s);
		cerr << ')';
		break;
	}
	if (dp->exp && s)
	    exp->print(-1);
	if (dp->next)
	    cerr << ',';
    }
}

// Erzeugt die Definition einer Variablen

void Def::gen(bool s)
{
    int		i = 0;

    for (const Def *dp = this; dp; dp = dp->next) {
	switch (dp->typ) {
	    case INT:
		outs << "long";
		break;
	    case BIT:
		outs << "S0";
		break;
	    case BITVEKTOR:
		outs << "S1";
		break;
	    case VEKTOR:
		outs << "V_<";
		if (dp->down->typ == STRUCT && dp->down->name)
		    dp->down->name->gen();
		else {
		    dp->down->gen();
		    if (dp->down->typ == VEKTOR)
			outs << ' ';
		}
		outs << '>';
		break;
	    case NAMEDEF:
		if (dp->name->def && dp->name->def->typ == TEMPLATE) {
		    dp->name->def->gen();
		    dp->name->defined = true;
		} else
		    dp->name->gen();
		break;
	    case TEMPLATE:
		dp->down->gen();
		break;
	    case STRUCT:
		outs << "struct ";
		if (dp->name) {
		    dp->name->gen();
		    outs << " { ";
		    dp->name->gen();
		    outs << "();";
		    dp->name->gen();
		    outs << '(';
		    dp->down->sgen(true);
		    outs << ");";
		} else
		    outs << "{ ";
		dp->down->gen(true);
		outs << " }";
		break;
	}
	if (s)
	    outs << " s" << i++ << ';';
    }
}

// Erzeugt die Definition einer Variablen

void Def::sgen(bool s)
{
    int		i = 0;

    for (const Def *dp = this; dp; dp = dp->next) {
	switch (dp->typ) {
	    case INT:
		outs << "long";
		break;
	    case BIT:
		outs << "S0";
		break;
	    case BITVEKTOR:
		outs << "S1&";
		break;
	    case VEKTOR:
		outs << "V_<";
		dp->down->gen();
		if (dp->down->typ == VEKTOR)
		    outs << ' ';
		outs << '>';
		break;
	    case NAMEDEF:
		if (dp->name->def && dp->name->def->typ == TEMPLATE) {
		    dp->name->def->gen();
		    dp->name->defined = true;
		} else
		    dp->name->gen();
		break;
	    case TEMPLATE:
		dp->down->gen();
		break;
	    case STRUCT:
		outs << "STRUCT";
		break;
	}
	if (s) {
	    outs << " v" << i++;
	    if (dp->next)
		outs << ',';
	}
    }
}

// Erzeugt die Längendefinition eines Vektors

void Def::vgen()
{
    for (const Def *dp = this; dp; dp = dp->down) {
	if (dp->typ == VEKTOR) {
	    outs << '(';
	    if (dp->anz)
		outs << dp->anz;
	    outs << ')';
	}
    }
}

static Namen	*copyfromfunction;
static Namen	*copytofunction;

Def *Def::copy()
{
    Def		*dp;

    if (this) {
#ifdef DEBUG_COPY
	cerr << "Dcopy>" << dindent;
	print(false);
	cerr << endl;
	dind++;
#endif
	dp = new Def(typ,anz);
	dp->name = name->copy();
	dp->next = next->copy();
	dp->exp = exp->copy();
	dp->down = down->copy();
#if 0
	if (typ == STRUCT) {
	    if (!dp->name)
		dp->name = Namen::neuerName(&namen,"st",++structs);
	    dp->name->def = dp;
	}
#endif
#ifdef DEBUG_COPY
	dind--;
	cerr << "Dcopy<" << dindent;
	dp->print(false);
	cerr << endl;
#endif
	return dp;
    }
    return NULL;
}

Def *Def::ecopy()
{
    Def		*dp;

    if (this) {
	dp = new Def(typ,anz);
	dp->name = name;
	dp->next = next->ecopy();
	dp->exp = exp->ecopy();
	dp->down = down->ecopy();
	return dp;
    }
    return NULL;
}

Def *Def::e1copy()
{
    Def		*dp;

    if (this) {
	dp = new Def(typ,anz);
	dp->name = name;
	dp->exp = exp->ecopy();
	dp->down = down->ecopy();
	return dp;
    }
    return NULL;
}

// Geht zum Basistyp einer evtl. Typdefinition

Def *Def::basetype()
{
//    if (typ == TEMPLATE)
//	return this;
    if (this) {
	if (typ == VEKTOR)
	    return down->basetype();
	if (typ != TYPEDEF && typ != BITVEKTOR && name && name->def && name->def != this)
	    return name->def->basetype();
    }
    return this;
}

// Geht zum richtig Typ einer evtl. Typdefinition

Def *Def::realtype()
{
    if (this) {
	if (typ != TYPEDEF && typ != BITVEKTOR && typ != VEKTOR && name && name->def && name->def != this)
	    return name->def->realtype();
    }
    return this;
}

// Test ob unter dieser Definition ein Template oder Typedef liegt

Def *Def::isTemplate()
{
    if (typ == TEMPLATE || typ == TYPEDEF)
	return this;
    if (this) {
	if (down)
	    return down->isTemplate();
	if (name && name->def)
	    return name->def->isTemplate();
    }
    return NULL;
}

// Erzeugt die Ziel-Definition einer Indexoperation

Def *Def::genIndex(Expr *rp)	// TODO Struktur von rp mit einbauen (jetzt nur int)
{
    Def		*dp;

    if (typ == VEKTOR)
	return down;
    if (typ == BITVEKTOR)
	return new Def(BIT);
    if (typ == NAMEDEF)
	return name->def->genIndex(rp);
    return NULL;
}

// Erzeugt die Ziel-Definition einer Feldauswahloperation

Def *Def::genField(Expr *rp)
{
    Def		*dp;
    Def		*dp1;
    long	i;

    if (typ == NAMEDEF)
	return name->def->genField(rp);
    if (typ == STRUCT && rp->oper == CONST && rp->konst->typ == LONG) {
	i = rp->konst->lval;
	for (dp = down; dp; dp = dp->next)
	    if (i-- <= 0)
		break;
	if (dp) {
	    dp1 = new Def(dp->typ,dp->anz);
	    dp1->name = dp->name;
	    dp1->down = dp->down;
	    return dp1;
	}
    } else if (typ == BITVEKTOR)
	return new Def(BIT);
    return NULL;
}

// Gibt die Parameterdefinition einer Funktion aus

void Def::gen_par()
{
    bool first = true;

    for (const Def *dp = down; dp; dp = dp->next) {
	if (dp->name->def->typ != TYPEDEF) {
	    if (!first)
		outs << ',';
	    first = false;
	    outs << "const ";
	    dp->name->def->gen();
	    outs << ' ';
	    if (dp->name->def->typ != BIT && dp->name->def->typ != INT)
		outs << '&';
	    dp->name->gen();
	    dp->name->defined = true;
	}
    }
    if (next && next->next) {	// more than 2 return values
	for (const Def *dp = next; dp; dp = dp->next) {
	    if (!first)
		outs << ',';
	    first = false;
	    dp->name->def->gen();
	    outs << " &";
	    dp->name->gen();
	    dp->name->defined = true;
	}
    }
}

// Gibt die Ergebnisdefinition einer Funktion aus

void Def::gen_retpar()
{
    if (next && !next->next)
	next->name->def->gen();
    else
	outs << "void";
    outs << ' ';
}

// Erzeugt den künstlichen Typnamen einer Definition aus

char *Def::create_typename(const char *cp)
{
    Def		*dp;
    char	*cp0;
static char	tmp[NAMELEN+NUMLEN];

    if (cp) {
	strcpy(tmp,cp);
	strcat(tmp,"_");
    }
    for (dp = this; dp; dp = dp->next) {
	switch (dp->typ) {
	    case INT:
		strcat(tmp,"l");
		break;
	    case BIT:
		strcat(tmp,"0");
		break;
	    case BITVEKTOR:
		strcat(tmp,"1");
		break;
	    case VEKTOR:
		strcat(tmp,"V");
		dp->down->create_typename(NULL);
		break;
	    case NAMEDEF:
		dp->name->def->create_typename(NULL);
		break;
	    case TYPEDEF:
		strcat(tmp,"_");
		strcat(tmp,dp->name->name);
		break;
	    case STRUCT:
		strcat(tmp,"S");
		dp->down->create_typename(NULL);
		break;
	}
    }
    return cp ? tmp : NULL;
}

// Erzeugt Statements zum Setzen variabler Parameterwerte

void Def::varparam()
{
    for (const Def *dp = down; dp; dp = dp->next) {
	if (dp->name &&
		(dp->name->def->typ == VEKTOR || dp->name->def->typ == BITVEKTOR) &&
		dp->name->def->name) {
	    outs << xindent;
	    dp->name->def->name->gen();
	    outs << " = ";
	    dp->name->gen();
	    outs << ".count();" << endl;
	}
    }
}

// Erzeugt ein Return-Statement

void Def::gen_return()
{
    if (next && !next->next) {
	outs << xindent << "return ";
	next->name->gen();
	outs << ';' << endl;
    }
}

void Def::template_set(Def *dp)
{
    Def		*dp2;
    Def		*dp3;

    for (Def *dp1 = this; dp1 && dp; dp1 = dp1->next) {
#ifdef DEBUG_COPY
	cerr << "TS:";
	dp1->print(false);
	cerr << endl;
#endif
	if (dp1->name && dp1->name->def && dp1->name->def->typ == TEMPLATE) {
	    dp1->name->def->down = dp->e1copy();
#ifdef DEBUG_COPY
	    cerr << "=>:";
	    dp1->print(false);
	    cerr << endl;
#endif
	}
	for (dp2 = dp1; dp2->typ == NAMEDEF; dp2 = dp2->name->def)
	    ;
	for (dp3 = dp; dp3->typ == NAMEDEF; dp3 = dp3->name->def)
	    ;
	if (dp2->down)
	    dp2->down->template_set(dp3->down);
	dp = dp->next;
    }
}

static void namedef_set(Def **dp0,Def *dp)
{
    Def		*dp2;
    Def		*dp3;

    for (Def *dp1 = *dp0; dp1 && dp; dp1 = dp1->next) {
	if (dp->typ == NAMEDEF) {
	    dp2 = (*dp0)->next;
	    *dp0 = dp->e1copy();
	    (*dp0)->next = dp2;
	}
	for (dp2 = dp1; dp2->typ == NAMEDEF; dp2 = dp2->name->def)
	    ;
	for (dp3 = dp; dp3->typ == NAMEDEF; dp3 = dp3->name->def)
	    ;
	if (dp2->down)
	    namedef_set(&dp2->down,dp3->down);
	dp = dp->next;
	dp0 = &dp1->next;
    }
}

long max(long a,long b)
{
    return a > b ? a : b;
}

bool Def::operator== (const Def &d) const
{
    if (down && d.down)
	if (!(*down == *d.down))
	    return false;
    if (next && d.next)
	if (!(*next == *d.next))
	    return false;
    if (typ == NAMEDEF && name->def)
	return *name->def == d;
    if (d.typ == NAMEDEF && d.name->def)
	return *this == *d.name->def;
    return typ == d.typ && anz == d.anz;
}

static Def *dyad_typ(Def *d1,Def *d2)
{
    Def		*dp1;
    Def		*dp2;

    dp1 = d1->basetype();
    dp2 = d2->basetype();
    if (dp1->typ == INT || dp2->typ == INT)
	return new Def(INT);
    if (dp1->typ == BITVEKTOR || dp2->typ == BITVEKTOR)
	return new Def(BITVEKTOR,max(dp1->anz,dp2->anz));
    if (dp1->typ == VEKTOR && dp2->typ == VEKTOR && *dp1 == *dp2)
	return dp1;
    if (dp1->typ == STRUCT && dp2->typ == STRUCT && *dp1 == *dp2)
	return dp1;
    if (dp1->typ == dp2->typ) {
	if (d1->name && d2->name)
	    return new Def(dp1->typ,dp1->anz);
	else
	    return d1;
    }
    return new Def(UNDEF);
}

Bereich::Bereich(Konstante *f,Konstante *t)
{
    next = NULL;
    if (f)
	from = *f;
    if (t)
	to = *t;
}

void Bereich::gen() const
{
    const Bereich	*bp;

    for (bp = this; bp; bp = bp->next) {
	if (bp != this)
	    outs << ',';
	outs << '{';
	if (bp->from.typ)
	    bp->from.gen();
	if (bp->to.typ) {
	    outs << ',';
	    bp->to.gen();
	}
	outs << '}';
    }
}

void Bereich::print() const
{
    const Bereich	*bp;

    for (bp = this; bp; bp = bp->next) {
	if (bp != this)
	    cerr << ',';
	if (bp->from.typ)
	    bp->from.print();
	if (bp->to.typ) {
	    cerr << "..";
	    bp->to.print();
	}
    }
}

Bereich *Bereich::copy()
{
    Bereich	*bp;

    if (this) {
#ifdef DEBUG_COPY
	cerr << "Bcopy>";
	print();
	cerr << endl;
#endif
	bp = new Bereich(&from,&to);
	bp->next = next->copy();
#ifdef DEBUG_COPY
	cerr << "Bcopy<";
	bp->print();
	cerr << endl;
#endif
	return bp;
    }
    return NULL;
}

Namen::Namen()
{
    next = NULL;
    name = NULL;
    def = NULL;
    besch = NULL;
    defined = false;
}

Namen::Namen(const char *cp)
{
    next = NULL;
    name = cp;
    def = NULL;
    besch = NULL;
    defined = false;
}

void Namen::print(bool s) const	// s = true => print local name list and expressions of a function
{
#ifdef DEBUG_ADDRS
    cerr << this << '-';
#endif
    cerr << name;
    if (def || besch)
	cerr << '=';
    if (def)
	def->print(s);
    if (besch) {
	cerr << '[';
	besch->print();
	cerr << ']';
    }
}

void Namen::gen()
{
    if (def && def->typ == TYPEDEF) {
	if (def->name)
	    def->name->gen();
	else
	    outs << "NIX";
    } else
	outs << name;
}

void Namen::gen_fkt()
{
#ifdef DEBUG_COPY
    cerr << "gen:" << name << endl;
#endif
    def->exp->gen_fktmodels();
    def->exp->gen_structs();
    namen->define_all();
    if (!defined) {
	outs << xindent;
	def->down->gen_retpar();
	gen();
	outs << '(';
	def->down->gen_par();
	outs << ')' << endl;
	outs << xindent << '{' << endl;
	ind++;
	def->name->define_all();
	def->down->varparam();
	def->exp->gen();
	def->down->gen_return();
	ind--;
	outs << xindent << '}' << endl;
    }
    defined = true;
}

void Namen::druckeNamen(bool s) const
{
    for (const Namen *np = this; np; np = np->next) {
	np->print(s);
	cerr << endl;
    }
}

// copy a function with template function tree ep1 and parameter tree ep2

Namen *Namen::fktcopy(Expr *ep1,Expr *ep2)
{
    Namen	*np;
    Namen	*np1;
    Namen	*np2;
    Namen	**npp;
    Def		*dp0;
    Def		*dp;
    char	*cp;
    int		i;

    if (this) {
#ifdef DEBUG_COPY
	cerr << "FNcopy:";
#ifdef DEBUG_ADDRS
	cerr << this << '-';
#endif
	cerr << name << endl;
	if (ep1) {
	    cerr << "FNcopy1:";
	    ep1->print(1);
	}
	if (ep2) {
	    cerr << "FNcopy2:";
	    ep2->print(1);
	}
#endif
	copyfromfunction = copytofunction = NULL;
	dp0 = new Def(def->typ,def->anz);
	dp0->name = def->name ? def->name->copy() : NULL;
	dp0->next = def->next->copy();
	if (def->typ == FUNCTION) {		// copy local name list
	    np1 = dp0->name;
	    copyfromfunction = def->name;
	    copytofunction = np1;
	    for (np2 = def->name->next; np2; np2 = np2->next) {
		np1->next = np2->copy();
		np1 = np1->next;
	    }
	}
#ifdef DEBUG_COPY
	cerr << "FLcopy:" << endl;
#endif
	dp0->down = def->down->copy();
	i = 0;
	for (dp = dp0->down->down; dp; dp = dp->next)	// insert templates into params
	    if (dp->name->def && dp->name->def->typ == TYPEDEF) {
		dp->name->def->name = ep1->var;
#ifdef DEBUG_COPY
		cerr << "FLY:";
		ep1->var->print(false);
		cerr << endl;
#endif
		ep1 = ep1->rhs;
	    } else {
		dp->name->def->template_set(ep2->oper == PARLIST ? ep2->lhs->edef : ep2->edef);
		namedef_set(&dp->name->def,ep2->oper == PARLIST ? ep2->lhs->edef : ep2->edef);
#ifdef DEBUG_COPY
		cerr << "FLT:";
		if (ep2->oper == PARLIST)
		    ep2->lhs->edef->print(1);
		else
		    ep2->edef->print(1);
		cerr << endl;
		cerr << "==>:";
		dp->name->def->print(false);
		cerr << endl;
#endif
		ep2 = ep2->rhs;
	    }
#ifdef DEBUG_COPY
	cerr << "FEcopy:" << endl;
#endif
	dp0->exp = def->exp->copy();
	cp = dp0->down->down->create_typename(name);
	np = namen->suche(cp);
	if (!np) {	// else delete the created dp0
	    np = neuerName(&namen,cp);
	    np->def = dp0;
	    np->besch = besch->copy();
	}
	copyfromfunction = copytofunction = NULL;
	return np;
    }
    return NULL;
}

Namen *Namen::copy()
{
    Namen	*np;
    Namen	*np1;
    Namen	**npp;

    if (this) {
#ifdef DEBUG_COPY
	cerr << "Ncopy>" << dindent;
	print(false);
	cerr << endl;
	dind++;
#endif
	np = copytofunction;	// replace old addresses by copied ones
	for (np1 = copyfromfunction; np1 && np; np1 = np1->next) {
	    if (np1 == this) {
#ifdef DEBUG_COPY
		dind--;
#endif
		return np;
	    }
	    np = np->next;
	}
	if (def && def->typ == FUNCTION) {
#ifdef DEBUG_COPY
	    dind--;
#endif
	    return this;
	}
#if 0
	if (def && def->typ == STRUCT) {
	    np = Namen::neuerName(&namen,"st",++structs);
#ifdef DEBUG_COPY
	    dind--;
	    cerr << "Ncopy<" << dindent;
	    np->print(false);
	    cerr << endl;
#endif
	    return np;	// TODO FALSCH beim Kopieren von Strukturen in Parametern
	}
#endif
	np = new Namen(name);	// soll hier wirklich immer kopiert werden? TODO
	np->def = def->copy();
	np->besch = besch->copy();
#ifdef DEBUG_COPY
	dind--;
	cerr << "Ncopy<" << dindent;
	np->print(false);
	cerr << endl;
#endif
	return np;
    }
    return NULL;
}

void Namen::define_all()
{
    for (Namen *np = this; np; np = np->next) {
	if (!np->defined && np->def && np->def->typ != TYPEDEF && np->def->typ != FUNCTION) {
	    outs << xindent;
	    np->def->gen();
	    if (np->def->typ != STRUCT) {
		outs << ' ';
		np->gen();
		if ((np->def->typ == VEKTOR || np->def->typ == BITVEKTOR) &&
			!np->def->name)
		    outs << '(' << np->def->anz << ')';
	    }
	    outs << ';' << endl;
	    np->defined = true;
	}
    }
}

Namen *Namen::suche(const char *cp)
{
    Namen	*np;
    int		len;

    len = strlen(cp);
    for (np = this; np; np = np->next)
	if (strncmp(np->name,cp,len) == 0 &&
		(np->name[len] == '\0' || np->name[len] == '_'))
	    return np;
    return NULL;
}

Namen *Namen::suche(const char *cp,long i)
{
    Namen	*np;
    char	tmp[NAMELEN+NUMLEN];

    sprintf(tmp,"%s_%ld",cp,i);
    for (np = this; np; np = np->next)
	if (strcmp(np->name,tmp) == 0)
	    return np;
    return NULL;
}

Namen *Namen::neuerName(Namen **first,const char *cp)
{
    Namen	*np;
    Namen	**npp;
    char	*cp1;

#ifdef DEBUG_COPY
    if (*first)
	cerr << "Nname:" << (*first)->name << '-' << cp << endl;
    else
	cerr << "Nname:" << first << '-' << cp << endl;
#endif
    npp = first;
    for (np = *first; np; npp = &np->next, np = np->next)
	if (strcmp(np->name,cp) == 0)
	    return np;
    cp1 = new char[strlen(cp)+1];
    strcpy(cp1,cp);
    *npp = new Namen(cp1);
    np = *npp;
    return np;
}

Namen *Namen::neuerName(Namen **first,const char *cp,long i)
{
    Namen	*np;
    Namen	**npp;
    char	tmp[NAMELEN+NUMLEN];
    char	*cp1;

    sprintf(tmp,"%s_%ld",cp,i);
#ifdef DEBUG_COPY
    if (*first)
	cerr << "Nname:" << (*first)->name << '-' << tmp << endl;
    else
	cerr << "Nname:" << first << '-' << tmp << endl;
#endif
    npp = first;
    for (np = *first; np; npp = &np->next, np = np->next)
	if (strcmp(np->name,tmp) == 0)
	    return np;
    cp1 = new char[strlen(tmp)+1];
    strcpy(cp1,tmp);
    *npp = new Namen(cp1);
    np = *npp;
    return np;
}

static char	*oper_c[] = {
    "NOOP", "TMP", "VAR", "CONST", "CAT", "DISVAL", "EQUIV", "IMPL", "OR", "AND",
    "LESS", "GREATER", "EQUAL", "GE", "LE", "NE",
    "ADD", "SUB", "MUL", "DIV",
    "ASSIGN", "ASSNEXT",
    "ASSOR", "ASSEQUIV", "ASSDISV", "ASSIMPL",
    "ASSAND", "ASSADD", "ASSSUB", "ASSMUL", "ASSDIV",
    "INDEX", "FIELD", "CALL", "PARLIST", "GENSTRUCT",
    "STMNT", "IF", "WHILE",
    "FOR0", "FOR1", "FOR2", "FOR3", "FOR4", "FOR5", "FOR6",
    "BREAK", "CONTINUE", "EXIST", "ALL", "FIRST", "NEXT", "LAST",
    "COUNT", "POS", "NEG", "NOT", "ABS"
};

enum Precedences {
    P_NONE, P_ASSIGN, P_CAT, P_PARLIST, P_EQUIV, P_OR, P_AND,
    P_COMPARE, P_ADD, P_MUL, P_MONO, P_INDEX, P_CALL, P_VAR
};

static Precedences	pre[] = {
    P_NONE, P_VAR, P_VAR, P_VAR, P_CAT, P_EQUIV, P_EQUIV, P_EQUIV, P_OR, P_AND,
    P_COMPARE, P_COMPARE, P_COMPARE, P_COMPARE, P_COMPARE, P_COMPARE,
    P_ADD, P_ADD, P_MUL, P_MUL,
    P_ASSIGN, P_ASSIGN,
    P_ASSIGN, P_ASSIGN, P_ASSIGN, P_ASSIGN,
    P_ASSIGN, P_ASSIGN, P_ASSIGN, P_ASSIGN, P_ASSIGN,
    P_INDEX, P_INDEX, P_CALL, P_PARLIST, P_CALL,
    P_NONE, P_NONE, P_NONE,
    P_NONE, P_NONE, P_NONE, P_NONE, P_NONE, P_NONE, P_NONE,
    P_NONE, P_NONE, P_NONE, P_NONE, P_NONE, P_NONE, P_NONE,
    P_MONO, P_MONO, P_MONO, P_MONO, P_MONO
};

static char	*gen_c[] = {
    "", "%v", "%v", "%k", "%l,%r", "%l^%r", "error(EQUIV)", "error(IMPL)", "%l|%r", "%l&%r",
    "%l<%r", "%l>%r", "%l==%r", "%l>=%r", "%l<=%r", "%l!=%r",
    "%l+%r", "%l-%r", "%l*%r", "%l/%r",
    "%i%l = %r;\n", "%i%l += %r;\n",
    "%i%l |= %r;\n", "error(ASSEQUIV)", "%i%l ^= %r;\n", "error(ASSIMPL)",
    "%i%l &= %r;\n", "%i%l += %r;\n", "%i%l -= %r;\n", "%i%l *= %r;\n", "%i%l /= %r;\n",
    "%l[%R]", "%l.s%R", "%l(%R)", "%l,%r", "%v(%R)",
    "%l%r", "%iif (%L) {\n%>%R%<%i}\n", "%iwhile (%L) {\n%>%R%<%i}\n",
    "%iwhile (%L) {\n%>%R%<%i}\n",
    "%ifor (%v = 0; %v < %l; %v++) {\n%>%R%<%i}\n",
    "%ifor (%v = %l-1; %v >= 0; %v--) {\n%>%R%<%i}\n",
    "%ifor (%v = %l; %v < %r; %v++) {\n%>%B%<%i}\n",
    "%ifor (%v = %l; %v > %r; %v--) {\n%>%B%<%i}\n",
    "%ifor (%v = %l; %l <= %r ? %v < %r : %v > %r; %l <= %r ? %v++ : %v--) {\n%>%B%<%i}\n",
    "%iwhile (%l.count()) {\n%>%i%v = %l->first();\n%i%B%i%r;\n%<%i}\n",
    "%ibreak;\n", "%icontinue;\n", "error(EXIST)", "error(ALL)", "error(FIRST)", "error(NEXT)", "error(LAST)",
    "%R.count()", "+%r", "-%r", "!%r", "abs(%R)"
};

Expr::Expr(Oper o)
{
    oper = o;
    label = 0;
    var = NULL;
    konst = NULL;
    up = lhs = rhs = bhs = NULL;
    edef = NULL;
}

Expr::Expr(Oper o,Namen *np)
{
    oper = o;
    label = 0;
    var = np;
    konst = NULL;
    up = lhs = rhs = bhs = NULL;
    genDef();
}

Expr::Expr(Oper o,Konstante *kp)
{
    oper = o;
    label = 0;
    var = NULL;
    konst = kp;
    up = lhs = rhs = bhs = NULL;
    genDef();
}

Expr::Expr(Oper o,Expr *rp)
{
    oper = o;
    label = 0;
    var = NULL;
    konst = NULL;
    rhs = rp;
    if (rp)
	rp->up= this;
    up = lhs = bhs = NULL;
    genDef();
}

Expr::Expr(Oper o,Expr *lp,Expr *rp)
{
    oper = o;
    label = 0;
    var = NULL;
    konst = NULL;
    lhs = lp;
    if (lp)
	lp->up= this;
    rhs = rp;
    if (rp)
	rp->up= this;
    up = bhs = NULL;
    genDef();
}

Expr::Expr(Oper o,Expr *lp,Expr *rp,Expr *bp)
{
    oper = o;
    label = 0;
    var = NULL;
    konst = NULL;
    lhs = lp;
    if (lp)
	lp->up= this;
    rhs = rp;
    if (rp)
	rp->up= this;
    bhs = bp;
    if (bp)
	bp->up= this;
    up = NULL;
    genDef();
}

Expr::Expr(Oper o,Namen *np,Expr *rp)
{
    oper = o;
    label = 0;
    var = np;
    konst = NULL;
    rhs = rp;
    if (rp)
	rp->up= this;
    up = lhs = bhs = NULL;
    genDef();
}

Expr::Expr(Oper o,Namen *np,Expr *lp,Expr *rp)
{
    oper = o;
    label = 0;
    var = np;
    konst = NULL;
    lhs = lp;
    if (lp)
	lp->up= this;
    rhs = rp;
    if (rp)
	rp->up= this;
    up = bhs = NULL;
    genDef();
}

Expr::Expr(Oper o,Namen *np,Expr *lp,Expr *rp,Expr *bp)
{
    oper = o;
    label = 0;
    var = np;
    konst = NULL;
    lhs = lp;
    if (lp)
	lp->up= this;
    rhs = rp;
    if (rp)
	rp->up= this;
    bhs = bp;
    if (bp)
	bp->up= this;
    up = NULL;
    genDef();
}

void Expr::replace(const Expr &e,const Expr &e1)
{
    Expr	*ep;

    if (*this == e) {
	ep = new Expr;
	*ep = e1;
	if (up->lhs == this) {
	    up->lhs = ep;
	    ep->up = up;
	}
	if (up->rhs == this) {
	    up->rhs = ep;
	    ep->up = up;
	}
	if (up->bhs == this) {
	    up->bhs = ep;
	    ep->up = up;
	}
    }
    if (lhs)
	lhs->replace(e,e1);
    if (rhs)
	rhs->replace(e,e1);
    if (bhs)
	bhs->replace(e,e1);
}

void Expr::genDef()
{
    Def		*dp;

    switch (oper) {
    case TMP:
    case VAR:
	edef = var->def;
	break;
    case CONST:
	edef = konst->genDef();
	break;
    case CAT:
	if (!lhs->edef)
	    lhs->genDef();
	if (!rhs->edef)
	    rhs->genDef();
	if (lhs->edef && rhs->edef) {
	    dp = lhs->edef->ecopy();
	    dp->next = rhs->edef->ecopy();
	    edef = dp;
	} else
	    edef = NULL;
	break;
    case DISVAL:
    case EQUIV:
    case IMPL:
    case OR:
    case AND:
    case ADD:
    case SUB:
    case MUL:
    case DIV:
	if (!lhs->edef)
	    lhs->genDef();
	if (!rhs->edef)
	    rhs->genDef();
	if (lhs->edef && rhs->edef)
	    edef = dyad_typ(lhs->edef,rhs->edef);
	else
	    edef = NULL;
	break;
    case LESS:
    case GREATER:
    case EQUAL:
    case GE:
    case LE:
    case NE:
	edef = new Def(BIT);
	break;
    case INDEX:
	if (!lhs->edef)
	    lhs->genDef();
	if (lhs->edef)
	    edef = lhs->edef->genIndex(rhs);
	else
	    edef = NULL;
	break;
    case FIELD:
	if (!lhs->edef)
	    lhs->genDef();
	if (lhs->edef)
	    edef = lhs->edef->genField(rhs);
	else
	    edef = NULL;
	break;
    case CALL:
	if (lhs->oper == VAR) {
	    if (lhs->var->def->typ == FUNCTION) {
		dp = lhs->var->def->down->next;
		edef = dp;
	    } else if (lhs->var->def->typ == TYPEDEF && lhs->var->def->down &&
		    lhs->var->def->down->typ == FUNCTION) {
		dp = lhs->var->def->down->down->next;
		edef = dp;
	    } else
		edef = NULL;
	}
	break;
    case GENSTRUCT:
	if (!rhs->edef)
	    rhs->genDef();
	if (rhs->edef) {
	    dp = new Def(STRUCT);
	    dp->down = rhs->edef;
	    edef = dp;
	} else
	    edef = NULL;
	break;
    case COUNT:
	edef = new Def(INT);
	break;
    case NOT:
	edef = new Def(BIT);
	break;
    case POS:
    case NEG:
    case ABS:
	if (!rhs->edef)
	    rhs->genDef();
	if (rhs->edef)
	    edef = rhs->edef;
	else
	    edef = NULL;
	break;
    }
}

void Expr::print(int depth) const
{
    cerr << dindent;
    dind += 4;
    if (label)
	cerr << label << ':';
    cerr << oper_c[oper];
    if (oper == VAR || oper == TMP) {
	cerr << ' ';
	var->print(false);
	if (!edef)
	    cerr << "no def!";
	cerr << endl;
    } else if (oper == CONST) {
	cerr << ' ';
	konst->print();
	if (edef) {
	    cerr << " def:";
	    edef->print(false);
	}
	cerr << endl;
    } else if (oper == BREAK || oper == CONTINUE) {
	cerr << ' ';
	if (konst)
	    konst->print();
	if (edef) {
	    cerr << " def:";
	    edef->print(false);
	}
	if (bhs)
	    cerr << "->" << bhs->label;
	cerr << endl;
    } else {
	if (edef) {
	    cerr << " def:";
	    edef->print(false);
	}
	cerr << endl;
	if (depth && var) {
	    cerr << dindent;
	    var->print(true);
	    cerr << endl;
	}
	if (depth && lhs) {
	    if (lhs->up != this)
		cerr << "UP-ERROR:lhs!" << endl;
	    lhs->print(depth-1);
	}
	if (depth && rhs) {
	    if (rhs->up != this)
		cerr << "UP-ERROR:rhs!" << endl;
	    rhs->print(depth-1);
	}
	if (depth && bhs) {
	    if (bhs->up != this)
		cerr << "UP-ERROR:bhs!" << endl;
	    bhs->print(depth-1);
	}
    }
    dind -= 4;
}

void Expr::printx(char *s,Oper o)
{
    for (; *s; s++)
	if (*s == '%')
	    switch (*++s) {
	    case '\0':		// % at end ist a single %
		outs << '%';
		return;
	    case '%':		// this is a single %
		outs << *s;
		break;
	    case '>':		// increment indent
		ind++;
		break;
	    case '<':		// decrement indent
		ind--;
		break;
	    case 'i':		// do indent
		outs << xindent;
		break;
	    case 'v':		// var
		var->gen();
		break;
	    case 'k':		// konst
		konst->gen();
		break;
	    case 'l':		// lhs
		lhs->gen(o);
		break;
	    case 'r':		// rhs
		rhs->gen(o);
		break;
	    case 'b':		// bhs
		if (bhs)
		    bhs->gen(o);
		break;
	    case 'L':		// lhs
		lhs->gen();
		break;
	    case 'R':		// rhs
		rhs->gen();
		break;
	    case 'B':		// bhs
		if (bhs)
		    bhs->gen();
		break;
	    default:		// unknown sequence emit unchanged
		outs << '%' << *s;
		break;
	    }
	else
	    outs << *s;		// normal character
}

void Expr::get_pardef(int nr)
{
    if (nr == 0)
	lhs->edef->gen();
    else if (nr == 1)
	rhs->edef->gen();
    else
	rhs->get_pardef(nr-1);
}

void Expr::gen(Oper o)
{
    Namen	*np;
    int		par;
    bool	first;

    if (o != NOOP && pre[o] > pre[oper])
	outs << '(';
    switch (oper) {
    case ASSIGN:
	    if (lhs->oper == INDEX && lhs->lhs->oper == VAR &&
					lhs->lhs->var->def->realtype()->typ == BITVEKTOR) {
		lhs->printx("%i%L(%R,");
		printx("%R);\n");
	    } else
		printx(gen_c[oper],oper);
	    break;
    case INDEX:
	    if (lhs->edef && lhs->edef->realtype()->typ != VEKTOR && lhs->edef->basetype()->typ == STRUCT)
		printx(gen_c[FIELD],oper);
	    else
		printx(gen_c[oper],oper);
	    break;
    case FIELD:
	    if (lhs->edef && (lhs->edef->realtype()->typ == VEKTOR || lhs->edef->basetype()->typ == BITVEKTOR))
		printx(gen_c[INDEX],oper);
	    else
		printx(gen_c[oper],oper);
	    break;
    case GENSTRUCT:
	    if (var)
		printx(gen_c[oper],oper);
	    else if (edef->name) {
		edef->name->gen();
		outs << '(';
		rhs->gen();
		outs << ')';
	    }
	    break;
    case BREAK:
	    if (bhs)
		outs << xindent << "goto label" << bhs->label << ';' << endl;
	    else
		printx(gen_c[oper],oper);
	    break;
    default:
	    printx(gen_c[oper],oper);
	    break;
    }
    if (label)
	outs << "label" << label << ":;" << endl;
    if (o != NOOP && pre[o] > pre[oper])
	outs << ')';
}

void Expr::gen_bits()
{
    char	*cp;

    if (oper == CONST && konst->typ == BITS) {
	cp = konst->bval.gen_name();
	outs << "S1 " << cp << " = " << konst->bval << endl;
	delete cp;
    } else {
	if (lhs)
	    lhs->gen_bits();
	if (rhs)
	    rhs->gen_bits();
	if (bhs && oper != BREAK)
	    bhs->gen_bits();
    }
}

void Expr::gen_structs()	// TODO ganz anders machen!!! Strukturen vergleichen!
{
    char	*cp;
    Namen	*np;

    if (oper == ASSIGN) {
	if (lhs->edef && lhs->edef->realtype()->typ == STRUCT &&
		rhs->edef && rhs->edef->realtype()->typ == STRUCT) {
	    if (*lhs->edef == *rhs->edef)
		rhs->edef = lhs->edef;
	}
    }
    if (oper == ASSNEXT) {
	if (lhs->edef && lhs->edef->realtype()->typ == VEKTOR &&
		rhs->edef && rhs->edef->realtype()->typ == STRUCT) {
	    if (*lhs->edef->realtype()->down == *rhs->edef)
		rhs->edef = lhs->edef->realtype()->down;
	}
    }
    if (lhs)
	lhs->gen_structs();
    if (rhs)
	rhs->gen_structs();
    if (bhs && oper != BREAK)
	bhs->gen_structs();
}

void Expr::gen_fktmodels()
{
    Namen	*np;

    if (oper == CALL) {
	if (bhs && lhs->edef->down) {	// function template
	    np = lhs->var->fktcopy(bhs,rhs);
	    lhs->var = np;
	    np->gen_fkt();
	    genDef();
	} else if (lhs->oper == VAR && lhs->var->def->typ == TYPEDEF && lhs->var->def->name->def->down) {
	    for (np = lhs->var->def->name; np; np = np->next)
		if (np->def->isTemplate())
		    break;
	    if (np) {
		np = lhs->var->def->name->fktcopy(bhs,rhs);
		lhs->var = np;
		np->gen_fkt();
		genDef();
	    }
	} else if (lhs->edef->down) {
	    for (np = lhs->var->def->name; np; np = np->next)
		if (np->def && np->def->isTemplate())
		    break;
	    if (np) {
		np = lhs->var->fktcopy(bhs,rhs);
		lhs->var = np;
		np->gen_fkt();
		genDef();
	    }
	}
    }
    if (lhs)
	lhs->gen_fktmodels();
    if (rhs)
	rhs->gen_fktmodels();
    if (bhs)
	bhs->gen_fktmodels();
}

Expr *Expr::search(Oper o)
{
    Expr	*ep;

    if (oper == o)
	return this;
    else {
	if (lhs) {
	    ep = lhs->search(o);
	    if (ep)
		return ep;
	}
	if (rhs) {
	    ep = rhs->search(o);
	    if (ep)
		return ep;
	}
	if (bhs && oper != BREAK) {
	    ep = bhs->search(o);
	    if (ep)
		return ep;
	}
    }
    return NULL;
}

Expr *Expr::search_stmnt()
{
    Expr	*stp;

    for (stp = this; stp; stp = stp->up) {
	if (!stp->up)
	    return stp;
	switch (stp->oper) {
	case ASSIGN:
	case ASSNEXT:
	case ASSOR:
	case ASSEQUIV:
	case ASSDISV:
	case ASSIMPL:
	case ASSAND:
	case ASSADD:
	case ASSSUB:
	case ASSMUL:
	case ASSDIV:
	case STMNT:
	case IF:
	case WHILE:
	case FOR0:
	case FOR1:
	case FOR2:
	case FOR3:
	case FOR4:
	case FOR5:
	case FOR6:
	    return stp;
	default:
	    break;
	}
    }
    return NULL;
}

Expr *Expr::copy()
{
    Expr	*ep;

    if (this) {
#ifdef DEBUG_COPY
	cerr << "Ecopy>";
	print(1);
	dind++;
#endif
	if (oper == CONST)
	    ep = new Expr(CONST,konst);
	else
	    ep = new Expr(oper,var->copy(),lhs->copy(),rhs->copy(),bhs->copy());
	ep->label = label;
	ep->genDef();
#ifdef DEBUG_COPY
	dind--;
	cerr << "Ecopy<";
	ep->print(1);
#endif
	return ep;
    }
    return NULL;
}

Expr *Expr::ecopy()
{
    Expr	*ep;

    if (this) {
	if (oper == CONST)
	    ep = new Expr(CONST,konst);
	else
	    ep = new Expr(oper,var,lhs->ecopy(),rhs->ecopy(),bhs->ecopy());
	ep->label = label;
	ep->genDef();
	return ep;
    }
    return NULL;
}

static int	labno = 0;

void Expr::label_all()
{
    Expr	*ep;
    long	i;
    int		loops;

    if (this) {
	if (oper == BREAK && konst) {
	    ep = this;
	    loops = 0;
	    for (i = 0; i < konst->lval && ep && ep->up; i++) {
		ep = ep->up->search_stmnt();
		if (ep && ep->oper >= WHILE && ep->oper <= FOR6)
		    loops++;
	    }
	    if (ep) {
		if (loops == 0 && ep->up &&
			    ep->up->oper >= WHILE && ep->up->oper <= FOR6)
		    oper = CONTINUE;
		else if (!(loops == 1 && ep->oper >= WHILE && ep->oper <= FOR6)) {
		    ep->label = ++labno;
		    bhs = ep;
		}
	    }
	}
	lhs->label_all();
	rhs->label_all();
	if (oper != BREAK)
	    bhs->label_all();
    }
}

int Expr::operator ==(const Expr &e) const
{
    return oper == e.oper && var == e.var && konst == e.konst &&
	    (!lhs && !e.lhs || lhs && e.lhs && *lhs == *e.lhs) &&
	    (!rhs && !e.rhs || rhs && e.rhs && *rhs == *e.rhs) &&
	    (!bhs && !e.bhs || bhs && e.bhs && *bhs == *e.bhs);
}

Expr *restruct(Expr *exp)
{
    Expr	*exp1;

    while ((exp1 = restruct_assimpl(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_assequiv(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_impl(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_equiv(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_all(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_first(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_exist(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_next(exp)) != NULL)
	exp = exp1;
    while ((exp1 = restruct_last(exp)) != NULL)
	exp = exp1;
    return exp;
}

Expr *restruct_assimpl(Expr *exp)
{
    Expr	*implp;

    implp = exp->search(ASSIMPL);
    if (implp) {
	implp->oper = ASSIGN;
	implp->rhs = new Expr(OR,new Expr(NOT,implp->lhs->ecopy()),implp->rhs);
	implp->rhs->up = implp;
	return exp;
    }
    return NULL;
}

Expr *restruct_assequiv(Expr *exp)
{
    Expr	*implp;
    Expr	*ep;
    Expr	*ep1;

    implp = exp->search(ASSEQUIV);
    if (implp) {
	implp->oper = ASSDISV;
	ep = implp->rhs;
	if (ep->oper == NOT) {
	    implp->rhs = ep->rhs;
	    implp->rhs->up = implp;
	    implp->genDef();
	    delete ep;
	} else {
	    implp->rhs = new Expr(NOT,implp->rhs);
	    implp->rhs->up = implp;
	}
	return exp;
    }
    return NULL;
}

Expr *restruct_impl(Expr *exp)
{
    Expr	*implp;
    Expr	*ep;

    implp = exp->search(IMPL);
    if (implp) {
	implp->oper = OR;
	ep = implp->lhs;
	if (ep->oper == NOT) {
	    implp->lhs = ep->rhs;
	    implp->lhs->up = implp;
	    implp->genDef();
	    delete ep;
	} else {
	    implp->lhs = new Expr(NOT,implp->lhs);
	    implp->lhs->up = implp;
	}
	return exp;
    }
    return NULL;
}

Expr *restruct_equiv(Expr *exp)
{
    Expr	*equivp0;
    Expr	*equivp;
    Expr	*ep;
    Expr	*ep1;

    equivp = exp->search(EQUIV);
    if (equivp) {
	equivp0 = equivp->up;
	equivp->oper = DISVAL;
	if (!equivp0 || equivp0->oper != NOT) {
	    ep = new Expr(NOT,equivp);
	    ep->up = equivp0;
	    if (equivp0) {
		if (equivp0->lhs == equivp)
		    equivp0->lhs = ep;
		if (equivp0->rhs == equivp)
		    equivp0->rhs = ep;
		if (equivp0->bhs == equivp)
		    equivp0->bhs = ep;
		equivp0->genDef();
		return exp;
	    }
	    return ep;
	}
	ep1 = equivp0->up;
	delete equivp0;
	equivp->up = ep1;
	if (ep1) {
	    if (ep1->lhs == equivp0)
		ep1->lhs = equivp;
	    if (ep1->rhs == equivp0)
		ep1->rhs = equivp;
	    if (ep1->bhs == equivp0)
		ep1->bhs = equivp;
	    ep1->genDef();
	    return exp;
	}
	return equivp;
    }
    return NULL;
}

static int	locs = 0;

Expr *restruct_all(Expr *exp)
{
    Expr	*exp0;
    Expr	*stp;
    Expr	*stp0;
    Expr	*ep;
    Expr	*ep1;
    Namen	*np1;
    Namen	*np2 = NULL;
    Namen	*np3;

    exp0 = exp;
    exp = exp->search(ALL);
    if (!exp)
	return NULL;
    stp = exp->search_stmnt();
    if (stp) {
	np1 = Namen::neuerName(&function->def->name,"t",++locs);
	np1->def = new Def(BIT);
	ep = new Expr(ASSIGN,new Expr(TMP,np1),new Expr(CONST,new Konstante(0L)));
	ep = new Expr(STMNT,ep,new Expr(BREAK));
	if (exp->rhs->oper != NOT) {
	    ep1 = new Expr(NOT,exp->rhs);
	    ep = new Expr(IF,ep1,ep);
	} else {
	    ep = new Expr(IF,exp->rhs->rhs,ep);
	    delete exp->rhs;
	}
	np3 = Namen::neuerName(&function->def->name,"i",++locs);
	np3->def = new Def(INT);
	if (exp->lhs) {
	    if (exp->lhs->oper != VAR)
		np2 = Namen::neuerName(&function->def->name,"t",++locs);
	    ep1 = new Expr(INDEX,exp->lhs,new Expr(VAR,np3));
	    ep1 = new Expr(ASSIGN,new Expr(VAR,exp->var),ep1);
	    ep = new Expr(STMNT,ep1,ep);
	    ep1 = new Expr(COUNT,exp->lhs->ecopy());
	} else {
//	    ep1 = new Expr(INDEX,new Expr(VAR,exp->var),new Expr(VAR,np3));
	    ep->replace(Expr(VAR,exp->var),Expr(VAR,np3));
	    ep1 = new Expr(COUNT,new Expr(VAR,exp->var));
	}
	ep = new Expr(FOR1,np3,ep1,ep);
	ep1 = new Expr(ASSIGN,new Expr(TMP,np1),new Expr(CONST,new Konstante(1L)));
	ep = new Expr(STMNT,ep1,ep);
	stp0 = stp->up;
	ep = new Expr(STMNT,ep,stp);
	ep1 = exp->up;
	if (ep1->lhs == exp) {
	    delete ep1->lhs;
	    ep1->lhs = new Expr(TMP,np1);
	    ep1->lhs->up = ep1;
	}
	if (ep1->rhs == exp) {
	    delete ep1->rhs;
	    ep1->rhs = new Expr(TMP,np1);
	    ep1->rhs->up = ep1;
	}
	if (ep1->bhs == exp) {
	    delete ep1->bhs;
	    ep1->bhs = new Expr(TMP,np1);
	    ep1->bhs->up = ep1;
	}
	ep1->genDef();
	if (stp0) {
	    if (stp0->lhs == stp)
		stp0->lhs = ep;
	    if (stp0->rhs == stp)
		stp0->rhs = ep;
	    if (stp0->bhs == stp)
		stp0->bhs = ep;
	    ep->up = stp0;
	    stp0->genDef();
	} else
	    return ep;
    }
    return exp0;
}

Expr *restruct_first(Expr *exp)
{
    Expr	*firstp1;
    Expr	*exp0;
    Expr	*stp;
    Expr	*stp0;
    Expr	*ep;
    Expr	*ep1;
    Namen	*np;

    exp0 = exp;
    exp = exp->search(FIRST);
    if (!exp)
	return NULL;
    stp = exp->search_stmnt();
    if (stp) {
	firstp1 = exp->up;
	stp0 = stp->up;
	ep = new Expr(STMNT,stp,new Expr(BREAK));
	ep = new Expr(IF,exp->rhs,ep);
	np = Namen::neuerName(&function->def->name,"i",++locs);
	np->def = new Def(INT);
	ep1 = new Expr(INDEX,exp->lhs,new Expr(VAR,np));
	ep1 = new Expr(ASSIGN,new Expr(VAR,exp->var),ep1);
	ep = new Expr(STMNT,ep1,ep);
	ep1 = new Expr(COUNT,exp->lhs->ecopy());
	ep = new Expr(FOR1,np,ep1,ep);
	if (firstp1->lhs == exp) {
	    firstp1->lhs = new Expr(VAR,exp->var);
	    firstp1->lhs->up = firstp1;
	}
	if (firstp1->rhs == exp) {
	    firstp1->rhs = new Expr(VAR,exp->var);
	    firstp1->rhs->up = firstp1;
	}
	if (firstp1->bhs == exp) {
	    firstp1->bhs = new Expr(VAR,exp->var);
	    firstp1->bhs->up = firstp1;
	}
	firstp1->genDef();
	if (stp0) {
	    if (stp0->lhs == stp)
		stp0->lhs = ep;
	    if (stp0->rhs == stp)
		stp0->rhs = ep;
	    if (stp0->bhs == stp)
		stp0->bhs = ep;
	    ep->up = stp0;
	    stp0->genDef();
	} else
	    return ep;
    }
    return exp0;
}

Expr *restruct_exist(Expr *exp)
{
    Expr	*exp0;
    Expr	*stp;
    Expr	*stp0;
    Expr	*ep;
    Expr	*ep1;
    Namen	*np1;
    Namen	*np2 = NULL;
    Namen	*np3;

    exp0 = exp;
    exp = exp->search(EXIST);
    if (!exp)
	return NULL;
    stp = exp->search_stmnt();
    if (stp) {
	np1 = Namen::neuerName(&function->def->name,"t",++locs);
	np1->def = new Def(BIT);
	ep = new Expr(ASSIGN,new Expr(TMP,np1),new Expr(CONST,new Konstante(1L)));
	ep = new Expr(STMNT,ep,new Expr(BREAK));
	ep = new Expr(IF,exp->rhs,ep);
	np3 = Namen::neuerName(&function->def->name,"i",++locs);
	np3->def = new Def(INT);
	if (exp->lhs) {
	    if (exp->lhs->oper != VAR)
		np2 = Namen::neuerName(&function->def->name,"t",++locs);
	    ep1 = new Expr(INDEX,exp->lhs,new Expr(VAR,np3));
	    ep1 = new Expr(ASSIGN,new Expr(VAR,exp->var),ep1);
	    ep = new Expr(STMNT,ep1,ep);
	    ep1 = new Expr(COUNT,exp->lhs->ecopy());
	} else {
//	    ep1 = new Expr(INDEX,new Expr(VAR,exp->var),new Expr(VAR,np3));
	    ep->replace(Expr(VAR,exp->var),Expr(VAR,np3));
	    ep1 = new Expr(COUNT,new Expr(VAR,exp->var));
	}
	ep = new Expr(FOR1,np3,ep1,ep);
	ep1 = new Expr(ASSIGN,new Expr(TMP,np1),new Expr(CONST,new Konstante(0L)));
	ep = new Expr(STMNT,ep1,ep);
	stp0 = stp->up;
	ep = new Expr(STMNT,ep,stp);
	ep1 = exp->up;
	if (ep1->lhs == exp) {
	    delete ep1->lhs;
	    ep1->lhs = new Expr(TMP,np1);
	    ep1->lhs->up = ep1;
	}
	if (ep1->rhs == exp) {
	    delete ep1->rhs;
	    ep1->rhs = new Expr(TMP,np1);
	    ep1->rhs->up = ep1;
	}
	if (ep1->bhs == exp) {
	    delete ep1->bhs;
	    ep1->bhs = new Expr(TMP,np1);
	    ep1->bhs->up = ep1;
	}
	ep1->genDef();
	if (stp0) {
	    if (stp0->lhs == stp)
		stp0->lhs = ep;
	    if (stp0->rhs == stp)
		stp0->rhs = ep;
	    if (stp0->bhs == stp)
		stp0->bhs = ep;
	    ep->up = stp0;
	    stp0->genDef();
	} else
	    return ep;
    }
    return exp0;
}

Expr *restruct_next(Expr *exp)
{
    Expr	*nextp;
    Expr	*ep;
    Expr	*ep1;
    Namen	*np;

    nextp = exp->search(NEXT);
    if (nextp) {
	nextp->oper = FOR1;
	ep = new Expr(IF,nextp->rhs,nextp->bhs);
	np = Namen::neuerName(&function->def->name,"i",++locs);
	np->def = new Def(INT);
	ep1 = new Expr(INDEX,nextp->lhs,new Expr(VAR,np));
	ep1 = new Expr(ASSIGN,new Expr(VAR,nextp->var),ep1);
	ep = new Expr(STMNT,ep1,ep);
	nextp->bhs = NULL;
	nextp->rhs = ep;
	nextp->rhs->up = nextp;
	nextp->var = np;
	nextp->lhs = new Expr(COUNT,nextp->lhs->ecopy());
	nextp->lhs->up = nextp;
	return exp;
    }
    return NULL;
}

Expr *restruct_last(Expr *exp)
{
    Expr	*lastp;
    Expr	*ep;
    Expr	*ep1;
    Namen	*np;

    lastp = exp->search(LAST);
    if (lastp) {
	lastp->oper = FOR2;
	ep = new Expr(IF,lastp->rhs,lastp->bhs);
	np = Namen::neuerName(&function->def->name,"i",++locs);
	np->def = new Def(INT);
	ep1 = new Expr(INDEX,lastp->lhs,new Expr(VAR,np));
	ep1 = new Expr(ASSIGN,new Expr(VAR,lastp->var),ep1);
	ep = new Expr(STMNT,ep1,ep);
	lastp->bhs = NULL;
	lastp->rhs = ep;
	lastp->rhs->up = lastp;
	lastp->var = np;
	lastp->lhs = new Expr(COUNT,lastp->lhs->ecopy());
	lastp->lhs->up = lastp;
	return exp;
    }
    return NULL;
}
