#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string.h>
#include "plankal.h"
#include "plankal.cc.h"

extern Namen	*namen;
extern int	yydebug;

extern int	yyparse();

extern char	eingabezeile[];
extern short	eingabeindex;

int	ind = 0;

ostream& xindent(ostream& x)
{
    for (int i = 0; i < ind; i++)
	x << "\t";
    return x;
}

int	dind = 0;

ostream& dindent(ostream& x)
{
    for (int i = 0; i < dind; i++)
	x << ' ';
    return x;
}

ifstream	*ins;
ofstream	outs;

main(int argc,char *argv[])
{
    int		rc;
    char	*cp;
    char	cfile[NAMELEN];
    char	ipath[NAMELEN];

    yydebug = argc > 2;
    ins = new ifstream(argv[1]);
    strcpy(cfile,argv[1]);
    cp = strrchr(cfile,'.');
    if (cp)
	strcpy(cp+1,"cc");
    else
	strcat(cfile,".cc");
    outs.open(cfile);
    outs << "#include \"plan.h\"" << endl;
    rc = yyparse();
    cerr << "returncode: " << rc << endl;
    ins->close();
    if (rc) {
	eingabezeile[eingabeindex] = '\0';
	cerr << eingabezeile << endl;
    } else {
	for (Namen *np = namen; np; np = np->next) {
#ifdef DEBUG_FKT
	    np->print(true);
	    cerr << endl;
#endif
	    if (!np->defined && np->def && np->def->typ == FUNCTION) {
		Def	*dp;
		for (dp = np->def->down->down; dp; dp = dp->next)
		    if (dp->isTemplate())
			break;
		if (!dp)	// generate only if no template
		    np->gen_fkt();
	    }
	}
    }
    outs.close();
    cerr << "Global Function and Namelist" << endl;
    namen->druckeNamen(true);
    cerr << "Global Namelist" << endl;
    namen->druckeNamen(false);
    if (!rc) {
	strcpy(ipath,"-I");
	cp = strrchr(argv[0],'/');
	if (cp) {
	    *cp = '\0';
	    strcat(ipath,argv[0]);
	} else
	    strcat(ipath,".");
	cerr << "starting c++ compiler" << endl;
	execlp("g++","g++","-c",ipath,cfile,NULL);
//	execlp("g++","g++","-Wall","-W","-c",ipath,cfile,NULL);
    }
    return 0;
}

