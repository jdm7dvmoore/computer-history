#include <iostream.h>
#include <minmax.h>
#include <string.h>

#define BITS_PER_BYTE	(8)
#define	MSB_IN_BYTE	(1 << (BITS_PER_BYTE-1))
#define	COM_BYTE	((1 << BITS_PER_BYTE)-1)
#define	BITS2BYTES(x)	((x+BITS_PER_BYTE-1)/BITS_PER_BYTE)
#define	bitsof(x)	((short)(BITS_PER_BYTE*sizeof(x)))

typedef unsigned char	S0;

class S1
{
    friend ostream& operator << (ostream& o, S1 & b);
    friend istream& operator >> (istream& i, S1 & b);
    friend S1 & operator + (int x,const S1 & x1);
    friend S1 & operator + (long x,const S1 & x1);
    friend S1 & operator + (long long x,const S1 & x1);
    friend S1 & operator - (int x,const S1 & x1);
    friend S1 & operator - (long x,const S1 & x1);
    friend S1 & operator - (long long x,const S1 & x1);
    friend S1 & operator * (int x,const S1 & x1);
    friend S1 & operator * (long x,const S1 & x1);
    friend S1 & operator * (long long x,const S1 & x1);
    friend S1 & operator / (int x,const S1 & x1);
    friend S1 & operator / (long x,const S1 & x1);
    friend S1 & operator / (long long x,const S1 & x1);
    friend bool operator < (int x,const S1 & x1);
    friend bool operator < (long x,const S1 & x1);
    friend bool operator < (long long x,const S1 & x1);
    friend bool operator > (int x,const S1 & x1);
    friend bool operator > (long x,const S1 & x1);
    friend bool operator > (long long x,const S1 & x1);
    friend bool operator <= (int x,const S1 & x1);
    friend bool operator <= (long x,const S1 & x1);
    friend bool operator <= (long long x,const S1 & x1);
    friend bool operator >= (int x,const S1 & x1);
    friend bool operator >= (long x,const S1 & x1);
    friend bool operator >= (long long x,const S1 & x1);
    friend bool operator == (int x,const S1 & x1);
    friend bool operator == (long x,const S1 & x1);
    friend bool operator == (long long x,const S1 & x1);
    friend bool operator != (int x,const S1 & x1);
    friend bool operator != (long x,const S1 & x1);
    friend bool operator != (long long x,const S1 & x1);
public:
    inline S1()
    {
	len = blen = 0;
	bits = NULL;
    }
    S1(long l)
    {
	len = l;
	blen = BITS2BYTES(len);
	if (blen) {
	    bits = new S0[blen];
	    memset(bits,0,blen*sizeof(S0));
	} else
	    bits = NULL;
    }
    S1(long l,unsigned char f, ...)
    {
	va_list ap;
	unsigned char *bp;

	va_start(ap,f);
	len = l;
	blen = BITS2BYTES(len);
	bp = bits = new unsigned char [blen];
	*bp++ = f;
	for (l = 0; l < blen; l++)
	    *bp++ = va_arg(ap,unsigned char);
	va_end(ap);
    }
    inline ~S1()
    {
	delete [] bits;
    }
    inline int sign() const
    {
	return blen && (bits[blen-1] & MSB_IN_BYTE) != 0;
    }
    inline bool isMinus() const
    {
	return sign();
    }
    inline bool isPlus() const
    {
	return !isMinus();
    }
    bool isNull() const
    {
	long	l;
	S0	*bp;

	for (bp = bits, l = 0; l < blen; bp++, l++)
	    if (*bp)
		return false;
	return true;
    }
    inline int S1::count() const
    {
	return len;
    }
    inline operator S0 () const
    {
	if (len > 0)
	    return *bits & 1;
	else
	    return 0;
    }
    inline operator int () const
    {
	return operator long();
    }
    operator long () const
    {
	short		i;
	long		l;
	long		x;
	S0		*bp;

	bp = bits;
	x = 0;
	for (i = 0, l = blen; i < bitsof(x) && l; i += BITS_PER_BYTE, l--)
	    x |= (unsigned long)*bp++ << i;
	if (isMinus()) {
	    for (; i < bitsof(x); i += BITS_PER_BYTE)
		x |= (unsigned long)COM_BYTE << i;
	    if (x >= 0)
		throw "cast to a smaller variable";
	    for (; l; l--)
		if (*bp++ != COM_BYTE)
		    throw "cast to a smaller variable";
	} else
	    for (; l; l--)
		if (*bp++ != 0)
		    throw "cast to a smaller variable";
	return x;
    }
    operator long long () const
    {
	short		i;
	long		l;
	long long	x;
	S0		*bp;

	bp = bits;
	x = 0;
	for (i = 0, l = blen; i < bitsof(x) && l; i += BITS_PER_BYTE, l--)
	    x |= (unsigned long long)*bp++ << i;
	if (isMinus()) {
	    for (; i < bitsof(x); i += BITS_PER_BYTE)
		x |= (unsigned long long)COM_BYTE << i;
	    if (x >= 0)
		throw "cast to a smaller variable";
	    for (; l; l--)
		if (*bp++ != COM_BYTE)
		    throw "cast to a smaller variable";
	} else
	    for (; l; l--)
		if (*bp++ != 0)
		    throw "cast to a smaller variable";
	return x;
    }
    S1 &operator |= (const S1 & x)
    {
	long	l;
	short	extra;
	S0	*bp0;
	S0	*bp1;

	if (bits == NULL) {
	    len = x.len;
	    blen = x.blen;
	    bits = new S0[blen];
	    memset(bits,0,blen);
	}
	l = x.bitcount(extra);
	if (len < l)
	    throw "assignment to a smaller variable";
	bp0 = bits;
	bp1 = x.bits;
	for (l = 0; l < x.blen; l++)
	    *bp0++ |= *bp1++;
	memset(bp0,x.isMinus() ? COM_BYTE : 0,(x.blen-l)*sizeof(S0));
	return *this;
    }
    S1 &operator &= (const S1 & x)
    {
	long	l;
	short	extra;
	S0	*bp0;
	S0	*bp1;

	if (bits == NULL) {
	    len = x.len;
	    blen = x.blen;
	    bits = new S0[blen];
	    memset(bits,0,blen);
	}
	l = x.bitcount(extra);
	if (len < l)
	    throw "assignment to a smaller variable";
	bp0 = bits;
	bp1 = x.bits;
	for (l = 0; l < x.blen; l++)
	    *bp0++ &= *bp1++;
	memset(bp0,x.isMinus() ? COM_BYTE : 0,(x.blen-l)*sizeof(S0));
	return *this;
    }
    S1 &operator ^= (const S1 & x)
    {
	long	l;
	short	extra;
	S0	*bp0;
	S0	*bp1;

	if (bits == NULL) {
	    len = x.len;
	    blen = x.blen;
	    bits = new S0[blen];
	    memset(bits,0,blen);
	}
	l = x.bitcount(extra);
	if (len < l)
	    throw "assignment to a smaller variable";
	bp0 = bits;
	bp1 = x.bits;
	for (l = 0; l < x.blen; l++)
	    *bp0++ ^= *bp1++;
	memset(bp0,x.isMinus() ? COM_BYTE : 0,(x.blen-l)*sizeof(S0));
	return *this;
    }
    S1 &operator += (const S1 & x)
    {
	long	l;
	short	extra;
	short	i;
	short	minus;
	S0	*bp0;
	S0	*bp1;

	if (bits == NULL) {
	    len = x.len;
	    blen = x.blen;
	    bits = new S0[blen];
	    memset(bits,0,blen);
	}
	l = x.bitcount(extra);
	if (len < l)
	    throw "assignment to a smaller variable";
	minus = x.isMinus() ? -1 : 0;
	bp0 = bits;
	bp1 = x.bits;
	i = 0;
	for (l = 0; l < x.blen; l++) {
	    i = *bp0 + *bp1++ + i;
	    *bp0++ = i;
	    i >>= BITS_PER_BYTE;
	}
	for (; l < blen; l++) {
	    i = *bp0 + minus + i;
	    *bp0++ = i;
	    i >>= BITS_PER_BYTE;
	}
	return *this;
    }
    S1 &operator -= (const S1 & x)
    {
	long	l;
	short	extra;
	short	i;
	short	minus;
	S0	*bp0;
	S0	*bp1;

	if (bits == NULL) {
	    len = x.len;
	    blen = x.blen;
	    bits = new S0[blen];
	    memset(bits,0,blen);
	}
	l = x.bitcount(extra);
	if (len < l)
	    throw "assignment to a smaller variable";
	minus = x.isMinus() ? -1 : 0;
	bp0 = bits;
	bp1 = x.bits;
	i = 0;
	for (l = 0; l < x.blen; l++) {
	    i = *bp0 - *bp1++ + i;
	    *bp0++ = i;
	    i >>= BITS_PER_BYTE;
	}
	for (; l < blen; l++) {
	    i = *bp0 - minus + i;
	    *bp0++ = i;
	    i >>= BITS_PER_BYTE;
	}
	return *this;
    }
    inline S1 &operator *= (const S1 &x)	// TODO nur tempor�r so
    {
	S1	tmp;

	tmp = *this * x;
	*this = tmp;
	return *this;
    }
    inline S1 &operator /= (const S1 &x)	// TODO nur tempor�r so
    {
	S1	tmp;

	tmp = *this / x;
	*this = tmp;
	return *this;
    }
    S1 &operator >>= (int i)
    {
	long		l;
	long		l1;
	int		i1;
	S0		*bp;
	unsigned short	x;
	short		minus;

	if (bits == NULL) {
	    len = 1;
	    blen = 1;
	    bits = new S0[blen];
	    *bits = 0;
	}
	i1 = i/BITS_PER_BYTE;
	l1 = blen-i1;
	minus = isMinus() ? -1 : 0;
	if (i1) {
	    memmove(bits,bits+i1,l1);
	    memset(bits+l1,minus,i1);
	}
	i %= BITS_PER_BYTE;
	if (i) {
	    for (bp = bits, l = 1; l < l1; l++) {
		x = (*(bp+1) << BITS_PER_BYTE) | *bp;
		*bp++ = (S0)(x >> i);
	    }
	    if (i1)
		x = *(bp+1) << BITS_PER_BYTE;
	    else
		x = minus << BITS_PER_BYTE;
	    *bp = (S0)((x | *bp) >> i);
	}
	return *this;
    }
    S1 &operator <<= (int i)
    {
	long		l;
	long		l1;
	int		i1;
	S0		*bp;
	unsigned short	x;

	if (bits == NULL) {
	    len = 1;
	    blen = 1;
	    bits = new S0[blen];
	    *bits = 0;
	}
	i1 = i/BITS_PER_BYTE;
	l1 = blen-i1;
	if (i1) {
	    memmove(bits+i1,bits,l1);
	    memset(bits,0,i1);
	}
	i %= BITS_PER_BYTE;
	if (i) {
	    x = 0;
	    for (bp = bits+i1, l = 0; l < l1; l++) {
		x |= (unsigned short)*bp << i;
		*bp++ = (S0)x;
		x >>= BITS_PER_BYTE;
	    }
	    if (l1+i1 < blen)
		*bp = (S0)x;
	}
	return *this;
    }
    S1 &operator = (const S1 &x)
    {
	long	l;
	short	extra;

	if (bits == NULL) {
	    len = x.len;
	    blen = x.blen;
	    bits = new S0[blen];
	}
	l = x.bitcount(extra);
	if (len < l)
	    throw "assignment to a smaller variable";
	memset(bits,x.isMinus() ? COM_BYTE : 0,blen*sizeof(S0));
	if (x.bits)
	    memcpy(bits,x.bits,BITS2BYTES(l));
	return *this;
    }
    S1 &operator = (long long x)
    {
	long long	x1;
	S0		*bp;
	short		l;
	short		extra;

	x1 = x;
	extra = 0;
	if (x1 < 0) {
	    x1 = ~x1;
	    for (l = 1; x1; x1 >>= 1)
		l++;
	} else if (x1) {
	    extra = 1;
	    for (l = 0; x1; x1 >>= 1)
		l++;
	} else
	    l = 1;
	if (bits == NULL) {
	    l += extra;
	    len = l;
	    blen = BITS2BYTES(len);
	    bits = new S0[blen];
	}
	if (len < l)
	    throw "assignment to a smaller variable";
	memset(bits,x < 0 ? COM_BYTE : 0,blen*sizeof(S0));
	bp = bits;
	for (l = BITS2BYTES(l); l; l--) {
	    *bp++ = (S0)x;
	    x >>= BITS_PER_BYTE;
	}
	return *this;
    }
    S1 &operator | (const S0 x) const
    {
	S1	*res;

	res = new S1(max(len,1L));
	memcpy(res->bits,bits,res->blen);
	*res->bits |= x & 1;
	return *res;
    }
    S1 &operator | (const S1 & x) const
    {
	S1	*res;
	long	l;
	long	l1;
	bool	minus;
	S0	*bp0;
	S0	*bp1;
	S0	*bp2;

	res = new S1(max(len,x.len));
	bp0 = bits;
	bp1 = x.bits;
	bp2 = res->bits;
	l1 = min(blen,x.blen);
	for (l = 0; l < l1; l++)
	    *bp2++ = *bp0++ | *bp1++;
	if (blen < x.blen) {
	    bp0 = bp1;
	    minus = isMinus();
	} else
	    minus = x.isMinus();
	for (; l < res->blen; l++)
	    *bp2++ = minus ? COM_BYTE : *bp0++;
	return *res;
    }
    S1 &operator & (const S0 x) const
    {
	S1	*res;

	res = new S1(max(len,1L));
	memcpy(res->bits,bits,res->blen);
	*res->bits &= x & 1;
	return *res;
    }
    S1 &operator & (const S1 & x) const
    {
	S1	*res;
	long	l;
	long	l1;
	bool	minus;
	S0	*bp0;
	S0	*bp1;
	S0	*bp2;

	res = new S1(max(len,x.len));
	bp0 = bits;
	bp1 = x.bits;
	bp2 = res->bits;
	l1 = min(blen,x.blen);
	for (l = 0; l < l1; l++)
	    *bp2++ = *bp0++ & *bp1++;
	if (blen < x.blen) {
	    bp0 = bp1;
	    minus = isMinus();
	} else
	    minus = x.isMinus();
	for (; l < res->blen; l++)
	    *bp2++ = minus ? *bp0++ : 0;
	return *res;
    }
    S1 &operator ^ (const S0 x) const
    {
	S1	*res;

	res = new S1(max(len,1L));
	memcpy(res->bits,bits,res->blen);
	*res->bits &= x & 1;
	return *res;
    }
    S1 &operator ^ (const S1 & x) const
    {
	S1	*res;
	long	l;
	long	l1;
	bool	minus;
	S0	*bp0;
	S0	*bp1;
	S0	*bp2;

	res = new S1(max(len,x.len));
	bp0 = bits;
	bp1 = x.bits;
	bp2 = res->bits;
	l1 = min(blen,x.blen);
	for (l = 0; l < l1; l++)
	    *bp2++ = *bp0++ ^ *bp1++;
	if (blen < x.blen) {
	    bp0 = bp1;
	    minus = isMinus();
	} else
	    minus = x.isMinus();
	for (; l < res->blen; l++)
	    *bp2++ = minus ? *bp0++ : 0;
	return *res;
    }
    S1 &operator + (const S1 & x) const
    {
	S1	*res;
	long	l;
	long	l1;
	short	i;
	short	minus;
	short	minus1;
	S0	*bp0;
	S0	*bp1;
	S0	*bp2;

	res = new S1(max(len,x.len)+1);
	bp0 = bits;
	bp1 = x.bits;
	bp2 = res->bits;
	minus = isMinus() ? -1 : 0;
	minus1 = x.isMinus() ? -1 : 0;
	l1 = min(blen,x.blen);
	i = 0;
	for (l = 0; l < l1; l++) {
	    i = *bp0++ + *bp1++ + i;
	    *bp2++ = i;
	    i >>= BITS_PER_BYTE;
	}
	for (; l < res->blen; l++) {
	    if (l < blen)
		i = *bp0++ + minus1 + i;
	    else
		i = minus + *bp1++ + i;
	    *bp2++ = i;
	    i >>= BITS_PER_BYTE;
	}
	return *res;
    }
    S1 &operator + (int x) const
    {
	return *this + (S1)x;
    }
    S1 &operator + (long x) const
    {
	return *this + (S1)x;
    }
    S1 &operator + (long long x) const
    {
	return *this + (S1)x;
    }
    S1 &operator - (const S1 & x) const
    {
	S1	*res;
	long	l;
	long	l1;
	short	i;
	short	minus;
	short	minus1;
	S0	*bp0;
	S0	*bp1;
	S0	*bp2;

	res = new S1(max(len,x.len)+1);
	bp0 = bits;
	bp1 = x.bits;
	bp2 = res->bits;
	minus = isMinus() ? -1 : 0;
	minus1 = x.isMinus() ? -1 : 0;
	l1 = min(blen,x.blen);
	i = 0;
	for (l = 0; l < l1; l++) {
	    i = *bp0++ - *bp1++ + i;
	    *bp2++ = i;
	    i >>= BITS_PER_BYTE;
	}
	for (; l < res->blen; l++) {
	    if (l < blen)
		i = *bp0++ - minus1 + i;
	    else
		i = minus - *bp1++ + i;
	    *bp2++ = i;
	    i >>= BITS_PER_BYTE;
	}
	return *res;
    }
    S1 &operator - (int x) const
    {
	return *this - (S1)x;
    }
    S1 &operator - (long x) const
    {
	return *this - (S1)x;
    }
    S1 &operator - (long long x) const
    {
	return *this - (S1)x;
    }
    S1 &operator * (const S0 x) const
    {
	S1	*res;

	res = new S1;
	if (x)
	    *res = *this;
	else
	    *res = 0L;
	return *res;
    }
    S1 &operator * (const S1 & x) const
    {
	S1	*res;
	long	l;
	short	extra;
	long	l1;
	short	extra1;
	bool	minus;

	minus = isMinus();
	l = bitcount(extra);
	l1 = l + x.bitcount(extra1) - 1;
	l1 += extra|extra1;
	S1 tmp(l1);
	tmp = x;
	if (minus) {
	    S1	tmp1(l1);
	    for (l1 = 0; l1 < l; l1++) {
		if (!(*this)[l1])
		    tmp1 += tmp;
		tmp <<= 1;
	    }
	    tmp1 += x;
	    return -tmp1;
	} else {
	    res = new S1(l1);
	    for (l1 = 0; l1 < l; l1++) {
		if ((*this)[l1])
		    *res += tmp;
		tmp <<= 1;
	    }
	    return *res;
	}
    }
    inline S1 &operator * (int x) const
    {
	return *this * (S1)x;
    }
    inline S1 &operator * (long x) const
    {
	return *this * (S1)x;
    }
    inline S1 &operator * (long long x) const
    {
	return *this * (S1)x;
    }
    S1 &operator / (const S0 x) const
    {
	S1	*res;

	res = new S1;
	if (x)
	    *res = *this;
	else
	    throw "Division by 0";
	return *res;
    }
    S1 &operator / (const S1 & x) const	// TODO negative Werte
    {
	S1	*res;
	long	l;
	short	extra;
	long	l1;
	short	extra1;
	bool	minus;

	l = bitcount(extra);
	l1 = x.bitcount(extra1);
	if (l1 == 1 && (x.bits == NULL || *x.bits == 0))
	    throw "Division by 0";
	if (l >= l1) {
	    res = new S1(l-l1+2);
	    S1 tmp(l+extra);
	    S1 tmp1(l+extra);
	    minus = isMinus();
	    if (minus) {
		l--;
		tmp = -*this;
	    } else
		tmp = *this;
	    if (x.isMinus()) {
		minus = !minus;
		l1--;
		tmp1 = -(S1&)x;
	    } else
		tmp1 = x;
	    l -= l1;
	    tmp1 <<= l;
	    for (; l >= 0; l--) {
		*res <<= 1;
		if (tmp >= tmp1) {
		    tmp -= tmp1;
		    *res->bits |= 1;
		}
		tmp1 >>= 1;
	    }
	    if (minus)
		*res = -*res;
	} else
	    res = new S1(1);
	return *res;
    }
    inline S1 &operator / (int x) const
    {
	return *this / (S1)x;
    }
    inline S1 &operator / (long x) const
    {
	return *this / (S1)x;
    }
    inline S1 &operator / (long long x) const
    {
	return *this / (S1)x;
    }
    inline bool operator < (const S1 & x) const
    {
	return compare(*this,x) < 0;
    }
    inline bool operator > (const S1 & x) const
    {
	return compare(*this,x) > 0;
    }
    inline bool operator == (const S1 & x) const
    {
	return compare(*this,x) == 0;
    }
    inline bool operator <= (const S1 & x) const
    {
	return compare(*this,x) <= 0;
    }
    inline bool operator >= (const S1 & x) const
    {
	return compare(*this,x) >= 0;
    }
    inline bool operator != (const S1 & x) const
    {
	return compare(*this,x) != 0;
    }
    inline bool operator < (int x) const
    {
	return *this < (S1)x;
    }
    inline bool operator < (long x) const
    {
	return *this < (S1)x;
    }
    inline bool operator < (long long x) const
    {
	return *this < (S1)x;
    }
    inline bool operator > (int x) const
    {
	return *this > (S1)x;
    }
    inline bool operator > (long x) const
    {
	return *this > (S1)x;
    }
    inline bool operator > (long long x) const
    {
	return *this > (S1)x;
    }
    inline bool operator <= (int x) const
    {
	return *this <= (S1)x;
    }
    inline bool operator <= (long x) const
    {
	return *this <= (S1)x;
    }
    inline bool operator <= (long long x) const
    {
	return *this <= (S1)x;
    }
    inline bool operator >= (int x) const
    {
	return *this >= (S1)x;
    }
    inline bool operator >= (long x) const
    {
	return *this >= (S1)x;
    }
    inline bool operator >= (long long x) const
    {
	return *this >= (S1)x;
    }
    inline bool operator == (int x) const
    {
	return *this == (S1)x;
    }
    inline bool operator == (long x) const
    {
	return *this == (S1)x;
    }
    inline bool operator == (long long x) const
    {
	return *this == (S1)x;
    }
    inline bool operator != (int x) const
    {
	return *this != (S1)x;
    }
    inline bool operator != (long x) const
    {
	return *this != (S1)x;
    }
    inline bool operator != (long long x) const
    {
	return *this != (S1)x;
    }
    S1 &operator >> (int i) const
    {
	S1		*res;
	long		l;
	long		l1;
	S0		*bp;
	unsigned short	x;
	short		extra;
	short		minus;

	l = bitcount(extra);
	l += extra;
	l1 = BITS2BYTES(l);
	l -= i;
	minus = isMinus() ? -1 : 0;
	if (l <= 0)
	    l = 1;
	res = new S1(l);
	if (bits) {
	    bp = bits+i/BITS_PER_BYTE;
	    memcpy(res->bits,bp,res->blen);
	}
	i %= BITS_PER_BYTE;
	if (i) {
	    for (bp = res->bits, l = 1; l < res->blen; l++) {
		x = (*(bp+1) << BITS_PER_BYTE) | *bp;
		*bp++ = (S0)(x >> i);
	    }
	    if (blen > l1)
		x = *(bits+l1-1) << BITS_PER_BYTE;
	    else
		x = minus << BITS_PER_BYTE;
	    *bp = (S0)((x | *bp) >> i);
	}
	return *res;
    }
    S1 &operator << (int i) const
    {
	S1		*res;
	long		l;
	long		l1;
	int		i1;
	S0		*bp;
	unsigned short	x;
	short		extra;

	l = bitcount(extra);
	l += extra;
	l1 = BITS2BYTES(l);
	l += i;
	i1 = i/BITS_PER_BYTE;
	res = new S1(l);
	bp = res->bits+i1;
	if (bits)
	    memcpy(bp,bits,l1);
	i %= BITS_PER_BYTE;
	if (i) {
	    x = 0;
	    for (l = 0; l < l1; l++) {
		x |= (unsigned short)*bp << i;
		*bp++ = (S0)x;
		x >>= BITS_PER_BYTE;
	    }
	    if (l1+i1 < res->blen)
		*bp = (S0)x;
	}
	return *res;
    }
    S1 &operator ~ () const
    {
	S1	*res;
	long	l;
	S0	*bp;
	S0	*bp0;

	res = new S1(len);
	bp0 = bits;
	for (bp = res->bits, l = 0; l < res->blen; l++)
	    *bp++ = ~*bp0++;
	return *res;
    }
    S1 &operator - () const
    {
	S1	*res;
	long	l;
	S0	*bp;
	S0	*bp0;
	short	x;

	res = new S1(len);
	bp0 = bits;
	x = 1;
	for (bp = res->bits, l = 0; l < res->blen; l++) {
	    x = (unsigned char)~*bp0++ + x;
	    *bp++ = x;
	    x >>= BITS_PER_BYTE;
	}
	return *res;
    }
    inline S1 &operator + () const
    {
	S1	*res;
	long	l;
	S0	*bp;
	S0	*bp0;
	short	x;

	res = new S1(len);
	bp0 = bits;
	x = 1;
	for (bp = res->bits, l = 0; l < res->blen; l++) {
	    x = (unsigned char)~*bp0++ + x;
	    *bp++ = x;
	    x >>= BITS_PER_BYTE;
	}
	return *res;
    }
    inline bool operator ! () const
    {
	return !isNull();
    }
    inline S0 operator [] (int i) const
    {
	indexCheck(i,len);
	return (bits[i/BITS_PER_BYTE] >> (i%BITS_PER_BYTE)) & 1;
    }
    inline S0 operator () (int i,int x)
    {
	setbit(i,x);
	return x;
    }
    void setbit(int i,S0 bit)
    {
	S0	*bp;

	indexCheck(i,len);
	bp = &bits[i/BITS_PER_BYTE];
	i %= BITS_PER_BYTE;
	*bp = (*bp & ~(1 << i)) | (bit << i);
    }
private:
    S0		*bits;
    long	len;
    long	blen;

    void indexCheck(int i, int n) const
    {
	if (i < 0 || i >= n)
	    throw "index error";
    }

    long bitcount(short &extra) const
    {
	long		l;
	S0		*bp;
	S0		x;
	signed char	xs;

	if (isMinus()) {
	    for (bp = bits+(blen-1); bp != bits; bp--)
		if (*bp != COM_BYTE)
		    break;
	    l = (bp-bits)*BITS_PER_BYTE+1;
	    if ((*bp & MSB_IN_BYTE) == 0)
		l += BITS_PER_BYTE;
	    else
		for (xs = *bp; xs != -1; xs >>= 1)
		    l++;
	    extra = 0;
	} else {
	    if (blen) {
		for (bp = bits+(blen-1); bp != bits; bp--)
		    if (*bp != 0)
			break;
		l = (bp-bits)*BITS_PER_BYTE;
		for (x = *bp; x; x >>= 1)
		    l++;
	    } else
		l = 0;
	    extra = l != 0;
	}
	return l == 0 ? 1 : l;
    }
    //	x0 > x1  => >0
    //	x0 < x1  => <0
    //	x0 = x1  => 0
    int compare(const S1 &x0,const S1 &x1) const
    {
	long		l;
	S0		*bp0;
	S0		*bp1;
	bool		minusl;
	bool		minusr;
	signed char	diff;

	bp0 = x0.bits+x0.blen;
	bp1 = x1.bits+x1.blen;
	minusl = false;
	minusr = false;
	if (x0.blen > x1.blen)
	    for (l = x0.blen; l > x1.blen; l--) {
		if ((signed char)*--bp0 > 0)
		    return minusl ? -1 : 1;
		if (*bp0 == COM_BYTE)
		    minusl = true;
		else if ((signed char)*bp0 < 0)
		    return -1;
	    }
	else
	    for (l = x1.blen; l > x0.blen; l--) {
		if ((signed char)*--bp1 > 0)
		    return minusr ? 1 : -1;
		if (*bp1 == COM_BYTE)
		    minusr = true;
		else if ((signed char)*bp1 < 0)
		    return 1;
	    }
	for (; l; l--)
	    if ((diff = *--bp0 - *--bp1) != 0) {
		if (minusl)
		    return -1;
		if (minusr)
		    return 1;
		return diff;
	    }
	return 0;
    }
};

ostream& operator << (ostream& o, S1 & b)
{
    long	i;

    if (b.len == 0)
	o << '0';
    else
	for (i = b.len-1; i >= 0; i--) {
	    o << (b[i] ? 'L' : '0');
	    if (i != 0 && i % BITS_PER_BYTE == 0)
		o << '.';
	}
    return o;
}

istream& operator >> (istream& i, S1 & b)
{
    S0		*buf;
    register S0	*tp;
    S0		*tpa;
    short	bit;

    if (b.blen) {
	buf = NULL;
	tpa = tp = b.bits+b.blen;
    } else {
	buf = new S0[256];	// temp. buffer
	tpa = tp = buf+sizeof(S0)*256;
    }
    bit = 0;
    if (i.ipfx0()) {
	register streambuf* sb = i._strbuf;

	while (true) {
	    int ch = sb->sbumpc();

	    if (ch == EOF) {
		i.set(ios::eofbit);
		break;
	    } else if (ch == '0' || ch == '1' || ch == 'L') {
		if (bit--)
		    *tp <<= 1;
		else {
		    *--tp = 0;
		    bit = BITS_PER_BYTE-1;
		}
		*tp |= ch == '0' ? 0 : 1;
	    } else if (ch != '.') {
		sb->sputbackc(ch);
		break;
	    }
	}
	if (bit == 0 && tp == tpa) {
	    i.set(ios::failbit);
	    delete [] buf;
	    return i;
	}
	*tp <<= bit;
	if (buf) {
	    b.blen = tpa-tp;
	    b.len = b.blen*BITS_PER_BYTE;	// use whole bytes
	    b.bits = new S0[b.blen];
	    memcpy(b.bits,tp,b.blen);
	    b >>= bit;
	    b.len = b.blen*BITS_PER_BYTE-bit;	// true len
	} else {
	    long tmplen = b.len;

	    b.len = b.blen*BITS_PER_BYTE;	// use whole bytes
	    b >>= bit+(b.blen-(tpa-tp))*BITS_PER_BYTE;
	    b.len = tmplen;			// back to the true len
	}
    }
    delete [] buf;
    return i;
}

inline S1 &operator + (int x,const S1 & x1)
{
    return x1 + x;
}

inline S1 &operator + (long x,const S1 & x1)
{
    return x1 + x;
}

inline S1 &operator + (long long x,const S1 & x1)
{
    return x1 + x;
}

inline S1 &operator - (int x,const S1 & x1)
{
    return -x1 + x;
}

inline S1 &operator - (long x,const S1 & x1)
{
    return -x1 + x;
}

inline S1 &operator - (long long x,const S1 & x1)
{
    return -x1 + x;
}

inline S1 &operator * (int x,const S1 & x1)
{
    return x1 * x;
}

inline S1 &operator * (long x,const S1 & x1)
{
    return x1 * x;
}

inline S1 &operator * (long long x,const S1 & x1)
{
    return x1 * x;
}

inline S1 &operator / (int x,const S1 & x1)
{
    return x1 + x;
}

inline S1 &operator / (long x,const S1 & x1)
{
    return x1 + x;
}

inline S1 &operator / (long long x,const S1 & x1)
{
    return x1 + x;
}

inline S1 &operator | (const S0 x,const S1 & x1)
{
    return x1 | x;
}

inline S1 &operator & (const S0 x,const S1 & x1)
{
    return x1 & x;
}

inline bool operator < (int x,const S1 & x1)
{
    return !(x1 >= x);
}

inline bool operator < (long x,const S1 & x1)
{
    return !(x1 >= x);
}

inline bool operator < (long long x,const S1 & x1)
{
    return !(x1 >= x);
}

inline bool operator > (int x,const S1 & x1)
{
    return !(x1 <= x);
}

inline bool operator > (long x,const S1 & x1)
{
    return !(x1 <= x);
}

inline bool operator > (long long x,const S1 & x1)
{
    return !(x1 <= x);
}

inline bool operator <= (int x,const S1 & x1)
{
    return !(x1 > x);
}

inline bool operator <= (long x,const S1 & x1)
{
    return !(x1 > x);
}

inline bool operator <= (long long x,const S1 & x1)
{
    return !(x1 > x);
}

inline bool operator >= (int x,const S1 & x1)
{
    return !(x1 < x);
}

inline bool operator >= (long x,const S1 & x1)
{
    return !(x1 < x);
}

inline bool operator >= (long long x,const S1 & x1)
{
    return !(x1 < x);
}

inline bool operator == (int x,const S1 & x1)
{
    return !(x1 != x);
}

inline bool operator == (long x,const S1 & x1)
{
    return !(x1 != x);
}

inline bool operator == (long long x,const S1 & x1)
{
    return !(x1 != x);
}

inline bool operator != (int x,const S1 & x1)
{
    return !(x1 == x);
}

inline bool operator != (long x,const S1 & x1)
{
    return !(x1 == x);
}

inline bool operator != (long long x,const S1 & x1)
{
    return !(x1 == x);
}
