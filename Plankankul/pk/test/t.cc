#include <iostream.h>

void fatal(char *cp)
{
    cerr << cp << endl;
    exit(1);
}

#include "../bits.h"
#include <dmalloc.h>

S1	*X;
S1	*Y;

int main(int argc,char *[])
{
    char	typ;
    char	typ1;
    long	n;
    long	n1;
    S0		n2;
    long	vl;

    if (argc > 1) {
	cout << "Usage: x < <oldtest> > <newtest>" << endl;
	cout << "d<n>\t\t\tdefinition" << endl;
	cout << "=<n> <n>\t\tint assign" << endl;
	cout << "a<n> <B>\t\tbit assign" << endl;
	cout << "s<n> <B>\t\tsign" << endl;
	cout << "0<n> <B>\t\tisNull" << endl;
	cout << "l<n> <B>\t\tcast long" << endl;
	cout << "L<n> <B>\t\tcast long long" << endl;
	cout << "!<n> <B>\t\tnot Null" << endl;
	cout << "~<n> <B>\t\tcomplement" << endl;
	cout << "n<n> <B>\t\tnegativ" << endl;
	cout << "p<n> <B>\t\tpositiv" << endl;
	cout << "[<n> <B> <n>\t\tgetbit" << endl;
	cout << "]<n> <B> <n> <n>\tsetbit" << endl;
	cout << "|=<nl> <nr> <Bl> <Br>\tor assign" << endl;
	cout << "&=<nl> <nr> <Bl> <Br>\tand assign" << endl;
	cout << "+=<nl> <nr> <Bl> <Br>\tadd assign" << endl;
	cout << "-=<nl> <nr> <Bl> <Br>\tsub assign" << endl;
	cout << "*=<nl> <nr> <Bl> <Br>\tmult assign" << endl;
	cout << "/=<nl> <nr> <Bl> <Br>\tdiv assign" << endl;
	cout << "|<nl> <nr> <Bl> <Br>\tor" << endl;
	cout << "&<nl> <nr> <Bl> <Br>\tand" << endl;
	cout << "+<nl> <nr> <Bl> <Br>\tadd" << endl;
	cout << "-<nl> <nr> <Bl> <Br>\tsub" << endl;
	cout << "*<nl> <nr> <Bl> <Br>\tmult" << endl;
	cout << "/<nl> <nr> <Bl> <Br>\tdiv" << endl;
	cout << "==<nl> <nr> <Bl> <Br>\tequal" << endl;
	cout << "<=<nl> <nr> <Bl> <Br>\tless or equal" << endl;
	cout << ">=<nl> <nr> <Bl> <Br>\tgreater or equal" << endl;
	cout << "!=<nl> <nr> <Bl> <Br>\tnot equal" << endl;
	cout << "<<=<n> <B> <n>\t\tshift assign left" << endl;
	cout << ">>=<n> <B> <n>\t\tshift assign right" << endl;
	cout << "<<<n> <B> <n>\t\tshift left" << endl;
	cout << ">><n> <B> <n>\t\tshift right" << endl;
	exit(99);
    }
    while (true) {
	cin >> typ;
	if (cin.eof())
	    return 0;
	n = 0;
	vl = 0;
	switch (typ) {
	case 'd':	// einfache Definition
	    cin >> n;
	    if (n)
		X = new S1(n);
	    else
		X = new S1;
	    cout << typ << n << endl;
	    cout << "# " << *X << endl;
	    delete X;
	    break;
	case 'a':	// Bitzuweisung
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << *X << endl;
		delete X;
	    }
	    break;
	case 's':	// Vorzeichentest
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << X->sign() << endl;
		delete X;
	    }
	    break;
	case '0':	// Test auf 0
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << X->isNull() << endl;
		delete X;
	    }
	    break;
	case 'l':	// Cast to long
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << (long)*X << endl;
		delete X;
	    }
	    break;
	case 'L':	// Cast to long long
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << (long long)*X << endl;
		delete X;
	    }
	    break;
	case '~':	// Complement
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << ~*X << endl;
		delete X;
	    }
	    break;
	case 'n':	// Negativ
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << -*X << endl;
		delete X;
	    }
	    break;
	case 'p':	// Positiv
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << +*X << endl;
		delete X;
	    }
	    break;
	case '[':	// Getbit
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		cin >> n1;
		*X = A;
		cout << typ << n << ' ' << A << ' ' << n1 << endl;
		cout << "# " << (int)(*X)[n1] << endl;
		delete X;
	    }
	    break;
	case ']':	// Setbit
	    {
		S1	A;

		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		cin >> n1;
		cin >> n2;
		*X = A;
		X->setbit(n1,n2);
		cout << typ << n << ' ' << A << ' ' << n1 << ' ' << n2 << endl;
		cout << "# " << *X << endl;
		delete X;
	    }
	    break;
	case '=':	// Integer assign or Equal
	case '<':	// Less or LessEqual
	case '>':	// Greater or GreaterEqual
	case '!':	// NotNull or NotEqual
	    cin >> typ1;
	    if (typ == '=' && typ1 != '=') {	// Integer assign
		cin.putback(typ1);
		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> vl;
		*X = vl;
		cout << typ << n << ' ' << vl << endl;
		cout << "# " << *X << endl;
		delete X;
	    } else if (typ == '!' && typ1 != '=') {	// NotNull
		S1	A;

		cin.putback(typ1);
		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		*X = A;
		cout << typ << n << ' ' << A << endl;
		cout << "# " << !*X << endl;
		delete X;
	    } else if (typ == '<' && typ1 == '<' ||
			typ == '>' && typ1 == '>') {	// Shift and Shift assign
		S1	A;

		cin >> typ1;
		if (typ1 != '=')
		    cin.putback(typ1);
		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> A;
		cin >> n1;
		*X = A;
		cout << typ << typ;
		if (typ1 == '=')
		    cout << typ1;
		cout << n << ' ' << A << ' ' << n1 << endl;
		cout << "# ";
		if (typ == '<' && typ1 == '=') {
		    *X <<= n1;
		    cout << *X << endl;
		} else if (typ == '>' && typ1 == '=') {
		    *X >>= n1;
		    cout << *X << endl;
		} else if (typ == '<')
		    cout << (*X << n1) << endl;
		else if (typ == '>')
		    cout << (*X >> n1) << endl;
		delete X;
	    } else {
		S1	A;
		S1	B;

		if (typ1 != '=')
		    cin.putback(typ1);
		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> n1;
		if (n1)
		    Y = new S1(n1);
		else
		    Y = new S1;
		cin >> A;
		cin >> B;
		*X = A;
		*Y = B;
		cout << typ;
		if (typ1 == '=')
		    cout << typ1;
		cout << n << ' ' << n1 << ' ' << A << ' ' << B << endl;
		cout << "# ";
		if (typ == '=' && typ1 == '=')
		    cout << (*X == *Y) << endl;
		else if (typ == '<' && typ1 == '=')
		    cout << (*X <= *Y) << endl;
		else if (typ == '>' && typ1 == '=')
		    cout << (*X >= *Y) << endl;
		else if (typ == '!' && typ1 == '=')
		    cout << (*X != *Y) << endl;
		else if (typ == '<')
		    cout << (*X < *Y) << endl;
		else if (typ == '>')
		    cout << (*X > *Y) << endl;
		delete X;
		delete Y;
	    }
	    break;
	case '|':	// Or or Or assign
	case '&':	// And or And assign
	case '+':	// Add or Add assign
	case '-':	// Sub or Sub assign
	case '*':	// Mult or Mult assign
	case '/':	// Div or Div assign
	    {
		S1	A;
		S1	B;

		cin >> typ1;
		if (typ1 != '=')
		    cin.putback(typ1);
		cin >> n;
		if (n)
		    X = new S1(n);
		else
		    X = new S1;
		cin >> n1;
		if (n1)
		    Y = new S1(n1);
		else
		    Y = new S1;
		cin >> A;
		cin >> B;
		*X = A;
		*Y = B;
		cout << typ;
		if (typ1 == '=')
		    cout << typ1;
		cout << n << ' ' << n1 << ' ' << A << ' ' << B << endl;
		cout << "# ";
		switch (typ) {
		case '|':
		    if (typ1 == '=') {
			*X |= *Y;
			cout << *X << endl;
		    } else
			cout << (*X | *Y) << endl;
		    break;
		case '&':
		    if (typ1 == '=') {
			*X &= *Y;
			cout << *X << endl;
		    } else
			cout << (*X & *Y) << endl;
		    break;
		case '+':
		    if (typ1 == '=') {
			*X += *Y;
			cout << *X << endl;
		    } else
			cout << (*X + *Y) << endl;
		    break;
		case '-':
		    if (typ1 == '=') {
			*X -= *Y;
			cout << *X << endl;
		    } else
			cout << (*X - *Y) << endl;
		    break;
		case '*':
		    if (typ1 == '=') {
			*X *= *Y;
			cout << *X << endl;
		    } else
			cout << (*X * *Y) << endl;
		    break;
		case '/':
		    if (typ1 == '=') {
			*X /= *Y;
			cout << *X << endl;
		    } else
			cout << (*X / *Y) << endl;
		    break;
		}
		delete X;
		delete Y;
	    }
	    break;
	case '#':
	    break;
	default:
	    cerr << "Input error" << endl;
	    return 1;
	}
	if (!cin) {
	    cerr << "Input error" << endl;
	    return 1;
	}
	cin.ignore(32000,'\n');
    }
    return 0;
}
