#define CR 257
#define ZAHL 258
#define INTEGER 259
#define NAME 260
#define NEWNAME 261
#define FUNCNAME 262
#define S0 263
#define S1 264
#define S2 265
#define W0 266
#define W1 267
#define W2 268
#define W3 269
#define W4 270
#define W5 271
#define W6 272
#define FIN 273
#define ERROR 274
#define YYLE 275
#define YYGE 277
#define YYNE 279
#define YYASS 281
#define YYRANGE 283
#define YYCOND 285
#define YYDISV 287
#define YYIMPL 289
#define YYSQRT 291
#define YYSOR 293
#define vv 294
#define YYSAND 295
#define YYSIMPL 297
#define YYSDISV 299
#define YYSEQUIV 301
#ifndef YYSTYPE_DEFINED
#define YYSTYPE_DEFINED
typedef union {
    char	*string;
    Konstante	*val;
    Oper	oper;
    Bereich	*bereich;
    Namen	*namensdef;
    Def		*definition;
    Expr	*expression;
} YYSTYPE;
#endif /* YYSTYPE_DEFINED */
extern YYSTYPE yylval;
