%{
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <stdarg.h>
#include "plankal.h"

extern ifstream	*ins;
extern ofstream	outs;

extern void	gen_comment(char *cp);
extern void	yyerror(char *);
extern int	yylex();

static Namen *out_fkt(Def *dp,Expr *ep);

char	buffer[NAMELEN];
int	structs = 0;

Namen *namen = NULL;
Namen *function = NULL;

//#if YYDEBUG != 0
#define YYPRINT(fp, t, v)       yyprint(fp, t, v)
static int yyprint(void *fp, int t, ...);
//#endif

%}

%token CR
%token ZAHL
%token INTEGER
%token NAME
%token NEWNAME
%token FUNCNAME
%token S0
%token S1
%token S2
%token W0
%token W1
%token W2
%token W3
%token W4
%token W5
%token W6
%token FIN
%token ERROR
%token YYLE	"<="
%token YYGE	">="
%token YYNE	"!="
%token YYASS	"=>"
%token YYRANGE	".."
%token YYCOND	"->"
%token YYDISV	"!~"
%token YYIMPL	"!>"
%token YYSQRT	"-/"
%token YYSOR	"vv"
%token YYSAND	"^^"
%token YYSIMPL	"!>>"
%token YYSDISV	"!~~"
%token YYSEQUIV	"~~"

%nonassoc ".."
%left "->"
%left "=>"
%left ','
%left '~' "!~"
%left "!>"
%left '^'
%left 'v'
%left '<' '>' '=' "<=" ">=" "!="
%left '+' '-'
%left '*' ':'
%nonassoc '[' ']'
%nonassoc '(' ')'
%nonassoc '|'

%union {
    char	*string;
    Konstante	*val;
    Oper	oper;
    Bereich	*bereich;
    Namen	*namensdef;
    Def		*definition;
    Expr	*expression;
};

%type <string>		NEWNAME
%type <namensdef>	NAME, FUNCNAME
%type <namensdef>	Vereinbarung, Typvereinbarung, Funktion, Funktionsvariable
%type <namensdef>	Variable, Rueckgabevariable, Parametervariable
%type <namensdef>	Beschraenkungsangabe, Wdhanfang, Typenvariable
%type <definition>	Artdefinition, Artangabe, Strukturelemente, Strukturdefinition
%type <definition>	Parameterliste, Eingangsparameter, Ausgangsparameter
%type <definition>	Rueckgabeliste, Rueckgabevariablen, Randauszug
%type <definition>	Randauszug1, Eingangsparameter1, Typenliste
%type <val>		ZAHL, INTEGER, Integerkonstante, Konstante
%type <bereich>		Konstantenbereich, Wertliste, Beschraenkung
%type <expression>	Klammerausdruck, Strichrechnung, Punktrechnung, Vergleich
%type <expression>	Undvergleich, Odervergleich, Implvergleich, Equivvergleich
%type <expression>	Ausdruck, Ausdruecke, Funktionsvariablenliste
%type <expression>	SubVariable, StrukturVariable, Ausdrucksliste
%type <expression>	Anweisung, Anweisungen, Programmzeile, Programmzeilen
%type <oper>		SubOp

%%
File:			  Programm
;
Programm:		  Planteil
			| Programm Planteil
;
Planteil:		  Vereinbarung
			    { }
			| Typvereinbarung
			    { }
			| Funktion
			    { }
;
Vereinbarung:		  NEWNAME ':' Strukturdefinition CR
				{
				  $$ = Namen::neuerName(&namen,$1);
				  delete [] $1;
				  $$->def = $3;
				  outs << xindent;
				  $$->def->gen();
				  outs << ' ';
				  $$->gen();
				  $$->def->vgen();
				  outs << ';' << endl;
				  $$->defined = true;
				}
;
Typvereinbarung:	  NEWNAME '=' Strukturdefinition CR
				{
				  $$ = Namen::neuerName(&namen,$1);
				  delete [] $1;
				  $$->def = $3;
				  if ($$->def->typ == STRUCT) {
				      if ($$->def->name)	// this name is not in use
					  $$->def->name->defined = true;
				      $$->def->name = $$;
				      $$->def->gen();
				  } else {
				      outs << xindent << "typedef ";
				      $$->def->gen();
				      outs << ' ';
				      $$->gen();
				  }
				  outs << ';' << endl;
				  $$->defined = true;
				}
			| NEWNAME '=' Beschraenkung CR
				{
				  $$ = Namen::neuerName(&namen,$1);
				  delete [] $1;
				  $$->besch = $3;
				  outs << xindent << "Restrict ";
				  $$->gen();
				  outs << "[] = {";
				  $$->besch->gen();
				  outs << "};" << endl;
				  $$->defined = true;
				}
;
Funktion:		  NEWNAME ':'
				{
				  function = Namen::neuerName(&namen,$1);
				  delete [] $1;
				  function->def = new Def(FUNCTION);
				}
				    Randauszug CR Programmzeilen
					{ $$ = out_fkt($4,$6); }
			| NEWNAME ':' '(' 'R'
				{
				  function = Namen::neuerName(&namen,$1);
				  delete [] $1;
				  function->def = new Def(FUNCTION);
				}
				    Randauszug1 CR Programmzeilen
					{ $$ = out_fkt($6,$8); }
			| NEWNAME '.' Integerkonstante ':'
				{
				  function = Namen::neuerName(&namen,$1,$3->lval);
				  delete [] $1;
				  delete $3;
				  function->def = new Def(FUNCTION);
				}
				    Randauszug CR Programmzeilen
					{ $$ = out_fkt($6,$8); }
			| FUNCNAME '.' Integerkonstante ':'
				{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,$1->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  Namen *np = namen->suche(tmp,$3->lval);
				  if (np)
				      YYERROR;
				  function = Namen::neuerName(&namen,tmp,$3->lval);
				  delete $3;
				  function->def = new Def(FUNCTION);
				}
				    Randauszug CR Programmzeilen
					{ $$ = out_fkt($6,$8); }
			| NEWNAME '.' Integerkonstante ':' '(' 'R'
				{
				  function = Namen::neuerName(&namen,$1,$3->lval);
				  delete [] $1;
				  delete $3;
				  function->def = new Def(FUNCTION);
				}
				    Randauszug1 CR Programmzeilen
					{ $$ = out_fkt($8,$10); }
			| FUNCNAME '.' Integerkonstante ':' '(' 'R'
				{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,$1->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  Namen *np = namen->suche(tmp,$3->lval);
				  if (np)
				      YYERROR;
				  function = Namen::neuerName(&namen,tmp,$3->lval);
				  delete $3;
				  function->def = new Def(FUNCTION);
				}
				    Randauszug1 CR Programmzeilen
					{ $$ = out_fkt($8,$10); }
;
Strukturdefinition:	  '(' Strukturelemente ')'
				{
				  if ($2->isTemplate())
				      $$ = new Def(STRUCT);
				  else {
				      Namen *np;
				      np = Namen::neuerName(&namen,"st",++structs);
				      $$ = new Def(STRUCT,np);
				      np->def = $$;
				  }
				  $$->down = $2;
				}
			| Artangabe
				{ $$ = $1; }
;
Strukturelemente:	  Strukturdefinition
				{ $$ = $1; }
			| Strukturelemente ',' Strukturdefinition
				{
				  Def	*dp;
				  for (dp = $1; dp->next; dp = dp->next)
				      ;
				  dp->next = $3;
				  $$ = $1;
				}
;
Artangabe:		  Artdefinition '#' Beschraenkungsangabe
				{ $$ = $1; }
			| Artdefinition
				{ $$ = $1; }
;
Artdefinition:		  Integerkonstante '*' Strukturdefinition
				{
				  $$ = new Def(VEKTOR,$1->lval);
				  delete $1;
				  $$->down = $3;
				}
			| NEWNAME '*' Strukturdefinition
				{
				  Namen *np = Namen::neuerName(&function->def->name,$1);
				  delete [] $1;
				  np->def = new Def(INT);
				  $$ = new Def(VEKTOR,np);
				  $$->down = $3;
				}
			| NAME '*' Strukturdefinition
				{
				  $$ = new Def(VEKTOR,$1);
				  $$->down = $3;
				}
			| NEWNAME
				{
				  Namen *np = Namen::neuerName(&function->def->name,$1);
				  delete [] $1;
				  np->def = new Def(TEMPLATE);
				  $$ = new Def(NAMEDEF,np);
				}
			| NAME			// originally 'An'
				{ $$ = new Def(NAMEDEF,$1); }
			| S0
				{ $$ = new Def(BIT); }
			| S1 '.' Integerkonstante
				{
				  $$ = new Def(BITVEKTOR,$3->lval);
				  delete $3;
				}
			| S1 '.' NEWNAME
				{
				  Namen *np = Namen::neuerName(&function->def->name,$3);
				  delete [] $3;
				  np->def = new Def(INT);
				  $$ = new Def(BITVEKTOR,np);
				}
			| S1 '.' NAME
				{ $$ = new Def(BITVEKTOR,$3); }
			| S2
				{ $$ = new Def(BIT); }	// TODO Paar beliebiger Struktur
;
Randauszug:		  Eingangsparameter "=>" Ausgangsparameter
				{ $$ = new Def($1,$3); }
			| Eingangsparameter
				{ $$ = new Def($1,NULL); }
			| "=>" Ausgangsparameter
				{ $$ = new Def(NULL,$2); }
			|
				{ $$ = new Def(NULL,NULL); }
;
Randauszug1:		  Eingangsparameter1 "=>" Ausgangsparameter
				{ $$ = new Def($1,$3); }
			| Eingangsparameter1
				{ $$ = new Def($1,NULL); }
;
Eingangsparameter:	  'R' '(' Parameterliste ')'
				{ $$ = $3; }
;
Eingangsparameter1:	  '(' Typenliste ')' ')' '(' Parameterliste ')'
				{
				  Def	*dp;
				  for (dp = $2; dp->next; dp = dp->next)
				      ;
				  dp->next = $6;
				  $$ = $2;
				}
;
Ausgangsparameter:	  Rueckgabevariablen
;
Parameterliste:		  Parametervariable
				{ $$ = new Def(NAMEDEF,$1); }
			| Parameterliste ',' Parametervariable
				{
				  Def	*dp;
				  for (dp = $1; dp->next; dp = dp->next)
				      ;
				  dp->next = new Def(NAMEDEF,$3);
				  $$ = $1;
				}
;
Typenliste:		  Typenvariable
				{
				  $1->def = new Def(TYPEDEF);
				  $$ = new Def(NAMEDEF,$1);
				}
			| Typenliste ',' Typenvariable
				{
				  Def	*dp;
				  for (dp = $1; dp->next; dp = dp->next)
				      ;
				  $3->def = new Def(TYPEDEF);
				  dp->next = new Def(NAMEDEF,$3);
				  $$ = $1;
				}
;
Parametervariable:	  NEWNAME '=' Strukturdefinition	// originally 'Vn'
				{
				  $$ = Namen::neuerName(&function->def->name,$1);
				  delete [] $1;
				  $$->def = $3;
				}
			| NAME '=' Strukturdefinition
				{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,$1->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  $$ = Namen::neuerName(&function->def->name,tmp);
				  $$->def = $3;
				}
;
Rueckgabevariablen:	  '(' Rueckgabeliste ')'
				{ $$ = $2; }
			| Rueckgabevariable
				{ $$ = new Def(NAMEDEF,$1); }
;
Rueckgabeliste:		  Rueckgabevariable
				{ $$ = new Def(NAMEDEF,$1); }
			| Rueckgabeliste ',' Rueckgabevariable
				{
				  Def	*dp;
				  for (dp = $1; dp->next; dp = dp->next)
				      ;
				  dp->next = new Def(NAMEDEF,$3);
				  $$ = $1;
				}
;
Rueckgabevariable:	  NEWNAME '=' Strukturdefinition	// originally 'Rn'
				{
				  $$ = Namen::neuerName(&function->def->name,$1);
				  delete [] $1;
				  $$->def = $3;
				}
			| NAME '=' Strukturdefinition
				{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,$1->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  $$ = Namen::neuerName(&function->def->name,tmp);
				  $$->def = $3;
				}
;
Typenvariable:		  NEWNAME	// originally 'ALPHAn...'
				{
				  $$ = Namen::neuerName(&function->def->name,$1);
				  delete [] $1;
				}
			| NAME
				{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,$1->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  $$ = Namen::neuerName(&function->def->name,tmp);
				}
;
Programmzeilen:		  Programmzeile
				{ $$ = $1; }
			| Programmzeile Programmzeilen
				{
				  if ($1)
				      $$ = $2;
				  else if (!$2)
				      $$ = $1;
				  else
				      $$ = new Expr(STMNT,$1,$2);
				}
;
Programmzeile:		  leer Anweisungen CR
				{ $$ = $2; }
;
Beschraenkung:		  '[' Wertliste ']'
				{ $$ = $2; }
;
Wertliste:		  Konstantenbereich
				{ $$ = $1; }
			| Wertliste ',' Konstantenbereich
				{
				  Bereich	*bp;
				  for (bp = $1; bp->next; bp = bp->next)
				      ;
				  bp->next = $3;
				  $$ = $1;
				}
;
Konstantenbereich:	  Konstante
				{ $$ = new Bereich($1); }
			| Konstante ".." Konstante
				{ $$ = new Bereich($1,$3); }
;
Anweisungen:		  Anweisung
				{ $$ = $1; }
			| Anweisung ';' Anweisungen
				{
				  if (!$1)
				      $$ = $3;
				  else if (!$3)
				      $$ = $1;
				  else
				      $$ = new Expr(STMNT,$1,$3);
				}
;
Anweisung:		  '{' Anweisungen '}'
				{ $$ = $2; }
			| FIN
				{ $$ = new Expr(BREAK,new Konstante(1L)); }
			| FIN '.' Integerkonstante
				{ $$ = new Expr(BREAK,$3); }
			| 'W' '(' Ausdruck ')' Anweisung
				{ $$ = new Expr(WHILE,$3,$5); }
			| W0 '(' Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR0,$3,$5); }
			| W1 Wdhanfang Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR1,$2,$3,$5); }
			| W2 Wdhanfang Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR2,$2,$3,$5); }
			| W3 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR3,$2,$3,$5,$7); }
			| W4 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR4,$2,$3,$5,$7); }
			| W5 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR5,$2,$3,$5,$7); }
			| W6 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung
				{ $$ = new Expr(FOR6,$2,$3,$5,$7); }
			| W6 Wdhanfang Ausdruck ',' ')' Anweisung
				{ $$ = new Expr(FOR6,$2,$3,NULL,$6); }
			| 'X' Variable '(' StrukturVariable ',' Ausdruck ')' Anweisung	// next
				{ $$ = new Expr(NEXT,$2,$4,$6,$8); }
			| 'L' Variable '(' StrukturVariable ',' Ausdruck ')' Anweisung	// last
				{ $$ = new Expr(LAST,$2,$4,$6,$8); }
			| Ausdruck "->" Anweisung
				{ $$ = new Expr(IF,$1,$3); }
			| Ausdruck "=>" FIN
				{ $$ = new Expr(IF,$1,new Expr(BREAK,new Konstante(2L))); }
			| Ausdruck "=>" FIN '.' Integerkonstante
				{
				  $5->lval++;
				  $$ = new Expr(IF,$1,new Expr(BREAK,$5));
				}
			| Ausdruck "=>" StrukturVariable
				{ $$ = new Expr(ASSIGN,$3,$1); }
			| Ausdruck "=>" SubOp StrukturVariable
				{ $$ = new Expr($3,$4,$1); }
			| Ausdruck "=>" 'X' StrukturVariable
				{ $$ = new Expr(ASSNEXT,$4,$1); }
			| Ausdruck
				{ $$ = $1; }
			|	{ $$ = NULL; }
;
Wdhanfang:		  '('
				{
				  $$ = Namen::neuerName(&function->def->name,"i");
				  $$->def = new Def(INT);
				}
			| '.' Integerkonstante '('
				{
				  char *tmp = new char[10];
				  sprintf(tmp,"i%d",$2->lval);
				  delete $2;
				  $$ = Namen::neuerName(&function->def->name,tmp);
				  $$->def = new Def(INT);
				}
;
SubOp:			  'v'	{ $$ = ASSOR; }
			| '^'	{ $$ = ASSAND; }
			| '~'	{ $$ = ASSEQUIV; }
			| "!~"	{ $$ = ASSDISV; }
			| "!>"	{ $$ = ASSIMPL; }
			| '+'	{ $$ = ASSADD; }
			| '-'	{ $$ = ASSSUB; }
			| '*'	{ $$ = ASSMUL; }
			| ':'	{ $$ = ASSDIV; }
;
Ausdrucksliste:		  Ausdruck
				{ $$ = $1; }
			| Ausdruck ',' Ausdrucksliste
				{ $$ = new Expr(PARLIST,$1,$3); }
;
Ausdruecke:		  Ausdruck
				{ $$ = $1; }
			| Ausdruck ',' Ausdruecke
				{
				  $$ = new Expr(CAT,$1,$3);
				}
;
Ausdruck:		  Equivvergleich
				{ $$ = $1; }
			| Ausdruck "!~" Equivvergleich
				{ $$ = new Expr(DISVAL,$1,$3); }
			| Ausdruck '~' Equivvergleich
				{ $$ = new Expr(EQUIV,$1,$3); }
;
Equivvergleich:		  Implvergleich
				{ $$ = $1; }
			| Equivvergleich "!>" Implvergleich
				{ $$ = new Expr(IMPL,$1,$3); }
;
Implvergleich:		  Undvergleich
				{ $$ = $1; }
			| Implvergleich '^' Undvergleich
				{ $$ = new Expr(AND,$1,$3); }
;
Undvergleich:		  Odervergleich
				{ $$ = $1; }
			| Undvergleich 'v' Odervergleich
				{ $$ = new Expr(OR,$1,$3); }
;
Odervergleich:		  Vergleich
				{ $$ = $1; }
			| Odervergleich '<' Vergleich
				{ $$ = new Expr(LESS,$1,$3); }
			| Odervergleich '>' Vergleich
				{ $$ = new Expr(GREATER,$1,$3); }
			| Odervergleich '=' Vergleich
				{ $$ = new Expr(EQUAL,$1,$3); }
			| Odervergleich "<=" Vergleich
				{ $$ = new Expr(LE,$1,$3); }
			| Odervergleich ">=" Vergleich
				{ $$ = new Expr(GE,$1,$3); }
			| Odervergleich "!=" Vergleich
				{ $$ = new Expr(NE,$1,$3); }
;
Vergleich:		  Strichrechnung
				{ $$ = $1; }
			| Vergleich '+' Strichrechnung
				{ $$ = new Expr(ADD,$1,$3); }
			| Vergleich '-' Strichrechnung
				{ $$ = new Expr(SUB,$1,$3); }
;
Strichrechnung:		  Punktrechnung
				{ $$ = $1; }
			| Strichrechnung '*' Punktrechnung
				{ $$ = new Expr(MUL,$1,$3); }
			| Strichrechnung ':' Punktrechnung
				{ $$ = new Expr(DIV,$1,$3); }
;
Punktrechnung:		  Klammerausdruck
				{ $$ = $1; }
			| '+'  Klammerausdruck
				{ $$ = new Expr(POS,$2); }
			| '-'  Klammerausdruck
				{ $$ = new Expr(NEG,$2); }
			| '!'  Klammerausdruck
				{ $$ = new Expr(NOT,$2); }
			| Klammerausdruck Funktionsvariable  Klammerausdruck
				{ $$ = new Expr(CALL,new Expr(VAR,$2),new Expr(PARLIST,$1,$3)); }
;
Klammerausdruck:	  '(' Ausdruecke ')'
				{
				  if ($2->oper == CAT)
				      $$ = new Expr(GENSTRUCT,$2);
				  else
				      $$ = $2;
				}
			| 'E' Variable '(' StrukturVariable ',' Ausdruck ')'	// exist
				{ $$ = new Expr(EXIST,$2,$4,$6); }
			| 'E' Variable '(' Ausdruck ')'		// exist
				{ $$ = new Expr(EXIST,$2,$4); }
			| 'F' Variable '(' StrukturVariable ',' Ausdruck ')'	// first
				{ $$ = new Expr(FIRST,$2,$4,$6); }
			| Variable '(' StrukturVariable ',' Ausdruck ')'	// all
				{ $$ = new Expr(ALL,$1,$3,$5); }
			| Variable '(' Ausdruck ')'	// all
				{ $$ = new Expr(ALL,$1,$3); }
			| '|' Ausdruck '|'
				{ $$ = new Expr(ABS,$2); }
			| StrukturVariable
				{ $$ = $1; }
			| Konstante
				{ $$ = new Expr(CONST,$1); }
;
StrukturVariable:	  SubVariable
				{ $$ = $1; }
			| StrukturVariable '.' Integerkonstante
				{ $$ = new Expr(FIELD,$1,new Expr(CONST,$3)); }
//			| StrukturVariable '.' NAME'
//				{ $$ = new Expr(FIELD,$1,new Expr(CONST,new Konstante(-1L))); }
;
SubVariable:		  Variable '[' Ausdruecke ']'
				{ $$ = new Expr(INDEX,new Expr(VAR,$1),$3); }
			| Variable
				{ $$ = new Expr(VAR,$1); }
			| 'N' '(' Ausdruck ')'					// count
				{ $$ = new Expr(COUNT,$3); }
			| '(' Funktionsvariable '(' Funktionsvariablenliste ')' ')' '(' Ausdrucksliste ')'
				{ $$ = new Expr(CALL,new Expr(VAR,$2),$8,$4); }
			| Funktionsvariable '(' Ausdrucksliste ')'
				{ $$ = new Expr(CALL,new Expr(VAR,$1),$3); }
;
Funktionsvariablenliste:  Funktionsvariable
				{ $$ = new Expr(VAR,$1); }
			| Funktionsvariable ',' Funktionsvariablenliste
				{ $$ = new Expr(PARLIST,new Expr(VAR,$1),$3); }
;
Funktionsvariable:	  FUNCNAME	// originally 'Rn' or 'Pn' ??
				{ $$ = $1; }
			| FUNCNAME '.' Integerkonstante
				{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,$1->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  $$ = namen->suche(tmp,$3->lval);
				  delete $3;
				  if (!$$)
				      YYERROR;
				}
;
Variable:		  NAME		// originally 'Vn' or 'Zn'
				{ $$ = $1; }
			| NEWNAME '=' Strukturdefinition
				{
				  if (function)
				      $$ = Namen::neuerName(&function->def->name,$1);
				  else
				      $$ = Namen::neuerName(&namen,$1);
				  delete [] $1;
				  $$->def = $3;
				}
			| NAME '#' Beschraenkungsangabe
				{ $$ = $1; }
;
Beschraenkungsangabe:	  NAME		// originally 'Bn'
				{ $$ = $1; }
;
Konstante:		  ZAHL
				{ $$ = $1; }
			| INTEGER
				{ $$ = $1; }
;
Integerkonstante:	  INTEGER
				{ $$ = $1; }
;
leer:			  '\t'
;
%%

void yyerror(char *cp)
{
    cerr << cp << endl;
}

#ifdef YYPRINT
//static int yyprint(FILE *fp, int t, YYSTYPE v)
static int yyprint(void *fpx, int t, ...)
{
    va_list	ap;
    int i;
    FILE	*fp;
    YYSTYPE	v;

    va_start(ap,t);
    fp = (FILE *)fpx;
    v = va_arg(ap,YYSTYPE);
    if (t == ZAHL || t == BITS || t == INTEGER) {
	fprintf(fp," ");
        v.val->print();
    } else if (t == NEWNAME)
        fprintf(fp, " \"%s\"", v.string);
    else if (t == NAME || t == FUNCNAME) {
	fprintf(fp," \"");
	v.namensdef->print(false);
	fprintf(fp,"\"");
    }
    va_end(ap);
    return 0;
}
#endif 

char	eingabezeile[256];
short	eingabeindex = 0;
static bool	zurueck = false;
static int	linenr = 0;
static ifstream	*includes[20];
static int	include_lnr[20];
static int	include_nr = 0;

static void zurueckZeichen(char c)
{
    ins->putback(c);
    zurueck = true;
}

static int holeZeichen()
{
    int		c;
    char	fname[NAMELEN];

    c = ins->get();
    if (c == EOF && include_nr) {
	delete ins;
	ins = includes[--include_nr];
	linenr = include_lnr[include_nr];
	c = ins->get();
    }
    if (!zurueck) {
	if (eingabeindex == 0 && c == '#') {
	    while (c == '#') {
		if (eingabeindex < sizeof(eingabezeile)-1)
		    eingabezeile[eingabeindex++] = c;
		while ((c = ins->get()) != '\n')
		    if (eingabeindex < sizeof(eingabezeile)-1)
			eingabezeile[eingabeindex++] = c;
		eingabezeile[eingabeindex] = '\0';
		gen_comment(eingabezeile);
		eingabeindex = 0;
		linenr++;
		if (eingabezeile[1] == '<') {
		    includes[include_nr] = ins;
		    include_lnr[include_nr++] = linenr;
		    strcpy(fname,&eingabezeile[2]);
		    ins = new ifstream(fname);
		    linenr = 0;
		}
		c = ins->get();
	    }
	}
	if (c == '\n') {
	    eingabezeile[eingabeindex] = '\0';
	    gen_comment(eingabezeile);
	    eingabeindex = 0;
	    linenr++;
	} else if (eingabeindex < sizeof(eingabezeile)-1)
	    eingabezeile[eingabeindex++] = c;
    }
    zurueck = false;
    return c;
}

int yylex()
{
    int		c;
    int		i;
    char	*cp;
    Namen	*np;

    while (1) {
	c = holeZeichen();
	switch (c) {
	    case EOF:
		return 0;
	    case '\n':
		return CR;
//	    case '\t':
	    case ' ':
		break;
	    case '=':
		c = holeZeichen();
		if (c == '>')
		    return YYASS;
		zurueckZeichen(c);
		return '=';
	    case '<':
		c = holeZeichen();
		if (c == '=')
		    return YYLE;
		zurueckZeichen(c);
		return '<';
	    case '>':
		c = holeZeichen();
		if (c == '=')
		    return YYGE;
		zurueckZeichen(c);
		return '>';
	    case '!':
		c = holeZeichen();
		if (c == '=')
		    return YYNE;
		if (c == '>') {
		    c = holeZeichen();
		    if (c == '>')
			return YYSIMPL;
		    zurueckZeichen(c);
		    return YYIMPL;
		}
		if (c == '~') {
		    c = holeZeichen();
		    if (c == '~')
			return YYSDISV;
		    zurueckZeichen(c);
		    return YYDISV;
		}
		zurueckZeichen(c);
		return '!';
	    case '.':
		c = holeZeichen();
		if (c == '.')
		    return YYRANGE;
		zurueckZeichen(c);
		return '.';
	    case '-':
		c = holeZeichen();
		if (c == '>')
		    return YYCOND;
		if (c == '/')
		    return YYSQRT;
		zurueckZeichen(c);
		return '-';
	    case '^':
		c = holeZeichen();
		if (c == '^')
		    return YYSAND;
		zurueckZeichen(c);
		return '^';
	    case '~':
		c = holeZeichen();
		if (c == '~')
		    return YYSEQUIV;
		zurueckZeichen(c);
		return '~';
	    case '%':
		c = holeZeichen();
		i = 1;
		cp = buffer;
		*cp++ = '%';
		while (isdigit(c)) {
		    if (++i < sizeof(buffer))
			*cp++ = c;
		    c = holeZeichen();
		}
		zurueckZeichen(c);
		*cp = '\0';
		np = NULL;
		if (function)		// first look for a local name
		    np = function->def->name->suche(buffer);
		if (!np) {		// then global
		    np = namen->suche(buffer);
		    if (!np) {
			yylval.string = new char[strlen(buffer)+1];
			strcpy(yylval.string,buffer);
			return NEWNAME;
		    }
		}
		yylval.namensdef = np;
		return NAME;
	    case '\t':
	    case '|':
	    case '#':
	    case '+':
	    case '*':
	    case ':':
	    case ',':
	    case ';':
	    case '(':
	    case ')':
	    case '[':
	    case ']':
	    case '{':		// TODO until we find something better
	    case '}':
		return c;
	    case '\'':
		c = holeZeichen();
		switch (c) {
		case '0':
		case 'L':
		    i = 0;
		    cp = buffer;
		    while (c == '0' || c == 'L') {
			if ((i & 7) == 0)
			    *cp = '\0';
			*cp <<= 1;
			if (c == 'L')
			    *cp |= 1;
			if (i < sizeof(buffer)*8 && (++i & 7) == 0)
			    cp++;
			c = holeZeichen();
		    }
		    if (c != '\'')
			return ERROR;
		    if ((i & 7) != 0)
			*cp <<= 8-(i & 7);
		    if (i == 1) {
			yylval.val = new Konstante((long)*cp);
			return INTEGER;
		    }
		    yylval.val = new Konstante(BITS);
		    yylval.val->bval.set((unsigned char *)buffer,i);
		    return ZAHL;
		case '+':
		case '-':
		    i = 0;
		    cp = buffer;
		    while (c == '-' || c == '+') {
			if ((i & 7) == 0)
			    *cp = '\0';
			if (c == '+')
			    *cp |= 1 << (i & 7);
			if (i < sizeof(buffer)*8 && (++i & 7) == 0)
			    cp++;
			c = holeZeichen();
		    }
		    if (c != '\'')
			return ERROR;
//		    if ((i & 7) != 0)
//			*cp <<= 8-(i & 7);
		    if (i == 1) {
			yylval.val = new Konstante((long)*cp);
			return INTEGER;
		    }
		    yylval.val = new Konstante(BITS);
		    yylval.val->bval.set_re((unsigned char *)buffer,i);
		    return ZAHL;
		case '\'':
		    yylval.val = new Konstante(0L);
		    return INTEGER;
		}
		return ERROR;
	    default:
		if (isdigit(c)) {
		    i = 0;
		    cp = buffer;
		    while (isdigit(c)) {
			if (++i < sizeof(buffer))
			    *cp++ = c;
			c = holeZeichen();
		    }
		    zurueckZeichen(c);
		    *cp = '\0';
		    yylval.val = new Konstante(atol(buffer));
		    return INTEGER;
		}
		if (isalpha(c)) {
		    i = 0;
		    cp = buffer;
		    while (isalpha(c) || isdigit(c)) {
			if (++i < sizeof(buffer))
			    *cp++ = c;
			c = holeZeichen();
		    }
		    zurueckZeichen(c);
		    *cp = '\0';
		    if (strcmp(buffer,"S0") == 0)
			return S0;	// a bit
		    if (strcmp(buffer,"S1") == 0)
			return S1;	// a bit of a bitvector
		    if (strcmp(buffer,"S2") == 0)
			return S2;	// a pair of any structure
		    if (strcmp(buffer,"E") == 0)
			return 'E';	// Existiert-Anweisung
		    if (strcmp(buffer,"F") == 0)
			return 'F';	// First-Anweisung
		    if (strcmp(buffer,"X") == 0)
			return 'X';	// Next-Anweisung
		    if (strcmp(buffer,"L") == 0)
			return 'L';	// Last-Anweisung
		    if (strcmp(buffer,"N") == 0)
			return 'N';	// Anzahl-Anweisung
		    if (strcmp(buffer,"R") == 0)
			return 'R';	// Resultatwert
		    if (strcmp(buffer,"W") == 0)
			return 'W';	// Wiederholungsanweisung
		    if (strcmp(buffer,"W0") == 0)
			return W0;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W1") == 0)
			return W1;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W2") == 0)
			return W2;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W3") == 0)
			return W3;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W4") == 0)
			return W4;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W5") == 0)
			return W5;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W6") == 0)
			return W6;	// Wiederholungsanweisung
		    if (strcmp(buffer,"FIN") == 0)
			return FIN;	// Abbruchanweisung
		    if (strcmp(buffer,"v") == 0)
			return 'v';	// or
		    if (strcmp(buffer,"vv") == 0)
			return YYSOR;	// or
		    np = NULL;
		    if (function)	// first look for a local name
			np = function->def->name->suche(buffer);
		    if (!np) {		// then global
			np = namen->suche(buffer);
			if (!np) {
			    yylval.string = new char[strlen(buffer)+1];
			    strcpy(yylval.string,buffer);
			    return NEWNAME;
			}
		    }
		    yylval.namensdef = np;
		    if (np->def && (np->def->typ == FUNCTION || np->def->typ == TYPEDEF))
			return FUNCNAME;
		    return NAME;
		}
		return ERROR;
	}
    }
    return ERROR;
}

static Namen *out_fkt(Def *dp,Expr *ep)
{
    Expr	*exp;
    Namen	*np;

    function->def->down = dp;
    exp = restruct(ep);
//    exp->define_all();
    exp->label_all();
    function->def->exp = exp;
    np = function;
    function = NULL;
    return np;
}
