#ifndef lint
/*static char yysccsid[] = "from: @(#)yaccpar	1.9 (Berkeley) 02/21/93";*/
static char yyrcsid[]
#if __GNUC__ >= 2
  __attribute__ ((unused))
#endif /* __GNUC__ >= 2 */
  = "$Interix: skeleton.c,v 1.26 $";
#endif
#include <stdlib.h>
#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYLEX yylex()
#define YYEMPTY -1
#define yyclearin (yychar=(YYEMPTY))
#define yyerrok (yyerrflag=0)
#define YYRECOVERING() (yyerrflag!=0)
#define YYPREFIX "yy"
#line 1 "plankal.y"

#include <iostream>
#include <fstream>
#include <ctype.h>
#include <stdarg.h>
#include "plankal.h"

extern ifstream	*ins;
extern ofstream	outs;

extern void	gen_comment(char *cp);
extern void	yyerror(char *);
extern int	yylex();

static Namen *out_fkt(Def *dp,Expr *ep);

char	buffer[NAMELEN];
int	structs = 0;

Namen *namen = NULL;
Namen *function = NULL;

/*#if YYDEBUG != 0*/
#define YYPRINT(fp, t, v)       yyprint(fp, t, v)
static int yyprint(void *fp, int t, ...);
/*#endif*/

#line 78 "plankal.y"
#ifndef YYSTYPE_DEFINED
#define YYSTYPE_DEFINED
typedef union {
    char	*string;
    Konstante	*val;
    Oper	oper;
    Bereich	*bereich;
    Namen	*namensdef;
    Def		*definition;
    Expr	*expression;
} YYSTYPE;
#endif /* YYSTYPE_DEFINED */
#line 61 "plankal.cc"
#define CR 257
#define ZAHL 258
#define INTEGER 259
#define NAME 260
#define NEWNAME 261
#define FUNCNAME 262
#define S0 263
#define S1 264
#define S2 265
#define W0 266
#define W1 267
#define W2 268
#define W3 269
#define W4 270
#define W5 271
#define W6 272
#define FIN 273
#define ERROR 274
#define YYLE 275
#define YYGE 277
#define YYNE 279
#define YYASS 281
#define YYRANGE 283
#define YYCOND 285
#define YYDISV 287
#define YYIMPL 289
#define YYSQRT 291
#define YYSOR 293
#define vv 294
#define YYSAND 295
#define YYSIMPL 297
#define YYSDISV 299
#define YYSEQUIV 301
#define YYERRCODE 256
#if defined(__cplusplus) || defined(__STDC__)
const short yylhs[] =
#else
short yylhs[] =
#endif
	{                                        -1,
    0,   48,   48,   49,   49,   49,    1,    2,    2,   50,
    3,   51,    3,   52,    3,   53,    3,   54,    3,   55,
    3,   14,   14,   13,   13,   12,   12,   11,   11,   11,
   11,   11,   11,   11,   11,   11,   11,   20,   20,   20,
   20,   21,   21,   16,   22,   17,   15,   15,   23,   23,
    7,    7,   19,   19,   18,   18,    6,    6,   10,   10,
   46,   46,   45,   28,   27,   27,   26,   26,   44,   44,
   43,   43,   43,   43,   43,   43,   43,   43,   43,   43,
   43,   43,   43,   43,   43,   43,   43,   43,   43,   43,
   43,   43,    9,    9,   47,   47,   47,   47,   47,   47,
   47,   47,   47,   42,   42,   38,   38,   37,   37,   37,
   36,   36,   35,   35,   33,   33,   34,   34,   34,   34,
   34,   34,   34,   32,   32,   32,   30,   30,   30,   31,
   31,   31,   31,   31,   29,   29,   29,   29,   29,   29,
   29,   29,   29,   41,   41,   40,   40,   40,   40,   40,
   39,   39,    4,    4,    5,    5,    5,    8,   25,   25,
   24,   56,
};
#if defined(__cplusplus) || defined(__STDC__)
const short yylen[] =
#else
short yylen[] =
#endif
	{                                         2,
    1,    1,    2,    1,    1,    1,    4,    4,    4,    0,
    6,    0,    8,    0,    8,    0,    8,    0,   10,    0,
   10,    3,    1,    1,    3,    3,    1,    3,    3,    3,
    1,    1,    1,    3,    3,    3,    1,    3,    1,    2,
    0,    3,    1,    4,    7,    1,    1,    3,    1,    3,
    3,    3,    3,    1,    1,    3,    3,    3,    1,    1,
    1,    2,    3,    3,    1,    3,    1,    3,    1,    3,
    3,    1,    3,    5,    5,    5,    5,    7,    7,    7,
    7,    6,    8,    8,    3,    3,    5,    3,    4,    4,
    1,    0,    1,    3,    1,    1,    1,    1,    1,    1,
    1,    1,    1,    1,    3,    1,    3,    1,    3,    3,
    1,    3,    1,    3,    1,    3,    1,    3,    3,    3,
    3,    3,    3,    1,    3,    3,    1,    3,    3,    1,
    2,    2,    2,    3,    3,    7,    5,    7,    6,    4,
    3,    1,    1,    1,    3,    4,    1,    4,    9,    4,
    1,    3,    1,    3,    1,    3,    3,    1,    1,    1,
    1,    1,
};
#if defined(__cplusplus) || defined(__STDC__)
const short yydefred[] =
#else
short yydefred[] =
#endif
	{                                      0,
    0,    0,    0,    4,    5,    6,    0,    2,    0,    0,
    0,    0,    3,  161,    0,    0,   33,    0,   37,    0,
    0,    0,   23,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,  159,  160,    0,   65,    0,    0,
   24,    0,    8,    0,    9,   12,    7,    0,    0,    0,
    0,    0,    0,   30,   29,   36,   35,   34,    0,    0,
   64,    0,   22,  158,   26,   28,    0,    0,    0,    0,
   54,   40,   46,    0,    0,    0,    0,    0,    0,    0,
   68,   66,   25,    0,    0,    0,    0,    0,   55,    0,
    0,    0,   47,    0,   38,  162,    0,   11,    0,   18,
    0,   20,    0,   60,   59,   49,    0,    0,    0,   58,
   57,    0,   53,    0,    0,    0,   44,   62,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,  143,    0,    0,  127,    0,    0,    0,
    0,    0,    0,  144,    0,    0,    0,    0,    0,    0,
    0,    0,    0,   13,   42,   56,   52,   51,   48,    0,
    0,    0,    0,   93,    0,    0,    0,    0,    0,    0,
    0,    0,  131,  132,    0,    0,    0,    0,    0,    0,
    0,    0,  133,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   63,
    0,   15,    0,   17,   50,    0,  157,  156,  154,    0,
    0,    0,    0,    0,    0,    0,    0,   73,    0,    0,
  135,  141,   71,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,  134,  128,  129,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   98,
   99,   97,   96,   95,  100,  101,  102,  103,    0,    0,
    0,    0,    0,   85,    0,    0,  145,   70,    0,    0,
    0,    0,   94,    0,    0,    0,    0,    0,    0,    0,
    0,  107,    0,    0,    0,    0,    0,    0,  148,    0,
  150,  146,  140,    0,    0,    0,    0,    0,   19,   21,
    0,   75,   76,   77,    0,    0,    0,    0,    0,    0,
    0,   74,    0,    0,  137,    0,    0,  105,    0,   87,
    0,   45,    0,    0,    0,   82,    0,    0,  152,    0,
    0,    0,    0,    0,  139,   78,   79,   80,   81,    0,
    0,    0,  136,  138,    0,   83,   84,  149,
};
#if defined(__cplusplus) || defined(__STDC__)
const short yydgoto[] =
#else
short yydgoto[] =
#endif
	{                                       3,
    4,    5,    6,  142,  143,   71,   93,   65,  176,  106,
   22,   23,   40,   41,   94,   50,   72,   90,   73,   51,
   85,   86,  107,   25,  144,   38,   39,   26,  145,  146,
  147,  148,  149,  150,  151,  152,  153,  187,  301,  154,
  155,  251,  156,  157,   97,   98,  283,    7,    8,   29,
   67,   78,   80,  158,  160,   99,
};
#if defined(__cplusplus) || defined(__STDC__)
const short yysindex[] =
#else
short yysindex[] =
#endif
	{                                   -226,
  262,   29,    0,    0,    0,    0, -226,    0,  106,  -25,
 -201, -201,    0,    0,   91,  125,    0,   98,    0, -189,
  202,  184,    0,  -81,  190,  -14,  147,   20,  -66,  211,
  243,  202,  202,  121,    0,    0,   50,    0,   67,  124,
    0,   59,    0,  202,    0,    0,    0,   39,  312,   61,
  103,  323,  328,    0,    0,    0,    0,    0, -189, -189,
    0,  202,    0,    0,    0,    0,  356,  317,  348, -148,
    0,    0,    0,  -80,   39,  375,  355,  -66,  360,  -66,
    0,    0,    0,   55,  148,  151,  202,  202,    0,  162,
  403,  411,    0,  224,    0,    0,  375,    0, 1143,    0,
  228,    0,  265,    0,    0,    0,  229,  375,   39,    0,
    0, -148,    0,  202,  202,  -80,    0,    0,  451,  420,
  463,  475,   -3,   -3,   -3,   -3,   -3,   -3,  477, 1096,
 1096, 1101, 1101, 1143,  484,  189,  189, 1096,  189,  189,
  486,  489,   75,    0,  269,   13,    0,  239,  419,   12,
  444,  251,   51,    0,  496,  488,  288,  356,  375,  356,
  375,   55,  502,    0,    0,    0,    0,    0,    0,   59,
  202, -201, 1101,    0, -201, 1101, 1101, 1101, 1101, 1101,
 1101, -201,    0,    0,  511,  -12,  512, -105,  430, 1101,
  517,  519,    0,  521,  522, 1101, 1101, 1101, 1101, 1096,
 1101, 1101, 1101, 1101, 1101, 1101, 1101, 1101, 1101, 1101,
 1101, 1101, 1101,  153, 1143, 1101, 1101, -201, 1143,    0,
  306,    0,  311,    0,    0,  529,    0,    0,    0,  -36,
  535,  -31,  -16,    8,    9,   10,   47,    0, 1101, 1101,
    0,    0,    0,  -10,  -34,  -34, 1101,  -34,   -2,   63,
  532,  487,    1,  303,    0,    0,    0,   13,   13,   12,
  239,  239,  239,  239,  239,  239,  419,  444,  539,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  269,  -34,
  490,  496,  -34,    0,  251,  251,    0,    0,  375,  375,
  -80, 1143,    0, 1143, 1143, 1101, 1101, 1101,  458,  135,
  538,    0, 1143,  309,  330,    6,  344,  351,    0, 1101,
    0,    0,    0, 1101, -201,  547,  496,  496,    0,    0,
  261,    0,    0,    0,   19,   36,   37, 1143,   43,  269,
  548,    0, 1101, 1101,    0, 1101, 1101,    0,   44,    0,
  269,    0, 1143, 1143, 1143,    0, 1143,  544,    0,  550,
   56,   68,   76,   84,    0,    0,    0,    0,    0, 1101,
 1143, 1143,    0,    0,  555,    0,    0,    0,};
#if defined(__cplusplus) || defined(__STDC__)
const short yyrindex[] =
#else
short yyrindex[] =
#endif
	{                                      0,
    0,    0,    0,    0,    0,    0,  600,    0,    0,  -65,
    0,    0,    0,    0,  -32,    5,    0,    0,    0,    0,
    0,   60,    0,    0,    0,    0,    0,    0,  346,    0,
    0,    0,    0,    0,    0,    0,  108,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  347,
    0,  -58,  -49,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,  346,    0,  346,
    0,    0,    0,    0,    0,  -81,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    2,    0,  -39,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   97,    0,
  591,    0,    0,    0,    0,    0,    0,    0,   23,    0,
    0,    0,    0,   24,    0,    0,    0,    0,    0,    0,
    0,    0,  268,    0,  386,  395,    0,  446,  916,  -37,
  928,  990,   49,    0,  358, -107,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   35,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   88,    0,    0,    0,   88,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  560,
    0,    0,    0,  859,    0,    0,    0,  473,  533,  911,
  646,  651,  706,  742,  781,  820,  921,  967,  141,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
  345,  178,    0,    0, 1029, 1038,    0,    0,    0,    0,
    0,   88,    0,   88,   88,    0,    0,    0,    0,  565,
    0,    0,   88,    0,    0,    0,  859,    0,    0,    0,
    0,    0,    0,    0,    0,    0,  196,  216,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   88,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   88,   88,   88,    0,   88,  565,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   88,   88,    0,    0,    0,    0,    0,    0,};
#if defined(__cplusplus) || defined(__STDC__)
const short yygindex[] =
#else
short yygindex[] =
#endif
	{                                      0,
    0,    0,    0,  326,  338,  102,  491,  438,  297,  447,
    0,    0,    0, 1226,  319,    0,  -41,    0,    0,  354,
  342,    0,    0,  -11,  298,  552,    0,    0,   74,   57,
  258,  286,  402,  412,  406,  252, 1152, -160,  292,    0,
  534, -101, 1024,  -93,    0,  452,    0,    0,  609,    0,
    0,    0,    0,    0,    0,    0,
};
#define YYTABLESIZE 1512
#if defined(__cplusplus) || defined(__STDC__)
const short yytable[] =
#else
short yytable[] =
#endif
	{                                      30,
   31,   61,   32,  115,  292,  279,  115,   32,   32,  294,
   32,   32,   32,   32,   27,   49,   10,   69,  242,   92,
  217,  115,   58,   14,  295,   32,   32,   32,   32,   32,
  303,  240,   16,   95,    1,    2,  174,  252,  309,   31,
  189,  313,  175,  141,   31,   31,  335,   31,   31,   31,
   31,  296,  297,  298,  201,  115,  115,   14,   32,  343,
   32,   32,   31,   31,   31,   31,   31,  165,   35,   36,
  202,  209,  211,  210,   12,  106,  344,  345,   70,  302,
  115,   72,   92,  347,  355,   32,  115,  115,  115,  217,
  299,   32,   32,   32,  217,   31,  361,   31,   31,   27,
   27,   27,   27,   27,   27,   27,  310,   91,  362,  217,
   60,   68,   69,  217,  199,  217,  363,   27,   27,   27,
   27,   27,   31,  217,  364,  288,  217,  106,   31,   31,
   31,  217,   32,  217,  217,  217,  155,  155,  155,  155,
  155,  155,  155,   34,  217,   21,   92,   72,   92,   69,
   27,   67,   27,   27,  155,  155,  155,  155,  155,   61,
  229,  217,  217,  231,   63,  198,   33,   62,  217,  217,
  238,   89,  217,   91,  197,   43,  217,   27,  330,   91,
   92,  217,  216,   27,   27,   27,   21,  155,  217,  155,
  155,   10,  279,  217,  277,  275,   20,  276,   14,   86,
   67,  217,  113,  183,  184,  112,  287,   16,  338,  217,
  278,  193,   92,  166,  155,   48,   10,   92,   42,  115,
  155,  155,  155,   14,   32,  119,  120,  121,   46,   32,
  141,   44,   16,   14,   15,   16,   88,   17,   18,   19,
  280,   21,   45,   32,  115,   32,  273,   32,  115,   32,
  115,  216,  115,   32,   90,   32,  216,   32,  365,  258,
  259,   31,   61,   61,  117,   86,   31,  116,   52,  163,
  274,  216,  162,  255,   89,  216,   47,  216,  272,   72,
   31,  203,   31,  204,   31,  216,   31,  206,  216,  207,
   31,  208,   31,  216,   31,  216,  216,  216,   68,   69,
   53,  342,   88,  340,  116,   91,  216,   11,  147,  147,
  147,  147,  147,  147,  104,  105,   27,   37,   64,   10,
   90,   27,    9,  216,  216,  147,  147,  147,  147,  147,
  216,  216,  214,   59,  216,   27,  215,   27,  216,   27,
   89,   27,   75,  216,   92,   27,  314,   27,  218,   27,
  216,   74,  333,  155,  218,  216,   81,   37,  155,   76,
  147,  147,   77,  216,   14,   15,   16,   79,   17,   18,
   19,  216,  155,  334,  155,  218,  155,   87,  155,   14,
   56,   57,  155,   96,  155,  147,  155,  336,  147,  218,
  147,  147,  147,  147,  337,   84,  218,   86,  142,  142,
  142,  142,  142,  147,  108,   14,   15,   16,   88,   17,
   18,   19,  119,  120,  121,  142,  142,  142,  142,  142,
  177,  178,  179,  180,  181,  269,  130,  130,  130,  130,
  130,  101,  109,  103,   88,  124,  100,  124,  124,  124,
  270,  102,  271,  130,  130,  130,  130,  130,  119,  120,
  142,  142,   90,  124,  124,  124,  124,  185,  256,  257,
   14,   15,   16,  114,   17,   18,   19,  285,  286,  147,
  200,  115,   89,  191,  192,  142,  194,  195,  130,  130,
  171,  142,  142,  142,  159,  170,  117,  124,  124,  117,
  138,  261,  262,  263,  264,  265,  266,  132,  328,  221,
  130,  223,  131,  130,  117,  117,  117,  117,  172,  130,
  130,  130,  124,  125,  173,  125,  125,  125,  124,  124,
  124,  161,  182,  190,  147,  196,  139,  140,  197,  147,
  121,  125,  125,  125,  125,  141,  205,  212,  117,  117,
  213,  218,  226,  147,  220,  147,  219,  147,  118,  147,
  239,  281,  241,  147,  243,  147,  245,  147,  246,  164,
  247,  248,  289,  117,  300,  125,  125,  290,  291,  117,
  117,  117,  311,  126,  293,  126,  126,  126,  331,  312,
  198,  133,  281,  281,  315,  281,  341,  330,  350,  360,
  125,  126,  126,  126,  126,  368,  125,  125,  125,    1,
  104,  147,   41,   39,  316,  151,  169,  227,  225,  321,
  222,   82,  224,  267,  142,   13,  260,  281,  268,  142,
  281,  349,    0,    0,    0,  126,  126,    0,    0,    0,
  153,  153,    0,  142,  153,  142,    0,  142,    0,  142,
    0,    0,  130,  142,    0,  142,    0,  142,    0,    0,
  126,  124,    0,    0,    0,  348,  126,  126,  126,  153,
  153,  130,    0,  130,    0,  130,  348,  130,  153,    0,
  124,  130,  124,  130,  124,  130,  124,    0,    0,    0,
  124,    0,  124,    0,  124,    0,  121,    0,    0,  121,
    0,  122,    0,    0,  122,    0,    0,    0,    0,    0,
    0,    0,  117,    0,  121,  121,  121,  121,    0,  122,
  122,  122,  122,    0,  153,   35,   36,  119,  120,  121,
    0,  117,    0,  117,    0,  117,    0,  117,    0,  125,
    0,  117,  254,  117,    0,  117,    0,    0,  121,  121,
  319,  320,    0,  122,  122,    0,  123,  282,  125,  123,
  125,    0,  125,    0,  125,    0,    0,    0,  125,    0,
  125,    0,  125,  121,  123,  123,  123,  123,  122,  121,
  121,  121,    0,    0,  122,  122,  122,    0,  304,  305,
  307,  308,  118,    0,    0,  118,    0,    0,    0,  126,
    0,    0,    0,    0,    0,    0,    0,    0,  123,  123,
  118,  118,  118,  118,    0,    0,    0,    0,  126,    0,
  126,    0,  126,  317,  126,    0,  318,    0,  126,    0,
  126,  119,  126,  123,  119,    0,    0,    0,    0,  123,
  123,  123,    0,    0,  118,  118,    0,    0,    0,  119,
  119,  119,  119,    0,    0,    0,    0,    0,  153,  153,
  153,  153,  153,    0,    0,    0,    0,    0,    0,  118,
  120,    0,    0,  120,    0,  118,  118,  118,    0,    0,
    0,    0,    0,  119,  119,    0,    0,    0,  120,  120,
  120,  120,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  119,  142,
  142,  142,  121,  142,  119,  119,  119,  122,    0,    0,
    0,    0,  120,  120,    0,    0,  142,    0,  142,  142,
  142,  121,    0,  121,    0,  121,  122,  121,  122,    0,
  122,  121,  122,  121,    0,  121,  122,  120,  122,    0,
  122,    0,    0,  120,  120,  120,    0,    0,    0,    0,
    0,  116,  142,    0,  116,    0,  113,    0,    0,  113,
    0,  114,  123,    0,  114,    0,    0,    0,  111,  116,
    0,  111,    0,    0,  113,    0,  142,    0,    0,  114,
    0,  123,    0,  123,  142,  123,  111,  123,    0,    0,
    0,  123,    0,  123,    0,  123,    0,    0,  118,    0,
    0,    0,    0,  116,  116,    0,    0,  112,  113,  113,
  112,    0,    0,  114,  114,    0,    0,  118,    0,  118,
  111,  118,    0,  118,    0,  112,    0,  118,  116,  118,
  108,  118,    0,  108,  116,  116,  116,  119,    0,  113,
  113,  113,    0,    0,  114,  114,  114,    0,  108,    0,
    0,  111,  111,  111,    0,    0,  119,    0,  119,  112,
  119,    0,  119,    0,    0,    0,  119,    0,  119,  109,
  119,    0,  109,    0,    0,    0,  120,    0,  110,    0,
    0,  110,  108,    0,    0,    0,    0,  109,    0,    0,
  112,  112,  112,    0,    0,  120,  110,  120,    0,  120,
    0,  120,    0,    0,    0,  120,    0,  120,    0,  120,
    0,    0,    0,  108,  108,  108,    0,    0,    0,    0,
  142,  109,    0,    0,    0,    0,    0,    0,    0,    0,
  110,    0,    0,  138,  142,  132,  142,    0,  142,    0,
  132,    0,    0,  130,    0,  131,  142,    0,  142,    0,
    0,    0,  109,  109,  109,    0,    0,    0,    0,    0,
    0,  110,  110,  110,  139,  140,    0,  116,    0,  139,
  140,    0,  113,  141,    0,  138,    0,  114,  141,    0,
    0,    0,  132,    0,  111,  130,    0,  131,    0,    0,
    0,    0,  116,    0,    0,    0,  116,  113,  116,    0,
  116,  113,  114,  113,    0,  113,  114,    0,  114,  111,
  114,  139,  140,  111,    0,  111,    0,  111,  137,  133,
  141,    0,    0,  112,  133,    0,    0,    0,    0,  135,
  136,    0,    0,    0,   24,   28,    0,    0,  284,    0,
    0,    0,    0,    0,    0,    0,  108,    0,  112,    0,
    0,    0,  112,    0,  112,    0,  112,   54,   55,    0,
    0,    0,    0,    0,    0,  134,  133,    0,    0,   66,
    0,  108,    0,    0,    0,  108,    0,  108,    0,    0,
    0,    0,    0,  186,  188,  109,    0,   83,    0,    0,
    0,    0,    0,    0,  110,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
  109,    0,  110,  111,  109,  322,  109,  323,  324,  110,
    0,    0,    0,  110,  230,  110,  332,  232,  233,  234,
  235,  236,  237,    0,    0,    0,    0,    0,    0,  167,
  168,  244,    0,    0,    0,    0,    0,  249,  250,  186,
  253,  346,    0,   35,   36,  119,  120,  121,   35,   36,
  119,  120,  121,    0,    0,    0,  356,  357,  358,    0,
  359,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,  366,  367,    0,    0,    0,    0,
  250,  186,    0,    0,    0,    0,  228,    0,  306,    0,
   35,   36,  119,  120,  121,    0,    0,    0,  122,  123,
  124,  125,  126,  127,  128,  129,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,  325,  326,  327,
  329,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  250,    0,    0,    0,  339,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,  351,  352,    0,  353,  354,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  250,
};
#if defined(__cplusplus) || defined(__STDC__)
const short yycheck[] =
#else
short yycheck[] =
#endif
	{                                      11,
   12,    0,   35,   41,   41,   40,   44,   40,   41,   41,
   43,   44,   45,   46,   40,   82,   82,  125,  124,   59,
  126,   59,   34,   82,   41,   58,   59,   60,   61,   62,
   41,   44,   82,   75,  261,  262,   40,  198,   41,   35,
  134,   41,   46,   78,   40,   41,   41,   43,   44,   45,
   46,   44,   44,   44,   42,   93,   94,  259,   91,   41,
   93,   94,   58,   59,   60,   61,   62,  109,  258,  259,
   58,   60,   61,   62,   46,   41,   41,   41,   40,  240,
  118,   59,   59,   41,   41,  118,  124,  125,  126,  126,
   44,  124,  125,  126,  126,   91,   41,   93,   94,   40,
   41,   42,   43,   44,   45,   46,   44,   59,   41,  126,
   44,  260,  261,  126,   40,  126,   41,   58,   59,   60,
   61,   62,  118,  126,   41,  219,  126,   93,  124,  125,
  126,  126,   42,  126,  126,  126,   40,   41,   42,   43,
   44,   45,   46,   46,  126,   40,   59,  125,  125,  257,
   91,   44,   93,   94,   58,   59,   60,   61,   62,   93,
  172,  126,  126,  175,   41,   91,   42,   44,  126,  126,
  182,   70,  126,  125,   40,  257,  126,  118,   44,  260,
  261,  126,  288,  124,  125,  126,   40,   91,  126,   93,
   94,  257,   40,  126,   42,   43,   91,   45,  257,   59,
   93,  126,   41,  130,  131,   44,  218,  257,  310,  126,
   58,  138,  125,  112,  118,  282,  282,  257,   35,  257,
  124,  125,  126,  282,  257,  260,  261,  262,   82,  262,
   78,   42,  282,  259,  260,  261,   59,  263,  264,  265,
   88,   40,  257,  276,  282,  278,   94,  280,  286,  282,
  288,  288,  290,  286,   59,  288,  288,  290,  360,  203,
  204,  257,  261,  262,   41,  125,  262,   44,   58,   41,
  118,  288,   44,  200,   59,  288,  257,  288,  126,  257,
  276,   43,  278,   45,  280,  288,  282,  276,  288,  278,
  286,  280,  288,  288,  290,  288,  288,  288,  260,  261,
   58,   41,  125,  315,   44,  257,  288,   46,   41,   42,
   43,   44,   45,   46,  260,  261,  257,   20,  260,   58,
  125,  262,   61,  288,  288,   58,   59,   60,   61,   62,
  288,  288,  282,  284,  288,  276,  286,  278,  288,  280,
  125,  282,  282,  288,  257,  286,   44,  288,   46,  290,
  288,   40,   44,  257,   46,  288,   59,   60,  262,  257,
   93,   94,   40,  288,  259,  260,  261,   40,  263,  264,
  265,  288,  276,   44,  278,   46,  280,   61,  282,  259,
  260,  261,  286,    9,  288,  118,  290,   44,   44,   46,
   46,  124,  125,  126,   44,   40,   46,  257,   41,   42,
   43,   44,   45,   59,  257,  259,  260,  261,   61,  263,
  264,  265,  260,  261,  262,   58,   59,   60,   61,   62,
  124,  125,  126,  127,  128,  273,   41,   42,   43,   44,
   45,   78,  282,   80,  257,   41,   82,   43,   44,   45,
  288,   82,  290,   58,   59,   60,   61,   62,  260,  261,
   93,   94,  257,   59,   60,   61,   62,  132,  201,  202,
  259,  260,  261,   61,  263,  264,  265,  216,  217,  125,
  145,   61,  257,  136,  137,  118,  139,  140,   93,   94,
   61,  124,  125,  126,  257,   35,   41,   93,   94,   44,
   33,  206,  207,  208,  209,  210,  211,   40,   41,  158,
   43,  160,   45,  118,   59,   60,   61,   62,   46,  124,
  125,  126,  118,   41,   40,   43,   44,   45,  124,  125,
  126,  257,   46,   40,  257,   40,   69,   70,   40,  262,
  262,   59,   60,   61,   62,   78,  118,   94,   93,   94,
  290,   46,   41,  276,  257,  278,   59,  280,   97,  282,
   40,  214,   41,  286,  125,  288,   40,  290,   40,  108,
   40,   40,  257,  118,  239,   93,   94,  257,   40,  124,
  125,  126,   41,   41,   40,   43,   44,   45,   41,   93,
   91,  124,  245,  246,   46,  248,   40,   44,   41,   40,
  118,   59,   60,   61,   62,   41,  124,  125,  126,    0,
   41,  257,  257,  257,  279,   41,  116,  170,  162,  291,
  159,   60,  161,  212,  257,    7,  205,  280,  213,  262,
  283,  330,   -1,   -1,   -1,   93,   94,   -1,   -1,   -1,
   40,   41,   -1,  276,   44,  278,   -1,  280,   -1,  282,
   -1,   -1,  257,  286,   -1,  288,   -1,  290,   -1,   -1,
  118,  257,   -1,   -1,   -1,  330,  124,  125,  126,   69,
   70,  276,   -1,  278,   -1,  280,  341,  282,   78,   -1,
  276,  286,  278,  288,  280,  290,  282,   -1,   -1,   -1,
  286,   -1,  288,   -1,  290,   -1,   41,   -1,   -1,   44,
   -1,   41,   -1,   -1,   44,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,  257,   -1,   59,   60,   61,   62,   -1,   59,
   60,   61,   62,   -1,  124,  258,  259,  260,  261,  262,
   -1,  276,   -1,  278,   -1,  280,   -1,  282,   -1,  257,
   -1,  286,  199,  288,   -1,  290,   -1,   -1,   93,   94,
  289,  290,   -1,   93,   94,   -1,   41,  214,  276,   44,
  278,   -1,  280,   -1,  282,   -1,   -1,   -1,  286,   -1,
  288,   -1,  290,  118,   59,   60,   61,   62,  118,  124,
  125,  126,   -1,   -1,  124,  125,  126,   -1,  245,  246,
  247,  248,   41,   -1,   -1,   44,   -1,   -1,   -1,  257,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   93,   94,
   59,   60,   61,   62,   -1,   -1,   -1,   -1,  276,   -1,
  278,   -1,  280,  280,  282,   -1,  283,   -1,  286,   -1,
  288,   41,  290,  118,   44,   -1,   -1,   -1,   -1,  124,
  125,  126,   -1,   -1,   93,   94,   -1,   -1,   -1,   59,
   60,   61,   62,   -1,   -1,   -1,   -1,   -1,  258,  259,
  260,  261,  262,   -1,   -1,   -1,   -1,   -1,   -1,  118,
   41,   -1,   -1,   44,   -1,  124,  125,  126,   -1,   -1,
   -1,   -1,   -1,   93,   94,   -1,   -1,   -1,   59,   60,
   61,   62,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  118,   41,
   42,   43,  257,   45,  124,  125,  126,  257,   -1,   -1,
   -1,   -1,   93,   94,   -1,   -1,   58,   -1,   60,   61,
   62,  276,   -1,  278,   -1,  280,  276,  282,  278,   -1,
  280,  286,  282,  288,   -1,  290,  286,  118,  288,   -1,
  290,   -1,   -1,  124,  125,  126,   -1,   -1,   -1,   -1,
   -1,   41,   94,   -1,   44,   -1,   41,   -1,   -1,   44,
   -1,   41,  257,   -1,   44,   -1,   -1,   -1,   41,   59,
   -1,   44,   -1,   -1,   59,   -1,  118,   -1,   -1,   59,
   -1,  276,   -1,  278,  126,  280,   59,  282,   -1,   -1,
   -1,  286,   -1,  288,   -1,  290,   -1,   -1,  257,   -1,
   -1,   -1,   -1,   93,   94,   -1,   -1,   41,   93,   94,
   44,   -1,   -1,   93,   94,   -1,   -1,  276,   -1,  278,
   93,  280,   -1,  282,   -1,   59,   -1,  286,  118,  288,
   41,  290,   -1,   44,  124,  125,  126,  257,   -1,  124,
  125,  126,   -1,   -1,  124,  125,  126,   -1,   59,   -1,
   -1,  124,  125,  126,   -1,   -1,  276,   -1,  278,   93,
  280,   -1,  282,   -1,   -1,   -1,  286,   -1,  288,   41,
  290,   -1,   44,   -1,   -1,   -1,  257,   -1,   41,   -1,
   -1,   44,   93,   -1,   -1,   -1,   -1,   59,   -1,   -1,
  124,  125,  126,   -1,   -1,  276,   59,  278,   -1,  280,
   -1,  282,   -1,   -1,   -1,  286,   -1,  288,   -1,  290,
   -1,   -1,   -1,  124,  125,  126,   -1,   -1,   -1,   -1,
  262,   93,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   93,   -1,   -1,   33,  276,   40,  278,   -1,  280,   -1,
   40,   -1,   -1,   43,   -1,   45,  288,   -1,  290,   -1,
   -1,   -1,  124,  125,  126,   -1,   -1,   -1,   -1,   -1,
   -1,  124,  125,  126,   69,   70,   -1,  257,   -1,   69,
   70,   -1,  257,   78,   -1,   33,   -1,  257,   78,   -1,
   -1,   -1,   40,   -1,  257,   43,   -1,   45,   -1,   -1,
   -1,   -1,  282,   -1,   -1,   -1,  286,  282,  288,   -1,
  290,  286,  282,  288,   -1,  290,  286,   -1,  288,  282,
  290,   69,   70,  286,   -1,  288,   -1,  290,   76,  124,
   78,   -1,   -1,  257,  124,   -1,   -1,   -1,   -1,   87,
   88,   -1,   -1,   -1,    9,   10,   -1,   -1,  215,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  257,   -1,  282,   -1,
   -1,   -1,  286,   -1,  288,   -1,  290,   32,   33,   -1,
   -1,   -1,   -1,   -1,   -1,  123,  124,   -1,   -1,   44,
   -1,  282,   -1,   -1,   -1,  286,   -1,  288,   -1,   -1,
   -1,   -1,   -1,  132,  133,  257,   -1,   62,   -1,   -1,
   -1,   -1,   -1,   -1,  257,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  282,   -1,   87,   88,  286,  292,  288,  294,  295,  282,
   -1,   -1,   -1,  286,  173,  288,  303,  176,  177,  178,
  179,  180,  181,   -1,   -1,   -1,   -1,   -1,   -1,  114,
  115,  190,   -1,   -1,   -1,   -1,   -1,  196,  197,  198,
  199,  328,   -1,  258,  259,  260,  261,  262,  258,  259,
  260,  261,  262,   -1,   -1,   -1,  343,  344,  345,   -1,
  347,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,  361,  362,   -1,   -1,   -1,   -1,
  239,  240,   -1,   -1,   -1,   -1,  171,   -1,  247,   -1,
  258,  259,  260,  261,  262,   -1,   -1,   -1,  266,  267,
  268,  269,  270,  271,  272,  273,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  296,  297,  298,
  299,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,  310,   -1,   -1,   -1,  314,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,  333,  334,   -1,  336,  337,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,  360,
};
#define YYFINAL 3
#ifndef YYDEBUG
#define YYDEBUG 1
#endif
#define YYMAXTOKEN 302
#if YYDEBUG
#if defined(__cplusplus) || defined(__STDC__)
const char * const yyname[] =
#else
char *yyname[] =
#endif
	{
"end-of-file",0,0,0,0,0,0,0,0,"'\\t'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,"'!'",0,"'#'",0,0,0,0,"'('","')'","'*'","'+'","','","'-'","'.'",0,0,0,0,0,0,
0,0,0,0,0,"':'","';'","'<'","'='","'>'",0,0,0,0,0,0,"'E'","'F'",0,0,0,0,0,"'L'",
0,"'N'",0,0,0,"'R'",0,0,0,0,"'W'","'X'",0,0,"'['",0,"']'","'^'",0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"'v'",0,0,0,0,"'{'","'|'","'}'","'~'",0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,"CR","ZAHL","INTEGER","NAME","NEWNAME","FUNCNAME","S0","S1","S2","W0",
"W1","W2","W3","W4","W5","W6","FIN","ERROR","YYLE","\"<=\"","YYGE","\">=\"",
"YYNE","\"!=\"","YYASS","\"=>\"","YYRANGE","\"..\"","YYCOND","\"->\"","YYDISV",
"\"!~\"","YYIMPL","\"!>\"","YYSQRT","\"-/\"","YYSOR","\"vv\"","YYSAND","\"^^\"",
"YYSIMPL","\"!>>\"","YYSDISV","\"!~~\"","YYSEQUIV","\"~~\"",
};
#if defined(__cplusplus) || defined(__STDC__)
const char * const yyrule[] =
#else
char *yyrule[] =
#endif
	{"$accept : File",
"File : Programm",
"Programm : Planteil",
"Programm : Programm Planteil",
"Planteil : Vereinbarung",
"Planteil : Typvereinbarung",
"Planteil : Funktion",
"Vereinbarung : NEWNAME ':' Strukturdefinition CR",
"Typvereinbarung : NEWNAME '=' Strukturdefinition CR",
"Typvereinbarung : NEWNAME '=' Beschraenkung CR",
"$$1 :",
"Funktion : NEWNAME ':' $$1 Randauszug CR Programmzeilen",
"$$2 :",
"Funktion : NEWNAME ':' '(' 'R' $$2 Randauszug1 CR Programmzeilen",
"$$3 :",
"Funktion : NEWNAME '.' Integerkonstante ':' $$3 Randauszug CR Programmzeilen",
"$$4 :",
"Funktion : FUNCNAME '.' Integerkonstante ':' $$4 Randauszug CR Programmzeilen",
"$$5 :",
"Funktion : NEWNAME '.' Integerkonstante ':' '(' 'R' $$5 Randauszug1 CR Programmzeilen",
"$$6 :",
"Funktion : FUNCNAME '.' Integerkonstante ':' '(' 'R' $$6 Randauszug1 CR Programmzeilen",
"Strukturdefinition : '(' Strukturelemente ')'",
"Strukturdefinition : Artangabe",
"Strukturelemente : Strukturdefinition",
"Strukturelemente : Strukturelemente ',' Strukturdefinition",
"Artangabe : Artdefinition '#' Beschraenkungsangabe",
"Artangabe : Artdefinition",
"Artdefinition : Integerkonstante '*' Strukturdefinition",
"Artdefinition : NEWNAME '*' Strukturdefinition",
"Artdefinition : NAME '*' Strukturdefinition",
"Artdefinition : NEWNAME",
"Artdefinition : NAME",
"Artdefinition : S0",
"Artdefinition : S1 '.' Integerkonstante",
"Artdefinition : S1 '.' NEWNAME",
"Artdefinition : S1 '.' NAME",
"Artdefinition : S2",
"Randauszug : Eingangsparameter \"=>\" Ausgangsparameter",
"Randauszug : Eingangsparameter",
"Randauszug : \"=>\" Ausgangsparameter",
"Randauszug :",
"Randauszug1 : Eingangsparameter1 \"=>\" Ausgangsparameter",
"Randauszug1 : Eingangsparameter1",
"Eingangsparameter : 'R' '(' Parameterliste ')'",
"Eingangsparameter1 : '(' Typenliste ')' ')' '(' Parameterliste ')'",
"Ausgangsparameter : Rueckgabevariablen",
"Parameterliste : Parametervariable",
"Parameterliste : Parameterliste ',' Parametervariable",
"Typenliste : Typenvariable",
"Typenliste : Typenliste ',' Typenvariable",
"Parametervariable : NEWNAME '=' Strukturdefinition",
"Parametervariable : NAME '=' Strukturdefinition",
"Rueckgabevariablen : '(' Rueckgabeliste ')'",
"Rueckgabevariablen : Rueckgabevariable",
"Rueckgabeliste : Rueckgabevariable",
"Rueckgabeliste : Rueckgabeliste ',' Rueckgabevariable",
"Rueckgabevariable : NEWNAME '=' Strukturdefinition",
"Rueckgabevariable : NAME '=' Strukturdefinition",
"Typenvariable : NEWNAME",
"Typenvariable : NAME",
"Programmzeilen : Programmzeile",
"Programmzeilen : Programmzeile Programmzeilen",
"Programmzeile : leer Anweisungen CR",
"Beschraenkung : '[' Wertliste ']'",
"Wertliste : Konstantenbereich",
"Wertliste : Wertliste ',' Konstantenbereich",
"Konstantenbereich : Konstante",
"Konstantenbereich : Konstante \"..\" Konstante",
"Anweisungen : Anweisung",
"Anweisungen : Anweisung ';' Anweisungen",
"Anweisung : '{' Anweisungen '}'",
"Anweisung : FIN",
"Anweisung : FIN '.' Integerkonstante",
"Anweisung : 'W' '(' Ausdruck ')' Anweisung",
"Anweisung : W0 '(' Ausdruck ')' Anweisung",
"Anweisung : W1 Wdhanfang Ausdruck ')' Anweisung",
"Anweisung : W2 Wdhanfang Ausdruck ')' Anweisung",
"Anweisung : W3 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung",
"Anweisung : W4 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung",
"Anweisung : W5 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung",
"Anweisung : W6 Wdhanfang Ausdruck ',' Ausdruck ')' Anweisung",
"Anweisung : W6 Wdhanfang Ausdruck ',' ')' Anweisung",
"Anweisung : 'X' Variable '(' StrukturVariable ',' Ausdruck ')' Anweisung",
"Anweisung : 'L' Variable '(' StrukturVariable ',' Ausdruck ')' Anweisung",
"Anweisung : Ausdruck \"->\" Anweisung",
"Anweisung : Ausdruck \"=>\" FIN",
"Anweisung : Ausdruck \"=>\" FIN '.' Integerkonstante",
"Anweisung : Ausdruck \"=>\" StrukturVariable",
"Anweisung : Ausdruck \"=>\" SubOp StrukturVariable",
"Anweisung : Ausdruck \"=>\" 'X' StrukturVariable",
"Anweisung : Ausdruck",
"Anweisung :",
"Wdhanfang : '('",
"Wdhanfang : '.' Integerkonstante '('",
"SubOp : 'v'",
"SubOp : '^'",
"SubOp : '~'",
"SubOp : \"!~\"",
"SubOp : \"!>\"",
"SubOp : '+'",
"SubOp : '-'",
"SubOp : '*'",
"SubOp : ':'",
"Ausdrucksliste : Ausdruck",
"Ausdrucksliste : Ausdruck ',' Ausdrucksliste",
"Ausdruecke : Ausdruck",
"Ausdruecke : Ausdruck ',' Ausdruecke",
"Ausdruck : Equivvergleich",
"Ausdruck : Ausdruck \"!~\" Equivvergleich",
"Ausdruck : Ausdruck '~' Equivvergleich",
"Equivvergleich : Implvergleich",
"Equivvergleich : Equivvergleich \"!>\" Implvergleich",
"Implvergleich : Undvergleich",
"Implvergleich : Implvergleich '^' Undvergleich",
"Undvergleich : Odervergleich",
"Undvergleich : Undvergleich 'v' Odervergleich",
"Odervergleich : Vergleich",
"Odervergleich : Odervergleich '<' Vergleich",
"Odervergleich : Odervergleich '>' Vergleich",
"Odervergleich : Odervergleich '=' Vergleich",
"Odervergleich : Odervergleich \"<=\" Vergleich",
"Odervergleich : Odervergleich \">=\" Vergleich",
"Odervergleich : Odervergleich \"!=\" Vergleich",
"Vergleich : Strichrechnung",
"Vergleich : Vergleich '+' Strichrechnung",
"Vergleich : Vergleich '-' Strichrechnung",
"Strichrechnung : Punktrechnung",
"Strichrechnung : Strichrechnung '*' Punktrechnung",
"Strichrechnung : Strichrechnung ':' Punktrechnung",
"Punktrechnung : Klammerausdruck",
"Punktrechnung : '+' Klammerausdruck",
"Punktrechnung : '-' Klammerausdruck",
"Punktrechnung : '!' Klammerausdruck",
"Punktrechnung : Klammerausdruck Funktionsvariable Klammerausdruck",
"Klammerausdruck : '(' Ausdruecke ')'",
"Klammerausdruck : 'E' Variable '(' StrukturVariable ',' Ausdruck ')'",
"Klammerausdruck : 'E' Variable '(' Ausdruck ')'",
"Klammerausdruck : 'F' Variable '(' StrukturVariable ',' Ausdruck ')'",
"Klammerausdruck : Variable '(' StrukturVariable ',' Ausdruck ')'",
"Klammerausdruck : Variable '(' Ausdruck ')'",
"Klammerausdruck : '|' Ausdruck '|'",
"Klammerausdruck : StrukturVariable",
"Klammerausdruck : Konstante",
"StrukturVariable : SubVariable",
"StrukturVariable : StrukturVariable '.' Integerkonstante",
"SubVariable : Variable '[' Ausdruecke ']'",
"SubVariable : Variable",
"SubVariable : 'N' '(' Ausdruck ')'",
"SubVariable : '(' Funktionsvariable '(' Funktionsvariablenliste ')' ')' '(' Ausdrucksliste ')'",
"SubVariable : Funktionsvariable '(' Ausdrucksliste ')'",
"Funktionsvariablenliste : Funktionsvariable",
"Funktionsvariablenliste : Funktionsvariable ',' Funktionsvariablenliste",
"Funktionsvariable : FUNCNAME",
"Funktionsvariable : FUNCNAME '.' Integerkonstante",
"Variable : NAME",
"Variable : NEWNAME '=' Strukturdefinition",
"Variable : NAME '#' Beschraenkungsangabe",
"Beschraenkungsangabe : NAME",
"Konstante : ZAHL",
"Konstante : INTEGER",
"Integerkonstante : INTEGER",
"leer : '\\t'",
};
#endif
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH 10000
#endif
#endif
#define YYINITSTACKSIZE 200
/* LINTUSED */
int yydebug;
int yynerrs;
int yyerrflag;
int yychar;
short *yyssp;
YYSTYPE *yyvsp;
YYSTYPE yyval;
YYSTYPE yylval;
short *yyss;
short *yysslim;
YYSTYPE *yyvs;
int yystacksize;
#line 709 "plankal.y"


void yyerror(char *cp)
{
    cerr << cp << endl;
}

#ifdef YYPRINT
//static int yyprint(FILE *fp, int t, YYSTYPE v)
static int yyprint(void *fpx, int t, ...)
{
    va_list	ap;
    int i;
    FILE	*fp;
    YYSTYPE	v;

    va_start(ap,t);
    fp = (FILE *)fpx;
    v = va_arg(ap,YYSTYPE);
    if (t == ZAHL || t == BITS || t == INTEGER) {
	fprintf(fp," ");
        v.val->print();
    } else if (t == NEWNAME)
        fprintf(fp, " \"%s\"", v.string);
    else if (t == NAME || t == FUNCNAME) {
	fprintf(fp," \"");
	v.namensdef->print(false);
	fprintf(fp,"\"");
    }
    va_end(ap);
    return 0;
}
#endif 

char	eingabezeile[256];
short	eingabeindex = 0;
static bool	zurueck = false;
static int	linenr = 0;
static ifstream	*includes[20];
static int	include_lnr[20];
static int	include_nr = 0;

static void zurueckZeichen(char c)
{
    ins->putback(c);
    zurueck = true;
}

static int holeZeichen()
{
    int		c;
    char	fname[NAMELEN];

    c = ins->get();
    if (c == EOF && include_nr) {
	delete ins;
	ins = includes[--include_nr];
	linenr = include_lnr[include_nr];
	c = ins->get();
    }
    if (!zurueck) {
	if (eingabeindex == 0 && c == '#') {
	    while (c == '#') {
		if (eingabeindex < sizeof(eingabezeile)-1)
		    eingabezeile[eingabeindex++] = c;
		while ((c = ins->get()) != '\n')
		    if (eingabeindex < sizeof(eingabezeile)-1)
			eingabezeile[eingabeindex++] = c;
		eingabezeile[eingabeindex] = '\0';
		gen_comment(eingabezeile);
		eingabeindex = 0;
		linenr++;
		if (eingabezeile[1] == '<') {
		    includes[include_nr] = ins;
		    include_lnr[include_nr++] = linenr;
		    strcpy(fname,&eingabezeile[2]);
		    ins = new ifstream(fname);
		    linenr = 0;
		}
		c = ins->get();
	    }
	}
	if (c == '\n') {
	    eingabezeile[eingabeindex] = '\0';
	    gen_comment(eingabezeile);
	    eingabeindex = 0;
	    linenr++;
	} else if (eingabeindex < sizeof(eingabezeile)-1)
	    eingabezeile[eingabeindex++] = c;
    }
    zurueck = false;
    return c;
}

int yylex()
{
    int		c;
    int		i;
    char	*cp;
    Namen	*np;

    while (1) {
	c = holeZeichen();
	switch (c) {
	    case EOF:
		return 0;
	    case '\n':
		return CR;
//	    case '\t':
	    case ' ':
		break;
	    case '=':
		c = holeZeichen();
		if (c == '>')
		    return YYASS;
		zurueckZeichen(c);
		return '=';
	    case '<':
		c = holeZeichen();
		if (c == '=')
		    return YYLE;
		zurueckZeichen(c);
		return '<';
	    case '>':
		c = holeZeichen();
		if (c == '=')
		    return YYGE;
		zurueckZeichen(c);
		return '>';
	    case '!':
		c = holeZeichen();
		if (c == '=')
		    return YYNE;
		if (c == '>') {
		    c = holeZeichen();
		    if (c == '>')
			return YYSIMPL;
		    zurueckZeichen(c);
		    return YYIMPL;
		}
		if (c == '~') {
		    c = holeZeichen();
		    if (c == '~')
			return YYSDISV;
		    zurueckZeichen(c);
		    return YYDISV;
		}
		zurueckZeichen(c);
		return '!';
	    case '.':
		c = holeZeichen();
		if (c == '.')
		    return YYRANGE;
		zurueckZeichen(c);
		return '.';
	    case '-':
		c = holeZeichen();
		if (c == '>')
		    return YYCOND;
		if (c == '/')
		    return YYSQRT;
		zurueckZeichen(c);
		return '-';
	    case '^':
		c = holeZeichen();
		if (c == '^')
		    return YYSAND;
		zurueckZeichen(c);
		return '^';
	    case '~':
		c = holeZeichen();
		if (c == '~')
		    return YYSEQUIV;
		zurueckZeichen(c);
		return '~';
	    case '%':
		c = holeZeichen();
		i = 1;
		cp = buffer;
		*cp++ = '%';
		while (isdigit(c)) {
		    if (++i < sizeof(buffer))
			*cp++ = c;
		    c = holeZeichen();
		}
		zurueckZeichen(c);
		*cp = '\0';
		np = NULL;
		if (function)		// first look for a local name
		    np = function->def->name->suche(buffer);
		if (!np) {		// then global
		    np = namen->suche(buffer);
		    if (!np) {
			yylval.string = new char[strlen(buffer)+1];
			strcpy(yylval.string,buffer);
			return NEWNAME;
		    }
		}
		yylval.namensdef = np;
		return NAME;
	    case '\t':
	    case '|':
	    case '#':
	    case '+':
	    case '*':
	    case ':':
	    case ',':
	    case ';':
	    case '(':
	    case ')':
	    case '[':
	    case ']':
	    case '{':		// TODO until we find something better
	    case '}':
		return c;
	    case '\'':
		c = holeZeichen();
		switch (c) {
		case '0':
		case 'L':
		    i = 0;
		    cp = buffer;
		    while (c == '0' || c == 'L') {
			if ((i & 7) == 0)
			    *cp = '\0';
			*cp <<= 1;
			if (c == 'L')
			    *cp |= 1;
			if (i < sizeof(buffer)*8 && (++i & 7) == 0)
			    cp++;
			c = holeZeichen();
		    }
		    if (c != '\'')
			return ERROR;
		    if ((i & 7) != 0)
			*cp <<= 8-(i & 7);
		    if (i == 1) {
			yylval.val = new Konstante((long)*cp);
			return INTEGER;
		    }
		    yylval.val = new Konstante(BITS);
		    yylval.val->bval.set((unsigned char *)buffer,i);
		    return ZAHL;
		case '+':
		case '-':
		    i = 0;
		    cp = buffer;
		    while (c == '-' || c == '+') {
			if ((i & 7) == 0)
			    *cp = '\0';
			if (c == '+')
			    *cp |= 1 << (i & 7);
			if (i < sizeof(buffer)*8 && (++i & 7) == 0)
			    cp++;
			c = holeZeichen();
		    }
		    if (c != '\'')
			return ERROR;
//		    if ((i & 7) != 0)
//			*cp <<= 8-(i & 7);
		    if (i == 1) {
			yylval.val = new Konstante((long)*cp);
			return INTEGER;
		    }
		    yylval.val = new Konstante(BITS);
		    yylval.val->bval.set_re((unsigned char *)buffer,i);
		    return ZAHL;
		case '\'':
		    yylval.val = new Konstante(0L);
		    return INTEGER;
		}
		return ERROR;
	    default:
		if (isdigit(c)) {
		    i = 0;
		    cp = buffer;
		    while (isdigit(c)) {
			if (++i < sizeof(buffer))
			    *cp++ = c;
			c = holeZeichen();
		    }
		    zurueckZeichen(c);
		    *cp = '\0';
		    yylval.val = new Konstante(atol(buffer));
		    return INTEGER;
		}
		if (isalpha(c)) {
		    i = 0;
		    cp = buffer;
		    while (isalpha(c) || isdigit(c)) {
			if (++i < sizeof(buffer))
			    *cp++ = c;
			c = holeZeichen();
		    }
		    zurueckZeichen(c);
		    *cp = '\0';
		    if (strcmp(buffer,"S0") == 0)
			return S0;	// a bit
		    if (strcmp(buffer,"S1") == 0)
			return S1;	// a bit of a bitvector
		    if (strcmp(buffer,"S2") == 0)
			return S2;	// a pair of any structure
		    if (strcmp(buffer,"E") == 0)
			return 'E';	// Existiert-Anweisung
		    if (strcmp(buffer,"F") == 0)
			return 'F';	// First-Anweisung
		    if (strcmp(buffer,"X") == 0)
			return 'X';	// Next-Anweisung
		    if (strcmp(buffer,"L") == 0)
			return 'L';	// Last-Anweisung
		    if (strcmp(buffer,"N") == 0)
			return 'N';	// Anzahl-Anweisung
		    if (strcmp(buffer,"R") == 0)
			return 'R';	// Resultatwert
		    if (strcmp(buffer,"W") == 0)
			return 'W';	// Wiederholungsanweisung
		    if (strcmp(buffer,"W0") == 0)
			return W0;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W1") == 0)
			return W1;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W2") == 0)
			return W2;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W3") == 0)
			return W3;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W4") == 0)
			return W4;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W5") == 0)
			return W5;	// Wiederholungsanweisung
		    if (strcmp(buffer,"W6") == 0)
			return W6;	// Wiederholungsanweisung
		    if (strcmp(buffer,"FIN") == 0)
			return FIN;	// Abbruchanweisung
		    if (strcmp(buffer,"v") == 0)
			return 'v';	// or
		    if (strcmp(buffer,"vv") == 0)
			return YYSOR;	// or
		    np = NULL;
		    if (function)	// first look for a local name
			np = function->def->name->suche(buffer);
		    if (!np) {		// then global
			np = namen->suche(buffer);
			if (!np) {
			    yylval.string = new char[strlen(buffer)+1];
			    strcpy(yylval.string,buffer);
			    return NEWNAME;
			}
		    }
		    yylval.namensdef = np;
		    if (np->def && (np->def->typ == FUNCTION || np->def->typ == TYPEDEF))
			return FUNCNAME;
		    return NAME;
		}
		return ERROR;
	}
    }
    return ERROR;
}

static Namen *out_fkt(Def *dp,Expr *ep)
{
    Expr	*exp;
    Namen	*np;

    function->def->down = dp;
    exp = restruct(ep);
//    exp->define_all();
    exp->label_all();
    function->def->exp = exp;
    np = function;
    function = NULL;
    return np;
}
#line 1161 "plankal.cc"
/* allocate initial stack or double stack size, up to YYMAXDEPTH */
#if defined(__cplusplus) || defined(__STDC__)
static int yygrowstack(void)
#else
static int yygrowstack()
#endif
{
    int newsize, i;
    short *newss;
    YYSTYPE *newvs;

    if ((newsize = yystacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return -1;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;
    i = yyssp - yyss;
    newss = yyss ? (short *)realloc(yyss, newsize * sizeof *newss) :
      (short *)malloc(newsize * sizeof *newss);
    if (newss == NULL)
        goto bail;
    yyss = newss;
    yyssp = newss + i;
    newvs = yyvs ? (YYSTYPE *)realloc(yyvs, newsize * sizeof *newvs) :
      (YYSTYPE *)malloc(newsize * sizeof *newvs);
    if (newvs == NULL)
        goto bail;
    yyvs = newvs;
    yyvsp = newvs + i;
    yystacksize = newsize;
    yysslim = yyss + newsize - 1;
    return 0;
bail:
    if (yyss)
            free(yyss);
    if (yyvs)
            free(yyvs);
    yyss = yyssp = NULL;
    yyvs = yyvsp = NULL;
    yystacksize = 0;
    return -1;
}

#define YYABORT goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR goto yyerrlab
int
#if defined(__cplusplus) || defined(__STDC__)
yyparse(void)
#else
yyparse()
#endif
{
    int yym, yyn, yystate;
#if YYDEBUG
#if defined(__cplusplus) || defined(__STDC__)
    const char *yys;
#else /* !(defined(__cplusplus) || defined(__STDC__)) */
    char *yys;
#endif /* !(defined(__cplusplus) || defined(__STDC__)) */

    if ((yys = getenv("YYDEBUG")))
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif /* YYDEBUG */

    yynerrs = 0;
    yyerrflag = 0;
    yychar = (-1);

    if (yyss == NULL && yygrowstack()) goto yyoverflow;
    yyssp = yyss;
    yyvsp = yyvs;
    *yyssp = yystate = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yyssp >= yysslim && yygrowstack())
        {
            goto yyoverflow;
        }
        *++yyssp = yystate = yytable[yyn];
        *++yyvsp = yylval;
        yychar = (-1);
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;
#if defined(lint) || defined(__GNUC__)
    goto yynewerror;
#endif
yynewerror:
    yyerror("syntax error");
#if defined(lint) || defined(__GNUC__)
    goto yyerrlab;
#endif
yyerrlab:
    ++yynerrs;
yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yyssp]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yyssp, yytable[yyn]);
#endif
                if (yyssp >= yysslim && yygrowstack())
                {
                    goto yyoverflow;
                }
                *++yyssp = yystate = yytable[yyn];
                *++yyvsp = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yyssp);
#endif
                if (yyssp <= yyss) goto yyabort;
                --yyssp;
                --yyvsp;
            }
        }
    }
    else
    {
        if (yychar == 0) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = 0;
            if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
            if (!yys) yys = "illegal-symbol";
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = (-1);
        goto yyloop;
    }
yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    yyval = yyvsp[1-yym];
    switch (yyn)
    {
case 4:
#line 113 "plankal.y"
{ }
break;
case 5:
#line 115 "plankal.y"
{ }
break;
case 6:
#line 117 "plankal.y"
{ }
break;
case 7:
#line 120 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&namen,yyvsp[-3].string);
				  delete [] yyvsp[-3].string;
				  yyval.namensdef->def = yyvsp[-1].definition;
				  outs << xindent;
				  yyval.namensdef->def->gen();
				  outs << ' ';
				  yyval.namensdef->gen();
				  yyval.namensdef->def->vgen();
				  outs << ';' << endl;
				  yyval.namensdef->defined = true;
				}
break;
case 8:
#line 134 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&namen,yyvsp[-3].string);
				  delete [] yyvsp[-3].string;
				  yyval.namensdef->def = yyvsp[-1].definition;
				  if (yyval.namensdef->def->typ == STRUCT) {
				      if (yyval.namensdef->def->name)	/* this name is not in use*/
					  yyval.namensdef->def->name->defined = true;
				      yyval.namensdef->def->name = yyval.namensdef;
				      yyval.namensdef->def->gen();
				  } else {
				      outs << xindent << "typedef ";
				      yyval.namensdef->def->gen();
				      outs << ' ';
				      yyval.namensdef->gen();
				  }
				  outs << ';' << endl;
				  yyval.namensdef->defined = true;
				}
break;
case 9:
#line 153 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&namen,yyvsp[-3].string);
				  delete [] yyvsp[-3].string;
				  yyval.namensdef->besch = yyvsp[-1].bereich;
				  outs << xindent << "Restrict ";
				  yyval.namensdef->gen();
				  outs << "[] = {";
				  yyval.namensdef->besch->gen();
				  outs << "};" << endl;
				  yyval.namensdef->defined = true;
				}
break;
case 10:
#line 166 "plankal.y"
{
				  function = Namen::neuerName(&namen,yyvsp[-1].string);
				  delete [] yyvsp[-1].string;
				  function->def = new Def(FUNCTION);
				}
break;
case 11:
#line 172 "plankal.y"
{ yyval.namensdef = out_fkt(yyvsp[-2].definition,yyvsp[0].expression); }
break;
case 12:
#line 174 "plankal.y"
{
				  function = Namen::neuerName(&namen,yyvsp[-3].string);
				  delete [] yyvsp[-3].string;
				  function->def = new Def(FUNCTION);
				}
break;
case 13:
#line 180 "plankal.y"
{ yyval.namensdef = out_fkt(yyvsp[-2].definition,yyvsp[0].expression); }
break;
case 14:
#line 182 "plankal.y"
{
				  function = Namen::neuerName(&namen,yyvsp[-3].string,yyvsp[-1].val->lval);
				  delete [] yyvsp[-3].string;
				  delete yyvsp[-1].val;
				  function->def = new Def(FUNCTION);
				}
break;
case 15:
#line 189 "plankal.y"
{ yyval.namensdef = out_fkt(yyvsp[-2].definition,yyvsp[0].expression); }
break;
case 16:
#line 191 "plankal.y"
{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,yyvsp[-3].namensdef->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  Namen *np = namen->suche(tmp,yyvsp[-1].val->lval);
				  if (np)
				      YYERROR;
				  function = Namen::neuerName(&namen,tmp,yyvsp[-1].val->lval);
				  delete yyvsp[-1].val;
				  function->def = new Def(FUNCTION);
				}
break;
case 17:
#line 205 "plankal.y"
{ yyval.namensdef = out_fkt(yyvsp[-2].definition,yyvsp[0].expression); }
break;
case 18:
#line 207 "plankal.y"
{
				  function = Namen::neuerName(&namen,yyvsp[-5].string,yyvsp[-3].val->lval);
				  delete [] yyvsp[-5].string;
				  delete yyvsp[-3].val;
				  function->def = new Def(FUNCTION);
				}
break;
case 19:
#line 214 "plankal.y"
{ yyval.namensdef = out_fkt(yyvsp[-2].definition,yyvsp[0].expression); }
break;
case 20:
#line 216 "plankal.y"
{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,yyvsp[-5].namensdef->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  Namen *np = namen->suche(tmp,yyvsp[-3].val->lval);
				  if (np)
				      YYERROR;
				  function = Namen::neuerName(&namen,tmp,yyvsp[-3].val->lval);
				  delete yyvsp[-3].val;
				  function->def = new Def(FUNCTION);
				}
break;
case 21:
#line 230 "plankal.y"
{ yyval.namensdef = out_fkt(yyvsp[-2].definition,yyvsp[0].expression); }
break;
case 22:
#line 233 "plankal.y"
{
				  if (yyvsp[-1].definition->isTemplate())
				      yyval.definition = new Def(STRUCT);
				  else {
				      Namen *np;
				      np = Namen::neuerName(&namen,"st",++structs);
				      yyval.definition = new Def(STRUCT,np);
				      np->def = yyval.definition;
				  }
				  yyval.definition->down = yyvsp[-1].definition;
				}
break;
case 23:
#line 245 "plankal.y"
{ yyval.definition = yyvsp[0].definition; }
break;
case 24:
#line 248 "plankal.y"
{ yyval.definition = yyvsp[0].definition; }
break;
case 25:
#line 250 "plankal.y"
{
				  Def	*dp;
				  for (dp = yyvsp[-2].definition; dp->next; dp = dp->next)
				      ;
				  dp->next = yyvsp[0].definition;
				  yyval.definition = yyvsp[-2].definition;
				}
break;
case 26:
#line 259 "plankal.y"
{ yyval.definition = yyvsp[-2].definition; }
break;
case 27:
#line 261 "plankal.y"
{ yyval.definition = yyvsp[0].definition; }
break;
case 28:
#line 264 "plankal.y"
{
				  yyval.definition = new Def(VEKTOR,yyvsp[-2].val->lval);
				  delete yyvsp[-2].val;
				  yyval.definition->down = yyvsp[0].definition;
				}
break;
case 29:
#line 270 "plankal.y"
{
				  Namen *np = Namen::neuerName(&function->def->name,yyvsp[-2].string);
				  delete [] yyvsp[-2].string;
				  np->def = new Def(INT);
				  yyval.definition = new Def(VEKTOR,np);
				  yyval.definition->down = yyvsp[0].definition;
				}
break;
case 30:
#line 278 "plankal.y"
{
				  yyval.definition = new Def(VEKTOR,yyvsp[-2].namensdef);
				  yyval.definition->down = yyvsp[0].definition;
				}
break;
case 31:
#line 283 "plankal.y"
{
				  Namen *np = Namen::neuerName(&function->def->name,yyvsp[0].string);
				  delete [] yyvsp[0].string;
				  np->def = new Def(TEMPLATE);
				  yyval.definition = new Def(NAMEDEF,np);
				}
break;
case 32:
#line 290 "plankal.y"
{ yyval.definition = new Def(NAMEDEF,yyvsp[0].namensdef); }
break;
case 33:
#line 292 "plankal.y"
{ yyval.definition = new Def(BIT); }
break;
case 34:
#line 294 "plankal.y"
{
				  yyval.definition = new Def(BITVEKTOR,yyvsp[0].val->lval);
				  delete yyvsp[0].val;
				}
break;
case 35:
#line 299 "plankal.y"
{
				  Namen *np = Namen::neuerName(&function->def->name,yyvsp[0].string);
				  delete [] yyvsp[0].string;
				  np->def = new Def(INT);
				  yyval.definition = new Def(BITVEKTOR,np);
				}
break;
case 36:
#line 306 "plankal.y"
{ yyval.definition = new Def(BITVEKTOR,yyvsp[0].namensdef); }
break;
case 37:
#line 308 "plankal.y"
{ yyval.definition = new Def(BIT); }
break;
case 38:
#line 311 "plankal.y"
{ yyval.definition = new Def(yyvsp[-2].definition,yyvsp[0].definition); }
break;
case 39:
#line 313 "plankal.y"
{ yyval.definition = new Def(yyvsp[0].definition,NULL); }
break;
case 40:
#line 315 "plankal.y"
{ yyval.definition = new Def(NULL,yyvsp[0].definition); }
break;
case 41:
#line 317 "plankal.y"
{ yyval.definition = new Def(NULL,NULL); }
break;
case 42:
#line 320 "plankal.y"
{ yyval.definition = new Def(yyvsp[-2].definition,yyvsp[0].definition); }
break;
case 43:
#line 322 "plankal.y"
{ yyval.definition = new Def(yyvsp[0].definition,NULL); }
break;
case 44:
#line 325 "plankal.y"
{ yyval.definition = yyvsp[-1].definition; }
break;
case 45:
#line 328 "plankal.y"
{
				  Def	*dp;
				  for (dp = yyvsp[-5].definition; dp->next; dp = dp->next)
				      ;
				  dp->next = yyvsp[-1].definition;
				  yyval.definition = yyvsp[-5].definition;
				}
break;
case 47:
#line 339 "plankal.y"
{ yyval.definition = new Def(NAMEDEF,yyvsp[0].namensdef); }
break;
case 48:
#line 341 "plankal.y"
{
				  Def	*dp;
				  for (dp = yyvsp[-2].definition; dp->next; dp = dp->next)
				      ;
				  dp->next = new Def(NAMEDEF,yyvsp[0].namensdef);
				  yyval.definition = yyvsp[-2].definition;
				}
break;
case 49:
#line 350 "plankal.y"
{
				  yyvsp[0].namensdef->def = new Def(TYPEDEF);
				  yyval.definition = new Def(NAMEDEF,yyvsp[0].namensdef);
				}
break;
case 50:
#line 355 "plankal.y"
{
				  Def	*dp;
				  for (dp = yyvsp[-2].definition; dp->next; dp = dp->next)
				      ;
				  yyvsp[0].namensdef->def = new Def(TYPEDEF);
				  dp->next = new Def(NAMEDEF,yyvsp[0].namensdef);
				  yyval.definition = yyvsp[-2].definition;
				}
break;
case 51:
#line 365 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&function->def->name,yyvsp[-2].string);
				  delete [] yyvsp[-2].string;
				  yyval.namensdef->def = yyvsp[0].definition;
				}
break;
case 52:
#line 371 "plankal.y"
{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,yyvsp[-2].namensdef->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  yyval.namensdef = Namen::neuerName(&function->def->name,tmp);
				  yyval.namensdef->def = yyvsp[0].definition;
				}
break;
case 53:
#line 382 "plankal.y"
{ yyval.definition = yyvsp[-1].definition; }
break;
case 54:
#line 384 "plankal.y"
{ yyval.definition = new Def(NAMEDEF,yyvsp[0].namensdef); }
break;
case 55:
#line 387 "plankal.y"
{ yyval.definition = new Def(NAMEDEF,yyvsp[0].namensdef); }
break;
case 56:
#line 389 "plankal.y"
{
				  Def	*dp;
				  for (dp = yyvsp[-2].definition; dp->next; dp = dp->next)
				      ;
				  dp->next = new Def(NAMEDEF,yyvsp[0].namensdef);
				  yyval.definition = yyvsp[-2].definition;
				}
break;
case 57:
#line 398 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&function->def->name,yyvsp[-2].string);
				  delete [] yyvsp[-2].string;
				  yyval.namensdef->def = yyvsp[0].definition;
				}
break;
case 58:
#line 404 "plankal.y"
{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,yyvsp[-2].namensdef->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  yyval.namensdef = Namen::neuerName(&function->def->name,tmp);
				  yyval.namensdef->def = yyvsp[0].definition;
				}
break;
case 59:
#line 415 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&function->def->name,yyvsp[0].string);
				  delete [] yyvsp[0].string;
				}
break;
case 60:
#line 420 "plankal.y"
{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,yyvsp[0].namensdef->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  yyval.namensdef = Namen::neuerName(&function->def->name,tmp);
				}
break;
case 61:
#line 430 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 62:
#line 432 "plankal.y"
{
				  if (yyvsp[-1].expression)
				      yyval.expression = yyvsp[0].expression;
				  else if (!yyvsp[0].expression)
				      yyval.expression = yyvsp[-1].expression;
				  else
				      yyval.expression = new Expr(STMNT,yyvsp[-1].expression,yyvsp[0].expression);
				}
break;
case 63:
#line 442 "plankal.y"
{ yyval.expression = yyvsp[-1].expression; }
break;
case 64:
#line 445 "plankal.y"
{ yyval.bereich = yyvsp[-1].bereich; }
break;
case 65:
#line 448 "plankal.y"
{ yyval.bereich = yyvsp[0].bereich; }
break;
case 66:
#line 450 "plankal.y"
{
				  Bereich	*bp;
				  for (bp = yyvsp[-2].bereich; bp->next; bp = bp->next)
				      ;
				  bp->next = yyvsp[0].bereich;
				  yyval.bereich = yyvsp[-2].bereich;
				}
break;
case 67:
#line 459 "plankal.y"
{ yyval.bereich = new Bereich(yyvsp[0].val); }
break;
case 68:
#line 461 "plankal.y"
{ yyval.bereich = new Bereich(yyvsp[-2].val,yyvsp[0].val); }
break;
case 69:
#line 464 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 70:
#line 466 "plankal.y"
{
				  if (!yyvsp[-2].expression)
				      yyval.expression = yyvsp[0].expression;
				  else if (!yyvsp[0].expression)
				      yyval.expression = yyvsp[-2].expression;
				  else
				      yyval.expression = new Expr(STMNT,yyvsp[-2].expression,yyvsp[0].expression);
				}
break;
case 71:
#line 476 "plankal.y"
{ yyval.expression = yyvsp[-1].expression; }
break;
case 72:
#line 478 "plankal.y"
{ yyval.expression = new Expr(BREAK,new Konstante(1L)); }
break;
case 73:
#line 480 "plankal.y"
{ yyval.expression = new Expr(BREAK,yyvsp[0].val); }
break;
case 74:
#line 482 "plankal.y"
{ yyval.expression = new Expr(WHILE,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 75:
#line 484 "plankal.y"
{ yyval.expression = new Expr(FOR0,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 76:
#line 486 "plankal.y"
{ yyval.expression = new Expr(FOR1,yyvsp[-3].namensdef,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 77:
#line 488 "plankal.y"
{ yyval.expression = new Expr(FOR2,yyvsp[-3].namensdef,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 78:
#line 490 "plankal.y"
{ yyval.expression = new Expr(FOR3,yyvsp[-5].namensdef,yyvsp[-4].expression,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 79:
#line 492 "plankal.y"
{ yyval.expression = new Expr(FOR4,yyvsp[-5].namensdef,yyvsp[-4].expression,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 80:
#line 494 "plankal.y"
{ yyval.expression = new Expr(FOR5,yyvsp[-5].namensdef,yyvsp[-4].expression,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 81:
#line 496 "plankal.y"
{ yyval.expression = new Expr(FOR6,yyvsp[-5].namensdef,yyvsp[-4].expression,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 82:
#line 498 "plankal.y"
{ yyval.expression = new Expr(FOR6,yyvsp[-4].namensdef,yyvsp[-3].expression,NULL,yyvsp[0].expression); }
break;
case 83:
#line 500 "plankal.y"
{ yyval.expression = new Expr(NEXT,yyvsp[-6].namensdef,yyvsp[-4].expression,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 84:
#line 502 "plankal.y"
{ yyval.expression = new Expr(LAST,yyvsp[-6].namensdef,yyvsp[-4].expression,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 85:
#line 504 "plankal.y"
{ yyval.expression = new Expr(IF,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 86:
#line 506 "plankal.y"
{ yyval.expression = new Expr(IF,yyvsp[-2].expression,new Expr(BREAK,new Konstante(2L))); }
break;
case 87:
#line 508 "plankal.y"
{
				  yyvsp[0].val->lval++;
				  yyval.expression = new Expr(IF,yyvsp[-4].expression,new Expr(BREAK,yyvsp[0].val));
				}
break;
case 88:
#line 513 "plankal.y"
{ yyval.expression = new Expr(ASSIGN,yyvsp[0].expression,yyvsp[-2].expression); }
break;
case 89:
#line 515 "plankal.y"
{ yyval.expression = new Expr(yyvsp[-1].oper,yyvsp[0].expression,yyvsp[-3].expression); }
break;
case 90:
#line 517 "plankal.y"
{ yyval.expression = new Expr(ASSNEXT,yyvsp[0].expression,yyvsp[-3].expression); }
break;
case 91:
#line 519 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 92:
#line 520 "plankal.y"
{ yyval.expression = NULL; }
break;
case 93:
#line 523 "plankal.y"
{
				  yyval.namensdef = Namen::neuerName(&function->def->name,"i");
				  yyval.namensdef->def = new Def(INT);
				}
break;
case 94:
#line 528 "plankal.y"
{
				  char *tmp = new char[10];
				  sprintf(tmp,"i%d",yyvsp[-1].val->lval);
				  delete yyvsp[-1].val;
				  yyval.namensdef = Namen::neuerName(&function->def->name,tmp);
				  yyval.namensdef->def = new Def(INT);
				}
break;
case 95:
#line 536 "plankal.y"
{ yyval.oper = ASSOR; }
break;
case 96:
#line 537 "plankal.y"
{ yyval.oper = ASSAND; }
break;
case 97:
#line 538 "plankal.y"
{ yyval.oper = ASSEQUIV; }
break;
case 98:
#line 539 "plankal.y"
{ yyval.oper = ASSDISV; }
break;
case 99:
#line 540 "plankal.y"
{ yyval.oper = ASSIMPL; }
break;
case 100:
#line 541 "plankal.y"
{ yyval.oper = ASSADD; }
break;
case 101:
#line 542 "plankal.y"
{ yyval.oper = ASSSUB; }
break;
case 102:
#line 543 "plankal.y"
{ yyval.oper = ASSMUL; }
break;
case 103:
#line 544 "plankal.y"
{ yyval.oper = ASSDIV; }
break;
case 104:
#line 547 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 105:
#line 549 "plankal.y"
{ yyval.expression = new Expr(PARLIST,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 106:
#line 552 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 107:
#line 554 "plankal.y"
{
				  yyval.expression = new Expr(CAT,yyvsp[-2].expression,yyvsp[0].expression);
				}
break;
case 108:
#line 559 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 109:
#line 561 "plankal.y"
{ yyval.expression = new Expr(DISVAL,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 110:
#line 563 "plankal.y"
{ yyval.expression = new Expr(EQUIV,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 111:
#line 566 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 112:
#line 568 "plankal.y"
{ yyval.expression = new Expr(IMPL,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 113:
#line 571 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 114:
#line 573 "plankal.y"
{ yyval.expression = new Expr(AND,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 115:
#line 576 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 116:
#line 578 "plankal.y"
{ yyval.expression = new Expr(OR,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 117:
#line 581 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 118:
#line 583 "plankal.y"
{ yyval.expression = new Expr(LESS,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 119:
#line 585 "plankal.y"
{ yyval.expression = new Expr(GREATER,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 120:
#line 587 "plankal.y"
{ yyval.expression = new Expr(EQUAL,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 121:
#line 589 "plankal.y"
{ yyval.expression = new Expr(LE,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 122:
#line 591 "plankal.y"
{ yyval.expression = new Expr(GE,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 123:
#line 593 "plankal.y"
{ yyval.expression = new Expr(NE,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 124:
#line 596 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 125:
#line 598 "plankal.y"
{ yyval.expression = new Expr(ADD,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 126:
#line 600 "plankal.y"
{ yyval.expression = new Expr(SUB,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 127:
#line 603 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 128:
#line 605 "plankal.y"
{ yyval.expression = new Expr(MUL,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 129:
#line 607 "plankal.y"
{ yyval.expression = new Expr(DIV,yyvsp[-2].expression,yyvsp[0].expression); }
break;
case 130:
#line 610 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 131:
#line 612 "plankal.y"
{ yyval.expression = new Expr(POS,yyvsp[0].expression); }
break;
case 132:
#line 614 "plankal.y"
{ yyval.expression = new Expr(NEG,yyvsp[0].expression); }
break;
case 133:
#line 616 "plankal.y"
{ yyval.expression = new Expr(NOT,yyvsp[0].expression); }
break;
case 134:
#line 618 "plankal.y"
{ yyval.expression = new Expr(CALL,new Expr(VAR,yyvsp[-1].namensdef),new Expr(PARLIST,yyvsp[-2].expression,yyvsp[0].expression)); }
break;
case 135:
#line 621 "plankal.y"
{
				  if (yyvsp[-1].expression->oper == CAT)
				      yyval.expression = new Expr(GENSTRUCT,yyvsp[-1].expression);
				  else
				      yyval.expression = yyvsp[-1].expression;
				}
break;
case 136:
#line 628 "plankal.y"
{ yyval.expression = new Expr(EXIST,yyvsp[-5].namensdef,yyvsp[-3].expression,yyvsp[-1].expression); }
break;
case 137:
#line 630 "plankal.y"
{ yyval.expression = new Expr(EXIST,yyvsp[-3].namensdef,yyvsp[-1].expression); }
break;
case 138:
#line 632 "plankal.y"
{ yyval.expression = new Expr(FIRST,yyvsp[-5].namensdef,yyvsp[-3].expression,yyvsp[-1].expression); }
break;
case 139:
#line 634 "plankal.y"
{ yyval.expression = new Expr(ALL,yyvsp[-5].namensdef,yyvsp[-3].expression,yyvsp[-1].expression); }
break;
case 140:
#line 636 "plankal.y"
{ yyval.expression = new Expr(ALL,yyvsp[-3].namensdef,yyvsp[-1].expression); }
break;
case 141:
#line 638 "plankal.y"
{ yyval.expression = new Expr(ABS,yyvsp[-1].expression); }
break;
case 142:
#line 640 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 143:
#line 642 "plankal.y"
{ yyval.expression = new Expr(CONST,yyvsp[0].val); }
break;
case 144:
#line 645 "plankal.y"
{ yyval.expression = yyvsp[0].expression; }
break;
case 145:
#line 647 "plankal.y"
{ yyval.expression = new Expr(FIELD,yyvsp[-2].expression,new Expr(CONST,yyvsp[0].val)); }
break;
case 146:
#line 652 "plankal.y"
{ yyval.expression = new Expr(INDEX,new Expr(VAR,yyvsp[-3].namensdef),yyvsp[-1].expression); }
break;
case 147:
#line 654 "plankal.y"
{ yyval.expression = new Expr(VAR,yyvsp[0].namensdef); }
break;
case 148:
#line 656 "plankal.y"
{ yyval.expression = new Expr(COUNT,yyvsp[-1].expression); }
break;
case 149:
#line 658 "plankal.y"
{ yyval.expression = new Expr(CALL,new Expr(VAR,yyvsp[-7].namensdef),yyvsp[-1].expression,yyvsp[-5].expression); }
break;
case 150:
#line 660 "plankal.y"
{ yyval.expression = new Expr(CALL,new Expr(VAR,yyvsp[-3].namensdef),yyvsp[-1].expression); }
break;
case 151:
#line 663 "plankal.y"
{ yyval.expression = new Expr(VAR,yyvsp[0].namensdef); }
break;
case 152:
#line 665 "plankal.y"
{ yyval.expression = new Expr(PARLIST,new Expr(VAR,yyvsp[-2].namensdef),yyvsp[0].expression); }
break;
case 153:
#line 668 "plankal.y"
{ yyval.namensdef = yyvsp[0].namensdef; }
break;
case 154:
#line 670 "plankal.y"
{
				  char	tmp[NAMELEN+NUMLEN];
				  char	*cp;
				  strcpy(tmp,yyvsp[-2].namensdef->name);
				  if ((cp = strchr(tmp,'_')))
				      *cp = '\0';
				  yyval.namensdef = namen->suche(tmp,yyvsp[0].val->lval);
				  delete yyvsp[0].val;
				  if (!yyval.namensdef)
				      YYERROR;
				}
break;
case 155:
#line 683 "plankal.y"
{ yyval.namensdef = yyvsp[0].namensdef; }
break;
case 156:
#line 685 "plankal.y"
{
				  if (function)
				      yyval.namensdef = Namen::neuerName(&function->def->name,yyvsp[-2].string);
				  else
				      yyval.namensdef = Namen::neuerName(&namen,yyvsp[-2].string);
				  delete [] yyvsp[-2].string;
				  yyval.namensdef->def = yyvsp[0].definition;
				}
break;
case 157:
#line 694 "plankal.y"
{ yyval.namensdef = yyvsp[-2].namensdef; }
break;
case 158:
#line 697 "plankal.y"
{ yyval.namensdef = yyvsp[0].namensdef; }
break;
case 159:
#line 700 "plankal.y"
{ yyval.val = yyvsp[0].val; }
break;
case 160:
#line 702 "plankal.y"
{ yyval.val = yyvsp[0].val; }
break;
case 161:
#line 705 "plankal.y"
{ yyval.val = yyvsp[0].val; }
break;
#line 2222 "plankal.cc"
    }
    yyssp -= yym;
    yystate = *yyssp;
    yyvsp -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yyssp = YYFINAL;
        *++yyvsp = yyval;
        if (yychar < 0)
        {
            if ((yychar = yylex()) < 0) yychar = 0;
#if YYDEBUG
            if (yydebug)
            {
                yys = 0;
                if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
                if (!yys) yys = "illegal-symbol";
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == 0) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yyssp, yystate);
#endif
    if (yyssp >= yysslim && yygrowstack())
    {
        goto yyoverflow;
    }
    *++yyssp = yystate;
    *++yyvsp = yyval;
    goto yyloop;
yyoverflow:
    yyerror("yacc stack overflow");
yyabort:
    if (yyss)
            free(yyss);
    if (yyvs)
            free(yyvs);
    yyss = yyssp = NULL;
    yyvs = yyvsp = NULL;
    yystacksize = 0;
    return (1);
yyaccept:
    if (yyss)
            free(yyss);
    if (yyvs)
            free(yyvs);
    yyss = yyssp = NULL;
    yyvs = yyvsp = NULL;
    yystacksize = 0;
    return (0);
}
