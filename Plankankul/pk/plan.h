//#include <stdlib.h>
//#include <stdarg.h>
//#include <iostream>

struct Restrict {
    unsigned char	from;
    unsigned char	to;
};

#include "bits.h"

template<class T>
class V_ {
public:
    V_();
    V_(long l);
    V_(const V_<T>&);
    ~V_();
    V_<T> &operator= (const V_<T> &x);
    const T &operator[] (long x) const;
    T &operator[] (long x);
    T &operator* ();
    bool operator== (const V_<T> &x) const;
    bool operator!= (const V_<T> &x) const;
    V_<T> &operator += (const V_<T> & x);
    V_<T> &operator += (const T &x);
    long count() const;
private:
    T		*p;
    long	len;
};

template<class T>
inline V_<T>::V_()
{
    p = NULL;
    len = 0;
}

template<class T>
inline V_<T>::V_(long l)
{
    p = new T[l];
    len = l;
}

template<class T>
inline V_<T>::~V_()
{
    delete [] p;
}

template<class T>
inline V_<T> &V_<T>::operator= (const V_<T> &x)
{
    long	i;

    p = new T[x.len];
    len = x.len;
    for (long i = 0; i < len; i++)
	p[i] = x.p[i];
    return *this;
}

template<class T>
inline T &V_<T>::operator[] (long x)
{
    if (x < len)
	return p[x];
    else
	throw "subscript out of bounds";
}

template<class T>
inline T &V_<T>::operator* ()
{
    return p;
}

template<class T>
bool V_<T>::operator== (const V_<T> &x) const
{
    for (long i = 0; i < len; i++)
	if (!(p[i] == x.p[i]))
	    return false;
    return true;
}

template<class T>
bool V_<T>::operator!= (const V_<T> &x) const
{
    for (long i = 0; i < len; i++)
	if (p[i] == x.p[i])
	    return false;
    return true;
}

template<class T>
V_<T> &V_<T>::operator += (const V_<T> & x)
{
    T	*pt;

    pt = new T[len+1];
    for (long i = 0; i < len; i++)
	pt[i] = p[i];
    for (long i = 0; i < x.len; i++)
	pt[len+i] = x.p[i];
    delete [] p;
    p = pt;
    len += x.len;
    return *this;
}

template<class T>
V_<T> &V_<T>::operator += (const T & x)
{
    T	*pt;

    pt = new T[len+1];
    for (long i = 0; i < len; i++)
	pt[i] = p[i];
    pt[len] = x;
    delete [] p;
    p = pt;
    len++;
    return *this;
}

template<class T>
inline long V_<T>::count() const
{
    return len;
}
